package com.sp.highconnect;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.sp.highconnect.common.BaseActivity;

public class SplashScreen extends BaseActivity {

    private static int SPLASH_TIME_OUT = 4000;
    Context mContext;

    private FirebaseAuth mAuth;

    private String TAG = "SplashScreen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = this;

        mAuth = FirebaseAuth.getInstance();

        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInAnonymously:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Log.w(TAG, "signInAnonymously:success "+user);
//                            Toast.makeText(mContext, "Authentication is Successful.",Toast.LENGTH_SHORT).show();
                            GetToken();
                        } else {
                            Log.w(TAG, "signInAnonymously:failure", task.getException());
//                            Toast.makeText(mContext, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                CreateAdultsOnlyDialog();

            }
        }, SPLASH_TIME_OUT);
    }

    private void CreateAdultsOnlyDialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(mContext);
        View promptsView = li.inflate(R.layout.dialog_age_limit, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        // set dialog message
        alertDialogBuilder.setCancelable(false);


        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        LinearLayout dialog_main = promptsView.findViewById(R.id.dialog_main);
        overrideFonts(dialog_main);
        final LinearLayout mI_am_under = (LinearLayout) promptsView.findViewById(R.id.ll_under);
        final LinearLayout mI_am_over = (LinearLayout) promptsView.findViewById(R.id.ll_over);

        mI_am_under.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(mContext, "You are Under age of 19 you can't access the app", Toast.LENGTH_SHORT).show();
            }
        });

        mI_am_over.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Toast.makeText(mContext, "You are Over age of 19", Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
                Intent i = new Intent(mContext, DashboardActivity.class);
                startActivity(i);
                finish();

            }
        });
    }


}
