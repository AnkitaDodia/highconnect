package com.sp.highconnect.apihelper;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sp.highconnect.common.BaseActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class APIProvider {
    private static final String TAG = APIProvider.class.getSimpleName();
    private static APIProvider mApiProvider;
    private RequestQueue mRequestQueue;
    private Context mContext;
    private APIProvider(Context context) {

        //Prevent form the reflection api.
        if (mApiProvider != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        } else {
            mRequestQueue = Volley.newRequestQueue(context);
        }
    }

    public static APIProvider getInstance(Context context) {
        if (mApiProvider == null) { //if there is no instance available... create new one
            synchronized (APIProvider.class) {
                if (mApiProvider == null) mApiProvider = new APIProvider(context);
            }
        }
        mApiProvider.mContext = context;
        return mApiProvider;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> request) {
        request.setTag(TAG);
        getRequestQueue().add(request);
    }


    //addUser
    public void addUser(JSONObject params, final OnAPIResultListener apiResultListener) {

        // JSONObject jsonParams = new JSONObject(params);

        // Log.e(TAG, params.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Constants.CUSTOMER_BASE_URL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                         Log.e("Volley Response", response.toString());

                        if (response != null) {
                            Boolean status = false;
                            String message = "Success";
                            JSONObject data = null;

                            try {
                                status = response.getBoolean("success");
                                message = response.getString("message");
                                data = response.getJSONObject("data");
                            } catch (Exception e) {
                                message = e.getMessage();
                            }

                            if (apiResultListener != null) {
                                APIResult apiResult = new APIResult(status, message, data);
                                apiResultListener.onSuccess(apiResult);
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (apiResultListener != null) {
                    APIResult apiResult = new APIResult(false, error.getMessage());
                    apiResultListener.onSuccess(apiResult);
                }

                // Log.e("TAG", "Error: " + error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("Authorization", "Bearer " +BaseActivity.fcmIdToken);
                 map.put("content-type","application/json");
                //map.put(Constants.CONTENTTYPE, Constants.APPLICATIONTYPE);
                return map;
            }
        };
       Volley.newRequestQueue(mContext).add(request);
        //addToRequestQueue(request);
    }

    public interface OnAPIResultListener {
        public void onSuccess(APIResult apiResult);
    }

    public class APIResult {
        public Boolean mSuccess;
        public String mMessage;
        public Object mData;

        public APIResult() {
            this.mSuccess = false;
            this.mMessage = "";
            this.mData = null;
        }

        public APIResult(Boolean success) {
            this(success, "", null);
        }

        public APIResult(Boolean success, String message) {
            this(success, message, null);
        }

        public APIResult(Boolean success, String message, Object data) {
            this.mSuccess = success;
            this.mMessage = message;
            this.mData = data;
        }

        public Boolean getmSuccess() {
            return mSuccess;
        }

        public void setmSuccess(Boolean mSuccess) {
            this.mSuccess = mSuccess;
        }

        public String getmMessage() {
            return mMessage;
        }

        public void setmMessage(String mMessage) {
            this.mMessage = mMessage;
        }

        public Object getmData() {
            return mData;
        }

        public void setmData(Object mData) {
            this.mData = mData;
        }
    }
}
