package com.sp.highconnect.apihelper;

public class Constants {

    //public static String CUSTOMER_BASE_URL = "http://18.237.254.224/api/v1/VenueCustomer/SignUpCustomer";
    public static String CUSTOMER_BASE_URL = "http://18.237.254.224/api/v1/Customer/SignUpCustomer";

    public static final String AUTHORIZATION = "Authorization";
    public static final String CONTENTTYPE = "Content-Type";
    public static String AUTHORIZATIONKEY = "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6ImEwY2ViNDY3NDJhNjNlMTk2NDIxNjNhNzI4NmRjZDQyZjc0MzYzNjYifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vaGlnaGNvbm5lY3QtZGVtby0zOTMwOSIsInByb3ZpZGVyX2lkIjoiYW5vbnltb3VzIiwiYXVkIjoiaGlnaGNvbm5lY3QtZGVtby0zOTMwOSIsImF1dGhfdGltZSI6MTUzNTY1NjA1OCwidXNlcl9pZCI6Im9EdXFOUFJHZzhUUUtPajQ2NEdTZ0xOcTFWOTIiLCJzdWIiOiJvRHVxTlBSR2c4VFFLT2o0NjRHU2dMTnExVjkyIiwiaWF0IjoxNTM1Njk0NzU5LCJleHAiOjE1MzU2OTgzNTksImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnt9LCJzaWduX2luX3Byb3ZpZGVyIjoiYW5vbnltb3VzIn19.fQ0c-iqxxJZZKhcVzXu4fxCggZSzjbKLaTMZnFegeqL_K55tHdvYAyr2O2N08sb_xSvspiyk9eEJZZygkbCjeMgVztkiZKrON3Vw5S7JARZwmPsAXycE84zvjnaXy3NMVcmuAG8vNDeOZCmqoV9GeByxo1amsxTFEwIqZLFB6KWgN0GD-EltCw_W0uqQ15quM6Wmi-at2a-jBj9dRTt8CT-4K02IaIAcItVcoKTzj3Mq6tKvpwI72-elyheSPFPimrcZbakGgXnr3Z9yvi7fS90ot3sXoY4h9WCI1VMvDjv7aBbE-U6W7-zlF9-u3yIf7ZjJqLjS2C4ZTkivazgAkg";
    public static final String APPLICATIONTYPE = "application/json";


    public static String TOP_SEARCHES_URL = "http://34.221.94.79/api/v1/r/Searches?filter[order]=hits%20DESC&filter[limit]=10";
    public static String ALL_REWARDS_URL = "http://18.237.254.224/api/v1/r/Rewards?filter[where][status]=active&filter[limit]=10000";

//    public static String GET_WAITLIST_URL = "https://demo.highconnect.co/api/v1/r/Waitlists?filter[where][location]=fe65d3f5-6783-4d3d-b610-438abea433a3";
//    public static String GET_CURRENT_CUSTOMER_WAITLIST_URL = "https://demo.highconnect.co/api/v1/r/Waitlists?filter[where][location]=fe65d3f5-6783-4d3d-b610-438abea433a3";
    public static String WAITLIST_CHECKIN_URL = "https://demo.highconnect.co/api/v1/Customer/CheckInCustomer";
    public static String JOINED_WAITLIST_URL = "http://34.221.94.79/api/v1/Waitlist/JoinedWaitlist";
    public static String REGISTER_VENUE_CUSTOMER_URL = "http://18.237.254.224/api/v1/VenueCustomer/RegisterVenueCustomer";
    public static String REWARD_SIGNUP_URL = "http://34.221.94.79/api/v1/SignUp/RewardSignUp";


    public static String REWARD_CLAIM_URL = "http://18.237.254.224/api/v1/CustomerReward/ImpendCustomerReward";
    public static String LOG_REWARD_CLAIM_URL = "http://34.221.94.79/api/v1/Claim/RewardClaim";
}
