package com.sp.highconnect.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.Offer;

/**
 * Created by My 7 on 16-Aug-18.
 */

public class OfferDetailFragment extends Fragment
{
    private static final String KEY_POSITION="position";
    private static final String KEY_CONTEXT="ctx";

    static DashboardActivity mContext;
    
    static int pos;

    Offer mOffer;

    ImageView img_offer_detail;

    ProgressBar progress_offer_detail;

    TextView title_offer_detail,id_offer_detail,expire_offer_detail,description_offer_detail,terms_offer_detail;

    public static Fragment newInstance(DashboardActivity ctx, int position)
    {
        OfferDetailFragment fragment = new OfferDetailFragment();
        Bundle args=new Bundle();

        args.putInt(KEY_POSITION, position);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.item_detail_offer, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashboardActivity) getActivity();

        int position = getArguments().getInt(KEY_POSITION, -1);
        mOffer = BaseActivity.mOFFERS_LIST.get(position);

        initView(view);
    }

    private void initView(View view)
    {
        img_offer_detail = view.findViewById(R.id.img_offer_detail);

        progress_offer_detail = view.findViewById(R.id.progress_offer_detail);

        title_offer_detail = view.findViewById(R.id.title_offer_detail);
        id_offer_detail = view.findViewById(R.id.id_offer_detail);
        expire_offer_detail = view.findViewById(R.id.expire_offer_detail);
        description_offer_detail = view.findViewById(R.id.description_offer_detail);
        terms_offer_detail = view.findViewById(R.id.terms_offer_detail);

        setData();
    }

    private void setData()
    {
        title_offer_detail.setText(mOffer.getTitle());
        id_offer_detail.setText(mOffer.getCouponId());
        expire_offer_detail.setText(mOffer.getDatesActive().getEnd());
        description_offer_detail.setText(mOffer.getDescription());
        terms_offer_detail.setText(mOffer.getFinePrint());

        String path = "https://tripntvcontent.s3.us-west-2.amazonaws.com/"+mOffer.getImageUrl();

        Glide.with(mContext).load(path).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                progress_offer_detail.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progress_offer_detail.setVisibility(View.GONE);
                return false;
            }
        }).into(img_offer_detail);
    }
}
