package com.sp.highconnect.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.adapter.OfferPagerAdapter;
import com.sp.highconnect.common.BaseActivity;

/**
 * Created by My 7 on 16-Aug-18.
 */

public class OfferPagerFragment extends Fragment
{
    DashboardActivity mContext;

    LinearLayout layout_offer_pager_main,ll_offer_left,ll_offer_right,layoutDots_play;

    ImageView img_dispensary_logo_detail_offer;

    TextView txt_title_detail_offers;

    ViewPager viewPager_offer_detail;

    ImageView image_offer_left,image_offer_right;

    LinearLayout ll_dispensary_details;

    OfferPagerAdapter adapter;

    TextView[] dots;
    private int no_of_offers;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.detail_offer_slider, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        no_of_offers = BaseActivity.mOFFERS_LIST.size();

        initView(view);
    }

    private void initView(View view)
    {
        layout_offer_pager_main = view.findViewById(R.id.layout_offer_pager_main);
        mContext.overrideFonts(layout_offer_pager_main);

        ll_offer_left = view.findViewById(R.id.ll_offer_left);
        ll_offer_right = view.findViewById(R.id.ll_offer_right);
        layoutDots_play = view.findViewById(R.id.layoutDots_play);

        image_offer_left = view.findViewById(R.id.image_offer_left);
        image_offer_right = view.findViewById(R.id.image_offer_right);

        image_offer_left.setColorFilter(mContext.getResources().getColor(R.color.theme_color_light_brown));
        image_offer_right.setColorFilter(mContext.getResources().getColor(R.color.theme_color_light_brown));

        img_dispensary_logo_detail_offer = view.findViewById(R.id.img_dispensary_logo_detail_offer);

        txt_title_detail_offers = view.findViewById(R.id.txt_title_detail_offers);

        ll_dispensary_details = view.findViewById(R.id.ll_dispensary_details);

        viewPager_offer_detail = view.findViewById(R.id.viewPager_offer_detail);
        viewPager_offer_detail.addOnPageChangeListener(viewPagerPageChangeListener);

        addBottomDots(0);
        setData();
        setListener();
    }

    private void setData()
    {
        Glide.with(mContext).load(BaseActivity.dispensaryImage).into(img_dispensary_logo_detail_offer);

        txt_title_detail_offers.setText(BaseActivity.dispensaryName);

        adapter = new OfferPagerAdapter(getChildFragmentManager(),mContext,no_of_offers);
        viewPager_offer_detail.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    private void setListener()
    {
        ll_offer_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItemAtPosition(-1);
                if (current >= 0) {
                    viewPager_offer_detail.setCurrentItem(current);
                    changeview(viewPager_offer_detail.getCurrentItem());
                }
            }
        });

        ll_offer_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItemAtPosition(+1);
                if (current <= no_of_offers -1) {
                    viewPager_offer_detail.setCurrentItem(current);
                    changeview(viewPager_offer_detail.getCurrentItem());
                }
            }
        });

        ll_dispensary_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                BaseActivity.mDispensaryData = mList.get(position);
                mContext.replaceFragment(new DispensaryDetailFragment());

            }
        });
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            changeview(viewPager_offer_detail.getCurrentItem());
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addBottomDots(int currentPage) {

        dots = new TextView[no_of_offers];

        layoutDots_play.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(mContext);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.dot_light));
            layoutDots_play.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(getResources().getColor(R.color.dot_dark));

    }

    public int getItemAtPosition(int i) {
        return viewPager_offer_detail.getCurrentItem() + i;
    }

    public void changeview(int pager_no) {

        if (pager_no == 0) {

            image_offer_left.setVisibility(View.GONE);
            image_offer_right.setVisibility(View.VISIBLE);

        } else if (pager_no == no_of_offers - 1) {

            image_offer_left.setVisibility(View.VISIBLE);
            image_offer_right.setVisibility(View.GONE);

        } else {

            image_offer_left.setVisibility(View.VISIBLE);
            image_offer_right.setVisibility(View.VISIBLE);

        }
    }
}
