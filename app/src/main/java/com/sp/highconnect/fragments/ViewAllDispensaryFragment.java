package com.sp.highconnect.fragments;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.support.v4.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.adapter.AllDispensaryAdapter;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.DispensaryPojo;

import java.util.ArrayList;
import java.util.Collections;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class ViewAllDispensaryFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener{
    DashboardActivity mContext;

    RecyclerView rv_all_dispensary;
    FrameLayout rl_map_layout;
    LinearLayout ll_down, ll_up;
    private static boolean isCollapse = true;

    AllDispensaryAdapter mAllDispensaryAdapter;
    public ArrayList<DispensaryPojo> mDispensary = new ArrayList<>();
    boolean isFirst = true;

    MapView mMapView;
    GoogleMap mGoogleMap;
    private Marker mMarker;

    private View mCustomMarkerView;
    private CircleImageView mMarkerImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.all_dispensary_list, container, false);

        mMapView = (MapView) view.findViewById(R.id.map_all_dispensary);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        mCustomMarkerView = inflater.inflate(R.layout.view_custom_marker, null);
        mMarkerImageView = mCustomMarkerView.findViewById(R.id.profile_image);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        inItView(view);
        setList();
        SetClicks();

        if (BaseActivity.isFromMap == true) {

            expand(rl_map_layout);

        } else {

        }

    }

    private void inItView(View view) {
        rl_map_layout = view.findViewById(R.id.rl_map_layout);
        ll_down = view.findViewById(R.id.ll_down);
        ll_up = view.findViewById(R.id.ll_up);

        rv_all_dispensary = view.findViewById(R.id.rv_all_dispensary);
        rv_all_dispensary.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
    }

    private void setList() {

        mDispensary = new ArrayList<DispensaryPojo>(BaseActivity.mDISPENSARY_LIST);

        mAllDispensaryAdapter = new AllDispensaryAdapter(mContext, mDispensary);
        rv_all_dispensary.setAdapter(mAllDispensaryAdapter);


        ShortList(BaseActivity.mMarkerClickPosition);
    }

    private void SetClicks() {
        ll_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (BaseActivity.isFromMap == true) {

                    if (isCollapse == false) {

                        expand(rl_map_layout);

                    } else {

                        collapse(rl_map_layout);
                    }


                    isCollapse = !isCollapse;
                }

            }
        });

        ll_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (BaseActivity.isFromMap == true) {

                    if (isCollapse == false) {

                        expand(rl_map_layout);

                    } else {

                        collapse(rl_map_layout);
                    }


                    isCollapse = !isCollapse;
                }
            }
        });
    }

    public static void expand(final View v) {

        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);

        ValueAnimator va = ValueAnimator.ofInt(1, targetHeight);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (Integer) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        va.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                v.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            }

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        va.setDuration(300);
        va.setInterpolator(new OvershootInterpolator());
        va.start();
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        ValueAnimator va = ValueAnimator.ofInt(initialHeight, 0);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (Integer) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        va.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                v.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        va.setDuration(300);
        va.setInterpolator(new DecelerateInterpolator());
        va.start();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        mGoogleMap = googleMap;
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

//        mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);
//        mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);


        mGoogleMap.setOnMarkerClickListener(this);

        ploatMarkers();

    }

    private void ploatMarkers()
    {

        if(BaseActivity.mMAPS_MARKER_LIST.size() > 0){

            mGoogleMap.clear();

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//            builder.include(Latlng1);


//            com.sp.highconnect.model.VenueLocation latLong = BaseActivity.dispensary.get(i).getLocation();
//            BaseActivity.mapMarkers.get()

            for(int i = 0; i < BaseActivity.mMAPS_MARKER_LIST.size(); i++) //BaseActivity.mapMarkers.size()
            {
                double lat = Double.parseDouble(BaseActivity.mMAPS_MARKER_LIST.get(i).getCoordinates().get(1).toString());
                double lon = Double.parseDouble(BaseActivity.mMAPS_MARKER_LIST.get(i).getCoordinates().get(0).toString());
                Log.e("LOCATION","for :  "+i+"   Lat: "+lat+" Lon: "+lon);

                builder.include(new LatLng(lat, lon));

                createMarker(lat, lon, ""+i, "", BaseActivity.mDISPENSARY_LIST.get(i).getSmlogo());

              /*  mMarker =  mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).title(""+i).snippet("New Sydney LatLong"));

                if (isFirst) {

                    LatLng mLocation = new LatLng(lat, lon);

                    // For zooming automatically to the location of the marker
//                    CameraPosition cameraPosition = new CameraPosition.Builder().target(mLocation).zoom(12).build();
//                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    isFirst =! isFirst;
                }*/

            }

//            LatLngBounds bounds = builder.build();
//            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));

        }else {

            Log.e("ploatMarkers","ploatMarkers : response not get yet");

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    ploatMarkers();

                }
            }, 5000);
        }

    }

    protected void createMarker(double latitude, double longitude, String title, String snippet, String mUrlImg) {

        LatLng mLocation = null;

       /* mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
//                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet));
//                .icon(BitmapDescriptorFactory.fromResource(iconResID)));*/

        mMarker =  mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(title).snippet("New Sydney LatLong"));


        loadMarkerIcon(mMarker, mUrlImg);

        if (isFirst) {

            if(BaseActivity.isFromMap){
                mLocation = BaseActivity.mMarkerClickLatlong;
            }
            else{

                mLocation = new LatLng(latitude, longitude);
            }
//            LatLng mLocation = new LatLng(latitude, longitude);
//            LatLng mLocation = BaseActivity.mMarkerClickLatlong;

            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(mLocation).zoom(12).build();
            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            isFirst =! isFirst;
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

//        Toast.makeText(mContext, "onMarkerClick"+marker.getTitle(), Toast.LENGTH_SHORT).show();

//        BaseActivity.isFromMap = true;
//        mContext.replaceFragment(new ViewAllDispensaryFragment());


        ShortList(Integer.parseInt(marker.getTitle()));

        Log.e("onMarkerClick","size before anything  "+mDispensary.size());



//        mTopicsAdapter.notifyDataSetChanged();


        return true;
    }


    public void ShortList(int mPosiotion){

        Collections.rotate(BaseActivity.mDISPENSARY_LIST, 0);
        mDispensary.clear();

//        Log.e("onMarkerClick","size of "+BaseActivity.dispensary.size());

        Log.e("onMarkerClick","size of after clear  "+mDispensary.size());

//        mDispensary = BaseActivity.dispensary;
        mDispensary = new ArrayList<DispensaryPojo>(BaseActivity.mDISPENSARY_LIST);

        Log.e("onMarkerClick","size of after asign  "+mDispensary.size());

        Collections.swap(mDispensary,0, mPosiotion);

        AllDispensaryAdapter mTopicsAdapter = new AllDispensaryAdapter(mContext, mDispensary);
        rv_all_dispensary.setAdapter(mTopicsAdapter);

    }

    private void loadMarkerIcon(final Marker marker, String mUrlImg) {
//         = "Url_imagePath;
        Glide.with(this).load(mUrlImg)
                .asBitmap().fitCenter().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {

                if(bitmap!=null){
                    //  Bitmap circularBitmap = getRoundedCornerBitmap(bitmap, 150);
//                    Bitmap mBitmap = getCircularBitmap(bitmap);
//                    mBitmap = addBorderToCircularBitmap(mBitmap, 2, Color.WHITE,squareBitmapWidth);

                    BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(mCustomMarkerView, bitmap));
                    marker.setIcon(icon);
                }

            }
        });

    }

    private Bitmap getMarkerBitmapFromView(View view, Bitmap bitmap) {

        mMarkerImageView.setImageBitmap(bitmap);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = view.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        view.draw(canvas);

        return returnedBitmap;
    }
}
