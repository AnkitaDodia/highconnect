package com.sp.highconnect.fragments;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;

/**
 * Created by My 7 on 15-Aug-18.
 */

public class SignupFragment extends Fragment
{
    DashboardActivity mContext;

    LinearLayout layout_account_main;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_account, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashboardActivity) getActivity();
        inItView(view);
    }

    private void inItView(View view)
    {
        layout_account_main = view.findViewById(R.id.layout_account_main);
        mContext.overrideFonts(layout_account_main);
    }
}
