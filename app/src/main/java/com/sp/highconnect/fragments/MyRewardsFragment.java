package com.sp.highconnect.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.adapter.EarnedRewardsAdapter;
import com.sp.highconnect.adapter.EnrolledRewardsAdapter;
import com.sp.highconnect.apihelper.Constants;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.AllReawards;
import com.sp.highconnect.model.DispensaryPojo;
import com.sp.highconnect.model.EarnedRewards;
import com.sp.highconnect.model.EnrolledRewards;
import com.sp.highconnect.model.EnrolledRewardsHistory;
import com.sp.highconnect.model.MyEarnedRewards;
import com.sp.highconnect.model.MyRewardsHistory;
import com.sp.highconnect.model.RewardsHistory;
import com.sp.highconnect.model.User;
import com.sp.highconnect.util.SettingsPreferences;
import com.sp.highconnect.util.SpacesItemDecoration;
import com.thoughtbot.expandablerecyclerview.listeners.GroupExpandCollapseListener;
import com.thoughtbot.expandablerecyclerview.listeners.OnGroupClickListener;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class MyRewardsFragment extends Fragment {

    DashboardActivity mContext;
    ArrayList<EnrolledRewardsHistory> mEnrolledRewardsHistoryList = new ArrayList<>();
    ArrayList<EnrolledRewards> mEnrolledRewardsList = new ArrayList<>();
    ArrayList<RewardsHistory> mRewardsHistoryList = new ArrayList<>();

    ArrayList<EarnedRewards> mEarnedRewardsList = new ArrayList<>();
    ArrayList<MyEarnedRewards> mMyEarnedRewardsList = new ArrayList<>();


    ArrayList<MyRewardsHistory> mMyRewardsHistoryList = new ArrayList<>();

    RecyclerView rv_enrolled_rewards, rv_earned_rewards;
    EnrolledRewardsAdapter mEnrolledRewardsAdapter;
//    String RewardId = null;

    MyRewardsFragment mMyRewardsFragment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mMyRewardsFragment = this;

        return inflater.inflate(R.layout.fragment_my_rewards, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        rv_enrolled_rewards = view.findViewById(R.id.rv_enrolled_rewards);
        rv_enrolled_rewards.setNestedScrollingEnabled(false);
        rv_enrolled_rewards.setLayoutManager(new LinearLayoutManager(mContext));

        rv_earned_rewards = view.findViewById(R.id.rv_earned_rewards);
        rv_earned_rewards.setLayoutManager(new LinearLayoutManager(mContext));
        rv_earned_rewards.getLayoutManager().setMeasurementCacheEnabled(false);
        rv_earned_rewards.setNestedScrollingEnabled(false);
        rv_earned_rewards.addItemDecoration(new SpacesItemDecoration(8));



        requestEarneddReward();

    }

    private void requestEarneddReward() {

        try {
//            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            final String auth = "Bearer " + BaseActivity.fcmIdToken;
            String EARNED_REWARDS = "http://18.237.254.224/api/v1/r/CustomerRewards?filter[limit]=1000&filter[where][customer]=" + mUser.getCustomerId();
//                    +mUser.getCustomerId();


            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, EARNED_REWARDS, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("EARNED_REWARDS", "response " + response.toString());
//                            mContext.showWaitIndicator(false);

                            try {

                                if (response != null) {


                                    for (int i = 0; i < response.length(); i++) {

                                        JSONObject jresponse = response.getJSONObject(i);

                                        Log.e("EARNED_REWARDS", "availableStars  " + jresponse.get("rewardDescription").toString());
                                        Log.e("EARNED_REWARDS", "venue  " + jresponse.get("reward").toString());

                                        EarnedRewards mEarnedRewards = new EarnedRewards();

                                        mEarnedRewards.setRewardDescription(jresponse.get("rewardDescription").toString());
                                        mEarnedRewards.setRewardId(jresponse.get("reward").toString());
                                        mEarnedRewards.setVenueid(jresponse.get("venue").toString());

//                                        "customerrewardId": "fe65d3f5-6783-4d3d-b610-438abea433a3 - Ankita - j3twqpFwtn - 1537790504277",
//                                                "customer": "Ankita",
//                                                "impendDate": "1537790504277",
//                                                "reward": "j3twqpFwtn",
//                                                "status": "Pending",
//                                                "venue": "fe65d3f5-6783-4d3d-b610-438abea433a3",
//                                                "venueCustomer": "fe65d3f5-6783-4d3d-b610-438abea433a3 - Ankita",
//                                                "rewardDescription": "20% Off All Indica",
//                                                "time": "qnAzBnbfLQ",
//                                                "redeemCode": "qnAzBnbfLQ",
//                                                "budtender": "",
//                                                "redeemDate": ""

                                        mEarnedRewardsList.add(mEarnedRewards);
                                    }

                                    CreateEarnedrewardsData();
                                    requestEnrolledReward();


                                } else {

//                                    rl_earned_rewards.setVisibility(View.GONE);
                                }

                            } catch (Exception e) {
                                Log.e("EXCEPTION", "" + e.getMessage());
                            }

                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
//                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    private void requestEnrolledReward() {

        try {
//            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            final String auth = "Bearer " + BaseActivity.fcmIdToken;
            String ENROLLED_REWARDS = "http://18.237.254.224/api/v1/r/VenueCustomers?filter[limit]=1000&filter[where][customer]=" + mUser.getCustomerId();
//                    +mUser.getCustomerId();

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, ENROLLED_REWARDS, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("ENROLLED_REWARDS", "response " + response.toString());
//                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    for (int i = 0; i < response.length(); i++) {

                                        JSONObject jresponse = response.getJSONObject(i);

                                        Log.e("ENROLLED_REWARDS", "availableStars  " + jresponse.get("availableStars").toString());
                                        Log.e("ENROLLED_REWARDS", "venue  " + jresponse.get("venue").toString());

                                        EnrolledRewards mEnrolledRewards = new EnrolledRewards();

                                        mEnrolledRewards.setAvailableStars(jresponse.get("availableStars").toString());
                                        mEnrolledRewards.setVenueid(jresponse.get("venue").toString());


                                        mEnrolledRewardsList.add(mEnrolledRewards);
                                    }

                                    requestRewardhistory();

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
//                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void requestRewardhistory() {

        try {
            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

            String REWARDS_HISTORY = "http://18.237.254.224/api/v1/r/HistoryLog?filter[where][customer]=" + mUser.getCustomerId() + "&filter[limit]=1000&filter[where][eventType][inq][0]=VenueCustomerEarnedStars&filter[where][eventType][inq][1]=CustomerRewardRedeemed&filter[where][eventType][inq][2]=VenueCustomerRegistered";
//            AllTest1
//            mUser.getCustomerId()
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, REWARDS_HISTORY, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("REWARDS_HISTORY", "response " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    for (int i = 0; i < response.length(); i++) {

                                        JSONObject jresponse = response.getJSONObject(i);
                                        Log.e("REWARDS_HISTORY", "availableStars  " + jresponse.get("eventType").toString());

                                        RewardsHistory mRewardsHistory = new RewardsHistory();

                                        mRewardsHistory.setEventType(jresponse.get("eventType").toString());
                                        mRewardsHistory.setTime(jresponse.get("time").toString());
                                        mRewardsHistory.setReason(jresponse.get("reason").toString());
                                        mRewardsHistory.setBudtender(jresponse.get("budtender").toString());
                                        mRewardsHistory.setStars(jresponse.get("stars").toString());
                                        mRewardsHistory.setVenueid(jresponse.get("venue").toString());

                                        mRewardsHistoryList.add(mRewardsHistory);
                                    }

                                    CreateEnrolledRewardsHistoryData();
                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void requestCancelRewards() {

        try {
            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            Calendar cal = Calendar.getInstance();
            String time = String.valueOf(cal.getTimeInMillis());

            JSONObject params = new JSONObject();
            params.put("customerrewardId", BaseActivity.RewardLocationId+"-"+mUser.getCustomerId()+"-"+BaseActivity.RewardId+"-"+time);
            params.put("time", time);

            Log.e("time","time : "+time);

//            {
//                "customerrewardId":"fe65d3f5-6783-4d3d-b610-438abea433a3-g2g2g2 - j3twqpFwtn - 1537543074",
//                    "time":"1537543073"
//            }

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

//            Log.e("auth", auth);

            String CANCEL_REWARD_URL = "http://18.237.254.224/api/v1/CustomerReward/CancelCustomerReward";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, CANCEL_REWARD_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("CANCEL_REWARD", "RESPONSE" + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {


                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            mContext.showWaitIndicator(false);
            e.printStackTrace();
        }

    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    public void CreateEnrolledRewardsHistoryData() {

//        EnrolledRewardsHistory mEnrolledRewardsHistory = new EnrolledRewardsHistory();


        for (EnrolledRewards mEnrolledRewardsItem : mEnrolledRewardsList) {

//            mEnrolledRewardsItem.getVenueid();

            Log.e("mEnrolledRewardsItem", "" + mEnrolledRewardsItem.getVenueid());

            for (RewardsHistory mRewardsHistoryitem : mRewardsHistoryList) {


                Log.e("mRewardsHistoryitem", "" + mRewardsHistoryitem.getVenueid());

                if (mRewardsHistoryitem.getVenueid().equalsIgnoreCase(mEnrolledRewardsItem.getVenueid())) {

                    MyRewardsHistory mMyRewardsHistory = new MyRewardsHistory();

                    mMyRewardsHistory.setBudtender(mRewardsHistoryitem.getBudtender());
                    mMyRewardsHistory.setEventType(mRewardsHistoryitem.getEventType());
                    mMyRewardsHistory.setReason(mRewardsHistoryitem.getReason());
                    mMyRewardsHistory.setTime(mRewardsHistoryitem.getTime());
                    mMyRewardsHistory.setStars(mRewardsHistoryitem.getStars());

                    mMyRewardsHistoryList.add(mMyRewardsHistory);


                    for (DispensaryPojo mDispensaryItem : BaseActivity.mDISPENSARY_LIST) {

                        if (mRewardsHistoryitem.getVenueid().equalsIgnoreCase(mDispensaryItem.getLocationId())) {

//                            new EnrolledRewardsHistory(mDispensaryItem.getName(), mEnrolledRewardsItem.getAvailableStars(), mDispensaryItem.getLogo(), mMyRewardsHistoryList);
                            mEnrolledRewardsHistoryList.add(new EnrolledRewardsHistory(mDispensaryItem.getName(), mEnrolledRewardsItem.getAvailableStars(), mDispensaryItem.getSmlogo(), mMyRewardsHistoryList));
                        }

                    }


                }

            }

        }

        Collections.reverse(mEnrolledRewardsHistoryList);
        mEnrolledRewardsAdapter = new EnrolledRewardsAdapter(mContext, mEnrolledRewardsHistoryList);
        mEnrolledRewardsAdapter.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(int flatPos) {

//                mEnrolledRewardsAdapter.toggleGroup();

                for(DispensaryPojo item : BaseActivity.mDISPENSARY_LIST){

                    if(mEnrolledRewardsHistoryList.get(flatPos).getDispensaryname().equalsIgnoreCase(item.getName())){

                        BaseActivity.mDispensaryData = item;
                    }

                }

                mContext.replaceFragment(new DispensaryDetailFragment());
                return false;
            }
        });

      /*  mEnrolledRewardsAdapter.setOnGroupExpandCollapseListener(new GroupExpandCollapseListener() {
            @Override
            public void onGroupExpanded(ExpandableGroup group) {
                if (expandedGroup != null) {
                    mEnrolledRewardsAdapter.toggleGroup(expandedGroup);
                }
                expandedGroup = group;
            }

            @Override
            public void onGroupCollapsed(ExpandableGroup group) {

            }
        });*/

        rv_enrolled_rewards.setAdapter(mEnrolledRewardsAdapter);

    }

    private void CreateEarnedrewardsData() {

//        Dispensary smallogo name
//        Reawrd star image

        for (EarnedRewards mEarnedRewardsItem : mEarnedRewardsList) {

            MyEarnedRewards mMyEarnedRewards = new MyEarnedRewards();

            mMyEarnedRewards.setRewardDescription(mEarnedRewardsItem.getRewardDescription());

            for (DispensaryPojo mDispensaryItem : BaseActivity.mDISPENSARY_LIST) {

                if (mEarnedRewardsItem.getVenueid().equalsIgnoreCase(mDispensaryItem.getLocationId())) {

                    mMyEarnedRewards.setDispensaryName(mDispensaryItem.getName());
                    mMyEarnedRewards.setDispensaryLogo(mDispensaryItem.getSmlogo());
                    mMyEarnedRewards.setDispensaryId(mEarnedRewardsItem.getVenueid());

                }

            }

            for (AllReawards mAllReawardsItems : BaseActivity.mALL_REWARDS_LIST) {

                if (mEarnedRewardsItem.getRewardId().equalsIgnoreCase(mAllReawardsItems.getRewardId())) {

                    mMyEarnedRewards.setRewardLogo(mAllReawardsItems.getImageURL());
                    mMyEarnedRewards.setRewardsStar(mAllReawardsItems.getStarsRequired());
                    mMyEarnedRewards.setRewardId(mAllReawardsItems.getRewardId());

                    Log.e("EARNED_REWARDS_Id","RewardId : "+mAllReawardsItems.getRewardId());
//                    RewardId
                }

            }

            mMyEarnedRewardsList.add(mMyEarnedRewards);

        }

        EarnedRewardsAdapter mAdapter = new EarnedRewardsAdapter(mContext, mMyRewardsFragment, mMyEarnedRewardsList);
        rv_earned_rewards.setAdapter(mAdapter);
    }

    public void ShowCancelRewardsDialog(String RewardInfo, String DispansaryName, String RequiredStar) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setTitle("Cancel Rewards");
        builder1.setMessage("Are you sure you want to cancel "+RewardInfo+" at "+DispansaryName+"? If you cancel, "+RequiredStar+" star will be returend to you" );
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//
                        requestCancelRewards();

                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

}
