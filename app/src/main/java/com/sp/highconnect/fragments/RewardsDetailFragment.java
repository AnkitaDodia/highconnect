package com.sp.highconnect.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.apihelper.Constants;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.AllReawards;
import com.sp.highconnect.model.Offer;
import com.sp.highconnect.model.User;
import com.sp.highconnect.util.SettingsPreferences;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by My 7 on 16-Aug-18.
 */

public class RewardsDetailFragment extends Fragment
{
    private static final String KEY_POSITION="position";
    private static final String KEY_CONTEXT="ctx";

    static DashboardActivity mContext;
    
    static int pos;

    AllReawards mAllReawards;

    ImageView img_rewards_list;

    ProgressBar progress_rewards_list;

    TextView text_dispensary_name_list, text_rewards_tital_list, txt_rewards_required_star, txt_available_star;
//            , title_offer_detail,id_offer_detail,expire_offer_detail,description_offer_detail,terms_offer_detail;

    Button btn_claim_reward;

    public static Fragment newInstance(DashboardActivity ctx, int position)
    {
        RewardsDetailFragment fragment = new RewardsDetailFragment();
        Bundle args=new Bundle();

        args.putInt(KEY_POSITION, position);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.item_detail_rewards, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashboardActivity) getActivity();

        int position = getArguments().getInt(KEY_POSITION, -1);
        mAllReawards = BaseActivity.mDispensary_Rewards_list.get(position);

        initView(view);
    }

    private void initView(View view)
    {
        img_rewards_list = view.findViewById(R.id.img_rewards_list);

        progress_rewards_list = view.findViewById(R.id.progress_rewards_list);

        text_dispensary_name_list = view.findViewById(R.id.text_dispensary_name_list);
        text_rewards_tital_list = view.findViewById(R.id.text_rewards_tital_list);
        txt_rewards_required_star = view.findViewById(R.id.txt_rewards_required_star);

        txt_available_star = view.findViewById(R.id.txt_available_star);

        btn_claim_reward = view.findViewById(R.id.btn_claim_reward);

        btn_claim_reward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                requestClaimReward();
                ShowJoinRewardsDialog();
            }
        });

        setData();
    }

    private void setData()
    {
        text_dispensary_name_list.setText(BaseActivity.mDispensaryData.getName());
        text_rewards_tital_list.setText(mAllReawards.getDescription());

        txt_available_star.setText(""+BaseActivity.mAvailableStars);

        txt_rewards_required_star.setText(mAllReawards.getStarsRequired());

//        terms_offer_detail.setText(mOffer.getFinePrint());

        String path = mAllReawards.getImageURL();

        Glide.with(mContext).load(path).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                progress_rewards_list.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progress_rewards_list.setVisibility(View.GONE);
                return false;
            }
        }).into(img_rewards_list);
    }

    private void ShowJoinRewardsDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setTitle("Claim Rewards");
        builder1.setMessage("To Claim "+mAllReawards.getDescription()+" for "+mAllReawards.getStarsRequired()+" click Claim below and show budtender at " + BaseActivity.mDispensaryData.getName());
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Claim",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                        mContext.replaceFragment(new CustomerQueueFragment());

                        requestClaimReward();

                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void requestClaimReward() {

        try {
            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            Calendar cal = Calendar.getInstance();
            String time = String.valueOf(cal.getTimeInMillis());
//            data.getName()
            JSONObject params = new JSONObject();
            params.put("reward", mAllReawards.getRewardId());
            params.put("venueCustomer", BaseActivity.mDispensaryData.getLocationId()+"-"+mUser.getCustomerId());
            params.put("time", time);
            params.put("customerrewardId", BaseActivity.mDispensaryData.getLocationId()+"-"+mUser.getCustomerId()+"-"+mAllReawards.getRewardId()+"-"+time);

//            {"customerrewardId":"fe65d3f5-6783-4d3d-b610-438abea433a3-g2g2g2 - j3twqpFwtn - 1537543074",
//                    "venueCustomer":"fe65d3f5-6783-4d3d-b610-438abea433a3-g2g2g2",
//                    "reward":"j3twqpFwtn",
//                    "time":"1537543074"}'

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

            Log.e("ClaimReward", "JSONObject : " + params.toString());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Constants.REWARD_CLAIM_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("ClaimReward", "response " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    requestLogClaimRewardEvent();

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void requestLogClaimRewardEvent() {

        try {
            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            Calendar cal = Calendar.getInstance();
            String time = String.valueOf(cal.getTimeInMillis());
//            data.getName()
            JSONObject params = new JSONObject();
            params.put("claimId", BaseActivity.mDispensaryData.getLocationId());
            params.put("user", mUser.getCustomerId());
            params.put("time", time);
            params.put("lat", BaseActivity.currnet_latitude);
            params.put("long", BaseActivity.currnet_longitude);
            params.put("reward", mAllReawards.getDescription());

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

            Log.e("LogClaimReward", "JSONObject : " + params.toString());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Constants.LOG_REWARD_CLAIM_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("LogClaimReward", "response " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {


                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
