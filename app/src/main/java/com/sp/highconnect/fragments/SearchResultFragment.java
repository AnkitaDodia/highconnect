package com.sp.highconnect.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.adapter.AllDispensaryAdapter;
import com.sp.highconnect.adapter.AllSearchAdapter;
import com.sp.highconnect.adapter.DispensaryMenuAdapter;
import com.sp.highconnect.adapter.GroupMenuAdapter;
import com.sp.highconnect.adapter.OfferAdapter;
import com.sp.highconnect.adapter.TopSearchesAdapter;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.Cost;
import com.sp.highconnect.model.DispensaryMenu;
import com.sp.highconnect.model.DispensaryPojo;
import com.sp.highconnect.model.GroupMenu;
import com.sp.highconnect.model.HomeOffer;
import com.sp.highconnect.model.NearMe;
import com.sp.highconnect.model.SearchAll;
import com.sp.highconnect.model.TopSearches;
import com.sp.highconnect.utility.DividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import static android.widget.LinearLayout.VERTICAL;
import static com.sp.highconnect.apihelper.Constants.TOP_SEARCHES_URL;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class SearchResultFragment extends Fragment implements View.OnClickListener {

    private String TAG = "SearchResult";
    DashboardActivity mContext;
    String auth;

    private ArrayList<DispensaryPojo> mDispensary;
    public ArrayList<HomeOffer> mOffers;

    public static ArrayList<DispensaryMenu> mDispensaryMenu = new ArrayList<>();
    public static ArrayList<DispensaryMenu> mDispensaryMenuOri = new ArrayList<>();

    private ArrayList<GroupMenu> mGroupMenuList = new ArrayList<>();

    private String mSearchString = null;

    private RecyclerView rv_dis_search_result, rv_offer_search_result, rv_menu_search_result, rv_all_search_result, rv_top_search;
    private EditText edt_search;
    private LinearLayout ll_search_categories, ll_main_search_results, ll_main_top_searches, ll_menu_categories, ll_search_all, ll_search_dispensaries,
            ll_search_offers, ll_search_menu, ll_search_menu_location, ll_search_menu_price, ll_searches_clear;

    private AllDispensaryAdapter mAllDispensaryAdapter;
    private OfferAdapter mOfferAdapter;
    private DispensaryMenuAdapter mDispensaryMenuAdapter;
    private AllSearchAdapter mAllSearchAdapter;

    private TopSearchesAdapter mTopSearchesAdapter;

    private GroupMenuAdapter mGroupMenuAdapter;

    private int mSelDrawableMenu, mSelDrawableDispensary, mSelDrawableOffer;
    private boolean isCategorySelected = true;

    ArrayList<SearchAll> mSearchAllList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_result, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        initViews(view);

        mDispensary = new ArrayList<>(BaseActivity.mDISPENSARY_LIST);
        mOffers = new ArrayList<>(BaseActivity.mALL_HOME_OFFERS_LIST);
        mDispensaryMenu = new ArrayList<>(BaseActivity.mMENU_LIST);
        mDispensaryMenuOri = new ArrayList<>(BaseActivity.mMENU_LIST);

        mSearchAllList = new ArrayList<>();

        mAllDispensaryAdapter = new AllDispensaryAdapter(mContext, mDispensary);
        mOfferAdapter = new OfferAdapter(mContext, mOffers);
        mDispensaryMenuAdapter = new DispensaryMenuAdapter(mContext, mDispensaryMenu);
        mGroupMenuAdapter = new GroupMenuAdapter(mContext, mGroupMenuList);

        if (BaseActivity.mIsFromTopPicks == true) {

            BaseActivity.mIsFromTopPicks = false;

//            ll_main_search_results.setVisibility(View.VISIBLE);
//            ll_search_categories.setVisibility(View.VISIBLE);
//            ll_menu_categories.setVisibility(View.VISIBLE);
            edt_search.setText(BaseActivity.mToppickSearch);
            edt_search.setSelection(edt_search.getText().length());
        }


    }

    private void initViews(View view) {

        edt_search = view.findViewById(R.id.edt_search);


        rv_dis_search_result = view.findViewById(R.id.rv_dis_search_result);
        rv_dis_search_result.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        rv_offer_search_result = view.findViewById(R.id.rv_offer_search_result);
        rv_offer_search_result.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        rv_menu_search_result = view.findViewById(R.id.rv_menu_search_result);
        rv_menu_search_result.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        rv_all_search_result = view.findViewById(R.id.rv_all_search_result);
        rv_all_search_result.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        rv_top_search = view.findViewById(R.id.rv_top_search);
        DividerItemDecoration itemDecor = new DividerItemDecoration(mContext, VERTICAL);
        rv_top_search.addItemDecoration(itemDecor);
        rv_top_search.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        ll_main_search_results = view.findViewById(R.id.ll_main_search_results);
        ll_main_top_searches = view.findViewById(R.id.ll_main_top_searches);

        ll_search_categories = view.findViewById(R.id.ll_search_categories);
        ll_menu_categories = view.findViewById(R.id.ll_menu_categories);

        ll_search_all = view.findViewById(R.id.ll_search_all);
        ll_search_dispensaries = view.findViewById(R.id.ll_search_dispensaries);
        ll_search_offers = view.findViewById(R.id.ll_search_offers);
        ll_search_menu = view.findViewById(R.id.ll_search_menu);

        ll_search_menu_location = view.findViewById(R.id.ll_search_menu_location);
        ll_search_menu_price = view.findViewById(R.id.ll_search_menu_price);

        ll_searches_clear = view.findViewById(R.id.ll_searches_clear);

        ll_searches_clear.setOnClickListener(this);

        ll_search_all.setOnClickListener(this);
        ll_search_dispensaries.setOnClickListener(this);
        ll_search_offers.setOnClickListener(this);
        ll_search_menu.setOnClickListener(this);

        ll_search_menu_location.setOnClickListener(this);
        ll_search_menu_price.setOnClickListener(this);

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                mSearchString = editable.toString();
                Log.e(TAG, "mSearchString : " + mSearchString);

                if (TextUtils.isEmpty(editable.toString().trim())) {

                    ll_main_search_results.setVisibility(View.GONE);
                    ll_main_top_searches.setVisibility(View.VISIBLE);


                    if (BaseActivity.mTOP_SEARCHES_LIST.size() > 0) {

//                        mTopSearchesAdapter = new TopSearchesAdapter(mContext, BaseActivity.mTOP_SEARCHES_LIST);

                        SetTopSearchAdapter();

                    } else {
                        RequestTopSearches();
                    }

                    ll_searches_clear.setVisibility(View.INVISIBLE);

                } else {

                    ll_searches_clear.setVisibility(View.VISIBLE);

                    isCategorySelected = true;

                    mSelDrawableDispensary = R.drawable.btn_search_unselected;
                    mSelDrawableOffer = R.drawable.btn_search_unselected;
                    mSelDrawableMenu = R.drawable.btn_search_unselected;

                    ll_search_dispensaries.setBackground(getResources().getDrawable(mSelDrawableDispensary));
                    ll_search_offers.setBackground(getResources().getDrawable(mSelDrawableOffer));
                    ll_search_menu.setBackground(getResources().getDrawable(mSelDrawableMenu));

                    ll_search_dispensaries.setClickable(true);
                    ll_search_offers.setClickable(true);
                    ll_search_menu.setClickable(true);

                    ll_main_search_results.setVisibility(View.VISIBLE);
                    ll_main_top_searches.setVisibility(View.GONE);
                    ll_menu_categories.setVisibility(View.GONE);

                    if (editable.toString().trim().equalsIgnoreCase("all")) {
                        edt_search.setTextColor(ContextCompat.getColor(mContext, R.color.concentrates));
                        ll_search_all.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));

                        rv_all_search_result.setVisibility(View.VISIBLE);
                        rv_dis_search_result.setVisibility(View.GONE);
                        rv_offer_search_result.setVisibility(View.GONE);
                        rv_menu_search_result.setVisibility(View.GONE);

                        CreateAllData();

                    } else {
                        edt_search.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                        CreateSearchResult(mSearchString.toLowerCase());
                    }
                }


            }
        });

        mSelDrawableDispensary = R.drawable.btn_search_unselected;
        mSelDrawableOffer = R.drawable.btn_search_unselected;
        mSelDrawableMenu = R.drawable.btn_search_unselected;

//        ll_search_all.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));
//        ll_search_dispensaries.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));
//        ll_search_offers.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));
//        ll_search_menu.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));


        /*rv_top_search.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_top_search, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {


                mSearchString = BaseActivity.mTOP_SEARCHES_LIST.get(position).getSearchId().toString();

                edt_search.setText(mSearchString);
                edt_search.setSelection(edt_search.getText().length());

                *//*ll_main_search_results.setVisibility(View.VISIBLE);
                ll_main_top_searches.setVisibility(View.GONE);

                ll_menu_categories.setVisibility(View.GONE);

                if (mSearchString.trim().equalsIgnoreCase("all")) {

                    ll_search_all.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));

                    rv_all_search_result.setVisibility(View.VISIBLE);
                    rv_dis_search_result.setVisibility(View.GONE);
                    rv_offer_search_result.setVisibility(View.GONE);
                    rv_menu_search_result.setVisibility(View.GONE);

                    CreateAllData();

                } else {

                    CreateSearchResult(mSearchString.toLowerCase());
                }*//*

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/

        rv_all_search_result.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_all_search_result, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {


                if (mSearchAllList.get(position).type == 2) {

                    Log.e("mMergedSearchAllList", "mMergedSearchAllList : " + mSearchAllList.get(position).mHomeOffer.getOffer().getTitle());
//                    Toast.makeText(mContext, "You clicked offers", Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < BaseActivity.mDISPENSARY_LIST.size(); i++) {
                        if (BaseActivity.mDISPENSARY_LIST.get(i).getLocationId().equalsIgnoreCase(mSearchAllList.get(position).mHomeOffer.getOffer().getDispensaryId())) {
                            BaseActivity.dispensaryName = BaseActivity.mDISPENSARY_LIST.get(i).getName();
                            BaseActivity.dispensaryImage = BaseActivity.mDISPENSARY_LIST.get(i).getSmlogo();
                            BaseActivity.mOFFERS_LIST = BaseActivity.mDISPENSARY_LIST.get(i).getOffers();
                            BaseActivity.mDispensaryData = BaseActivity.mDISPENSARY_LIST.get(i);
                            Log.e("OFFER_SIZE", "" + BaseActivity.mOFFERS_LIST.size());
                            break;
                        }
                    }

                    BaseActivity.mOffersFromWhere = 1;
                    mContext.replaceFragment(new OfferPagerFragment());
//                    changeFragment();

                } else {

//                    Toast.makeText(mContext, "You clicked else", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        rv_offer_search_result.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_offer_search_result, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {


//                    Log.e("mMergedSearchAllList", "mMergedSearchAllList : " + mSearchAllList.get(position).mHomeOffer.getOffer().getTitle());
//                Toast.makeText(mContext, "You clicked offers", Toast.LENGTH_SHORT).show();

                for (int i = 0; i < BaseActivity.mDISPENSARY_LIST.size(); i++) {
                    if (BaseActivity.mDISPENSARY_LIST.get(i).getLocationId().equalsIgnoreCase(mOffers.get(position).getOffer().getDispensaryId())) {
                        BaseActivity.dispensaryName = BaseActivity.mDISPENSARY_LIST.get(i).getName();
                        BaseActivity.dispensaryImage = BaseActivity.mDISPENSARY_LIST.get(i).getSmlogo();
                        BaseActivity.mOFFERS_LIST = BaseActivity.mDISPENSARY_LIST.get(i).getOffers();
                        BaseActivity.mDispensaryData = BaseActivity.mDISPENSARY_LIST.get(i);
                        Log.e("OFFER_SIZE", "" + BaseActivity.mOFFERS_LIST.size());
                        break;
                    }
                }

                BaseActivity.mOffersFromWhere = 1;
                mContext.replaceFragment(new OfferPagerFragment());
//                    changeFragment();


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        edt_search.requestFocus();
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edt_search, InputMethodManager.SHOW_IMPLICIT);

        ll_search_menu_location.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));
        ll_search_menu_price.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));

    }

    private void SetTopSearchAdapter() {


        mTopSearchesAdapter = new TopSearchesAdapter(mContext, BaseActivity.mTOP_SEARCHES_LIST, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                if (view.getId() == R.id.ll_item_top_searches_cancel) {
//                    Toast.makeText(mContext, "ITEM PRESSED : " + String.valueOf(position), Toast.LENGTH_SHORT).show();

                    BaseActivity.mTOP_SEARCHES_LIST.remove(position);
                    mTopSearchesAdapter.notifyDataSetChanged();

                } else {
//                    Toast.makeText(mContext, "ROW PRESSED : " + String.valueOf(position), Toast.LENGTH_SHORT).show();

                    mSearchString = BaseActivity.mTOP_SEARCHES_LIST.get(position).getSearchId().toString();

                    edt_search.setText(mSearchString);
                    edt_search.setSelection(edt_search.getText().length());
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rv_top_search.setAdapter(mTopSearchesAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_search_all:

                ll_search_all.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));
                ll_search_dispensaries.setBackground(getResources().getDrawable(mSelDrawableDispensary));
                ll_search_offers.setBackground(getResources().getDrawable(mSelDrawableOffer));
                ll_search_menu.setBackground(getResources().getDrawable(mSelDrawableMenu));

                rv_all_search_result.setVisibility(View.VISIBLE);
                rv_dis_search_result.setVisibility(View.GONE);
                rv_offer_search_result.setVisibility(View.GONE);
                rv_menu_search_result.setVisibility(View.GONE);

                ll_menu_categories.setVisibility(View.GONE);

                CreateAllMergeData();
                mAllSearchAdapter.notifyDataSetChanged();


                break;

            case R.id.ll_search_dispensaries:

                ll_search_dispensaries.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));
                ll_search_all.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));
                ll_search_offers.setBackground(getResources().getDrawable(mSelDrawableOffer));
                ll_search_menu.setBackground(getResources().getDrawable(mSelDrawableMenu));

                rv_all_search_result.setVisibility(View.GONE);
                rv_dis_search_result.setVisibility(View.VISIBLE);
                rv_offer_search_result.setVisibility(View.GONE);
                rv_menu_search_result.setVisibility(View.GONE);

                ll_menu_categories.setVisibility(View.GONE);

//                setDispansaryData();
//                rv_dis_search_result.swapAdapter(mAllDispensaryAdapter, true);
//                rv_dis_search_result.setAdapter(mAllDispensaryAdapter);
                mAllDispensaryAdapter.notifyDataSetChanged();

                break;
            case R.id.ll_search_offers:

                ll_search_offers.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));
                ll_search_dispensaries.setBackground(getResources().getDrawable(mSelDrawableDispensary));
                ll_search_all.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));
                ll_search_menu.setBackground(getResources().getDrawable(mSelDrawableMenu));

                rv_all_search_result.setVisibility(View.GONE);
                rv_dis_search_result.setVisibility(View.GONE);
                rv_offer_search_result.setVisibility(View.VISIBLE);
                rv_menu_search_result.setVisibility(View.GONE);

                ll_menu_categories.setVisibility(View.GONE);

//                setOfferList();
//                rv_offer_search_result.swapAdapter(mOfferAdapter, true);
//                rv_offer_search_result.setAdapter(mOfferAdapter);
                mOfferAdapter.notifyDataSetChanged();

                break;
            case R.id.ll_search_menu:

                ll_search_menu.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));
                ll_search_dispensaries.setBackground(getResources().getDrawable(mSelDrawableDispensary));
                ll_search_offers.setBackground(getResources().getDrawable(mSelDrawableOffer));
                ll_search_all.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));

                rv_all_search_result.setVisibility(View.GONE);
                rv_dis_search_result.setVisibility(View.GONE);
                rv_offer_search_result.setVisibility(View.GONE);
                rv_menu_search_result.setVisibility(View.VISIBLE);

                ll_menu_categories.setVisibility(View.VISIBLE);

//                setMenuData();
//                rv_menu_search_result.swapAdapter(mGroupMenuAdapter, true);
//                mDispensaryMenuAdapter.notifyDataSetChanged();
//                rv_menu_search_result.setAdapter(mGroupMenuAdapter);
                mGroupMenuAdapter.notifyDataSetChanged();

//                rv_menu_search_result.swapAdapter(mDispensaryMenuAdapter, true);
//                mDispensaryMenuAdapter.notifyDataSetChanged();

                break;

            case R.id.ll_search_menu_location:

                ll_search_menu_location.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));
                ll_search_menu_price.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));

                CreateLocationMenuData();

                break;

            case R.id.ll_search_menu_price:

                ll_search_menu_location.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));
                ll_search_menu_price.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));

//                setMenuData();

                CreatePriceMenuData();

                break;

            case R.id.ll_searches_clear:

                edt_search.setText("");

                break;
        }
    }

    private void CreateSearchResult(String mSearchQuery) {

        mDispensary.clear();
        mOffers.clear();
        mDispensaryMenu.clear();
        mGroupMenuList.clear();
        mDispensaryMenuOri.clear();

        for (DispensaryPojo mDispensaryItem : BaseActivity.mDISPENSARY_LIST) {

            if (mDispensaryItem.getName().toLowerCase().contains(mSearchQuery)
                    || mDispensaryItem.getPhoneNumber().toLowerCase().contains(mSearchQuery)
                    || mDispensaryItem.getWebsite().toLowerCase().contains(mSearchQuery)
                    || mDispensaryItem.getDescription().toLowerCase().contains(mSearchQuery)
                    || mDispensaryItem.getAddress().toLowerCase().contains(mSearchQuery)) {

//                Log.e(TAG, "DispensaryPojo : " + mDispensaryItem.getName());
                mDispensary.add(mDispensaryItem);

            }

        }

        for (HomeOffer mOfferItem : BaseActivity.mALL_HOME_OFFERS_LIST) {

            if (mOfferItem.getOffer().getTitle().toLowerCase().contains(mSearchQuery)
                    || mOfferItem.getOffer().getFinePrint().toLowerCase().contains(mSearchQuery)
                    || mOfferItem.getOffer().getDatesActive().getEnd().toString().toLowerCase().contains(mSearchQuery)
                    || mOfferItem.getOffer().getDescription().toLowerCase().contains(mSearchQuery)) {

//                Log.e(TAG, "Offer : " + mOfferItem.getOffer().getTitle());

//                Log.e(TAG, "Offer Date : " + mOfferItem.getOffer().getFinePrint().toString());

                mOffers.add(mOfferItem);

            }
        }


        for (DispensaryPojo mDispensaryItem : BaseActivity.mDISPENSARY_LIST) {

            boolean isFirstTime = true;

            for (DispensaryMenu mMenuItem : mDispensaryItem.getDispensaryMenus()) {


                if (mMenuItem.getName().toLowerCase().contains(mSearchQuery)
                        || mMenuItem.getName().toLowerCase().contains(mSearchQuery)
                        || mMenuItem.getCategory().toLowerCase().contains(mSearchQuery)
                        || mMenuItem.getDescription().toLowerCase().contains(mSearchQuery)
                        ) {
//                    mGroupMenuList.contains(mDispensaryItem.getName())
                    if (isFirstTime) {

                        GroupMenu mGroupMenuItem2 = new GroupMenu();
                        mGroupMenuItem2.type = true;

                        NearMe mNearMe = new NearMe();
                        mNearMe.setTitle(mDispensaryItem.getName());

                        mGroupMenuItem2.mNearMe = mNearMe;
                        mGroupMenuItem2.mDispensaryMenu = null;

                        mGroupMenuList.add(mGroupMenuItem2);

//                        mGroupMenuList.get(0).mDispensaryMenu.getCost().get(0).getPrice()

                        Log.e(TAG, "Title mDispensaryItem : " + mDispensaryItem.getName());
                        Log.e(TAG, "Title mMenuItem : " + mMenuItem.getName());

                        isFirstTime = !isFirstTime;

                    }

                    GroupMenu mGroupMenuItem = new GroupMenu();

                    mGroupMenuItem.type = false;
                    mGroupMenuItem.mDispensaryMenu = mMenuItem;
                    mGroupMenuItem.mNearMe = null;

                    mGroupMenuList.add(mGroupMenuItem);
                    mDispensaryMenu.add(mMenuItem);

                    Log.e(TAG, "mDispensaryItem : " + mDispensaryItem.getName());
                    Log.e(TAG, "mMenuItem : " + mMenuItem.getName());
                }

                for (Cost mCostItem : mMenuItem.getCost()) {


                    if (mCostItem.getPrice().contains(mSearchQuery)
                            || mCostItem.getUnit().contains(mSearchQuery)) {

                        if (mGroupMenuList.contains(mCostItem)) {

                        } else {


                            if (!mGroupMenuList.contains(mDispensaryItem.getName())) {

                                GroupMenu mGroupMenuItem2 = new GroupMenu();
                                mGroupMenuItem2.type = true;

                                NearMe mNearMe = new NearMe();
                                mNearMe.setTitle(mDispensaryItem.getName());

                                mGroupMenuItem2.mNearMe = mNearMe;
                                mGroupMenuItem2.mDispensaryMenu = null;

                                mGroupMenuList.add(mGroupMenuItem2);

                                Log.e(TAG, "Title mDispensaryItem cost : " + mDispensaryItem.getName());
                                Log.e(TAG, "Title mMenuItem cost : " + mMenuItem.getName());
                            }

                            GroupMenu mGroupMenuItem = new GroupMenu();

                            mGroupMenuItem.type = false;
                            mGroupMenuItem.mDispensaryMenu = mMenuItem;
                            mGroupMenuItem.mNearMe = null;

                            mGroupMenuList.add(mGroupMenuItem);

                            mDispensaryMenu.add(mMenuItem);

                            Log.e(TAG, "mDispensaryItem : " + mDispensaryItem.getName());
                            Log.e(TAG, "mMenuItem : " + mMenuItem.getName());
                        }

//                        Log.e(TAG, "mCostItem : " + mCostItem.getPrice());
                    }
                }

            }

            isFirstTime = !isFirstTime;
        }

        Log.e(TAG, "Size of mDispensaryMenu" + mGroupMenuList.size());


        mDispensaryMenuOri = new ArrayList<>(mDispensaryMenu);

        Log.e(TAG, "mDispensaryMenuOri : " + mDispensaryMenuOri.size());

        mAllDispensaryAdapter.notifyDataSetChanged();
        mOfferAdapter.notifyDataSetChanged();
        mDispensaryMenuAdapter.notifyDataSetChanged();
        mGroupMenuAdapter.notifyDataSetChanged();

        CreateAllMergeData();

    }

    private void CreatePriceMenuData() {

        mGroupMenuList.clear();

        Collections.sort(mDispensaryMenu, new Comparator<DispensaryMenu>() {
            @Override
            public int compare(DispensaryMenu first, DispensaryMenu second) {
                return Double.compare(Double.parseDouble(first.getCost().get(0).getPrice()), Double.parseDouble(second.getCost().get(0).getPrice()));
            }
        });

        for (DispensaryMenu mItem : mDispensaryMenu) {

//            Log.e("DispensaryMenu", "DispensaryMenu item : " + mItem.getCost().get(0).getPrice());
//            Log.e("DispensaryMenu", "DispensaryMenu item : " + mItem.getDispensaryName());

            if (!mGroupMenuList.contains(mItem.getDispensaryName())) {

                GroupMenu mGroupMenuItem2 = new GroupMenu();
                mGroupMenuItem2.type = true;

                NearMe mNearMe = new NearMe();
                mNearMe.setTitle(mItem.getDispensaryName());

                mGroupMenuItem2.mNearMe = mNearMe;
                mGroupMenuItem2.mDispensaryMenu = null;

                mGroupMenuList.add(mGroupMenuItem2);

//                Log.e(TAG, "Title mDispensaryItem cost : " + mItem.getDispensaryName());
//                Log.e(TAG, "Title mMenuItem cost : " + mItem.getName());
            }

            GroupMenu mGroupMenuItem = new GroupMenu();

            mGroupMenuItem.type = false;
            mGroupMenuItem.mDispensaryMenu = mItem;
            mGroupMenuItem.mNearMe = null;

            mGroupMenuList.add(mGroupMenuItem);

//            Log.e(TAG, "mDispensaryItem : " + mItem.getDispensaryName());
//            Log.e(TAG, "mMenuItem : " + mItem.getName());
        }

        mGroupMenuAdapter.notifyDataSetChanged();

    }

    public boolean containsDispensaryName(String mDispensaryName) {
        if (mGroupMenuList.size() > 0) {
            for (GroupMenu o : mGroupMenuList) {
                if (o.mDispensaryMenu != null) {
                    if (o.mDispensaryMenu.getDispensaryName().equalsIgnoreCase(mDispensaryName)) {
                        return true;
                    }
                }

            }
//            return false;
        }

        return false;
    }

    private void CreateLocationMenuData() {

        mGroupMenuList.clear();

        Log.e(TAG, "mDispensaryMenuOri : " + mDispensaryMenuOri.size());

        for (DispensaryMenu mItem : mDispensaryMenuOri) {

            if (!containsDispensaryName(mItem.getDispensaryName())) {

                GroupMenu mGroupMenuItem2 = new GroupMenu();
                mGroupMenuItem2.type = true;

                NearMe mNearMe = new NearMe();
                mNearMe.setTitle(mItem.getDispensaryName());

                mGroupMenuItem2.mNearMe = mNearMe;
                mGroupMenuItem2.mDispensaryMenu = null;

                mGroupMenuList.add(mGroupMenuItem2);

                Log.e("CreateLocationMenuData", " if is not contain");

            } else {

                Log.e("CreateLocationMenuData", " else is contain");
            }

            GroupMenu mGroupMenuItem = new GroupMenu();

            mGroupMenuItem.type = false;
            mGroupMenuItem.mDispensaryMenu = mItem;
            mGroupMenuItem.mNearMe = null;

            mGroupMenuList.add(mGroupMenuItem);
        }

        mGroupMenuAdapter.notifyDataSetChanged();


    }


    public void CreateAllMergeData() {


//        ArrayList<SearchAll> mSearchAllList = new ArrayList<>();
        mSearchAllList.clear();

        if (mDispensary.size() > 0) {

            NearMe mNearMe = new NearMe();
            mNearMe.setTitle("DISPENSARIES");
            SearchAll mSearchAllTitle = new SearchAll();
            mSearchAllTitle.mDispensaryPojo = null;
            mSearchAllTitle.mHomeOffer = null;
            mSearchAllTitle.mDispensaryMenu = null;
            mSearchAllTitle.mNearMe = mNearMe;
            mSearchAllTitle.type = 0;
            mSearchAllList.add(mSearchAllTitle);

            for (DispensaryPojo item : mDispensary) {

                SearchAll mSearchAllDis = new SearchAll();

                mSearchAllDis.mDispensaryPojo = item;
                mSearchAllDis.mHomeOffer = null;
                mSearchAllDis.mDispensaryMenu = null;
                mSearchAllDis.mNearMe = null;
                mSearchAllDis.type = 1;
                mSearchAllList.add(mSearchAllDis);
            }

            if (isCategorySelected) {

//                mSelDrawableDispensary = R.drawable.btn_search_selected;

                rv_all_search_result.setVisibility(View.GONE);
                rv_dis_search_result.setVisibility(View.VISIBLE);
                rv_offer_search_result.setVisibility(View.GONE);
                rv_menu_search_result.setVisibility(View.GONE);

                ll_search_all.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));
                ll_search_dispensaries.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));
                ll_search_dispensaries.setClickable(true);
                isCategorySelected = !isCategorySelected;
            }

            rv_dis_search_result.setAdapter(mAllDispensaryAdapter);

        } else {
            mSelDrawableDispensary = R.drawable.ic_red_line;
            ll_search_dispensaries.setBackground(getResources().getDrawable(mSelDrawableDispensary));
            ll_search_dispensaries.setClickable(false);
        }


        if (mOffers.size() > 0) {

            NearMe mNearMe1 = new NearMe();
            mNearMe1.setTitle("OFFERS");
            SearchAll mSearchAllTitle1 = new SearchAll();
            mSearchAllTitle1.mDispensaryPojo = null;
            mSearchAllTitle1.mHomeOffer = null;
            mSearchAllTitle1.mDispensaryMenu = null;
            mSearchAllTitle1.mNearMe = mNearMe1;
            mSearchAllTitle1.type = 0;
            mSearchAllList.add(mSearchAllTitle1);

            for (HomeOffer item : mOffers) {

                SearchAll mSearchAllDis = new SearchAll();

                mSearchAllDis.mDispensaryPojo = null;
                mSearchAllDis.mHomeOffer = item;
                mSearchAllDis.mDispensaryMenu = null;
                mSearchAllDis.mNearMe = null;
                mSearchAllDis.type = 2;
                mSearchAllList.add(mSearchAllDis);
            }

            if (isCategorySelected) {

//                mSelDrawableOffer = R.drawable.btn_search_selected;

                rv_all_search_result.setVisibility(View.GONE);
                rv_dis_search_result.setVisibility(View.GONE);
                rv_offer_search_result.setVisibility(View.VISIBLE);
                rv_menu_search_result.setVisibility(View.GONE);

                ll_search_all.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));
                ll_search_offers.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));
                ll_search_offers.setClickable(true);
                isCategorySelected = !isCategorySelected;
            }

            rv_offer_search_result.setAdapter(mOfferAdapter);

        } else {
            mSelDrawableOffer = R.drawable.ic_red_line;
            ll_search_offers.setBackground(getResources().getDrawable(mSelDrawableOffer));
            ll_search_offers.setClickable(false);
        }

        if (mDispensaryMenu.size() > 0) {

            NearMe mNearMe2 = new NearMe();
            mNearMe2.setTitle("MENU ITEM");
            SearchAll mSearchAllTitle2 = new SearchAll();
            mSearchAllTitle2.mDispensaryPojo = null;
            mSearchAllTitle2.mHomeOffer = null;
            mSearchAllTitle2.mDispensaryMenu = null;
            mSearchAllTitle2.mNearMe = mNearMe2;
            mSearchAllTitle2.type = 0;
            mSearchAllList.add(mSearchAllTitle2);

            for (DispensaryMenu item : mDispensaryMenu) {

                SearchAll mSearchAllDis = new SearchAll();

                mSearchAllDis.mDispensaryPojo = null;
                mSearchAllDis.mHomeOffer = null;
                mSearchAllDis.mDispensaryMenu = item;
                mSearchAllDis.mNearMe = null;
                mSearchAllDis.type = 3;
                mSearchAllList.add(mSearchAllDis);
            }


            if (isCategorySelected) {

//                mSelDrawableMenu = R.drawable.btn_search_selected;

                rv_all_search_result.setVisibility(View.GONE);
                rv_dis_search_result.setVisibility(View.GONE);
                rv_offer_search_result.setVisibility(View.GONE);
                rv_menu_search_result.setVisibility(View.VISIBLE);

                ll_search_all.setBackground(getResources().getDrawable(R.drawable.btn_search_unselected));
                ll_search_menu.setBackground(getResources().getDrawable(R.drawable.btn_search_selected));
                ll_menu_categories.setVisibility(View.VISIBLE);
                ll_search_menu.setClickable(true);
                isCategorySelected = !isCategorySelected;
            }

            rv_menu_search_result.setAdapter(mGroupMenuAdapter);

        } else {
            mSelDrawableMenu = R.drawable.ic_red_line;
            ll_search_menu.setBackground(getResources().getDrawable(mSelDrawableMenu));
            ll_search_menu.setClickable(false);
        }

        mAllSearchAdapter = new AllSearchAdapter(mContext, mSearchAllList);
//        rv_all_search_result.swapAdapter(mAllSearchAdapter, true);
        rv_all_search_result.setAdapter(mAllSearchAdapter);

    }


    public void CreateAllData() {


        mSearchAllList.clear();

        if (mDispensary.size() > 0) {

            NearMe mNearMe = new NearMe();
            mNearMe.setTitle("DISPENSARIES");
            SearchAll mSearchAllTitle = new SearchAll();
            mSearchAllTitle.mDispensaryPojo = null;
            mSearchAllTitle.mHomeOffer = null;
            mSearchAllTitle.mDispensaryMenu = null;
            mSearchAllTitle.mNearMe = mNearMe;
            mSearchAllTitle.type = 0;
            mSearchAllList.add(mSearchAllTitle);

            for (DispensaryPojo item : mDispensary) {

                SearchAll mSearchAllDis = new SearchAll();

                mSearchAllDis.mDispensaryPojo = item;
                mSearchAllDis.mHomeOffer = null;
                mSearchAllDis.mDispensaryMenu = null;
                mSearchAllDis.mNearMe = null;
                mSearchAllDis.type = 1;
                mSearchAllList.add(mSearchAllDis);
            }


        }

        if (mOffers.size() > 0) {

            NearMe mNearMe1 = new NearMe();
            mNearMe1.setTitle("OFFERS");
            SearchAll mSearchAllTitle1 = new SearchAll();
            mSearchAllTitle1.mDispensaryPojo = null;
            mSearchAllTitle1.mHomeOffer = null;
            mSearchAllTitle1.mDispensaryMenu = null;
            mSearchAllTitle1.mNearMe = mNearMe1;
            mSearchAllTitle1.type = 0;
            mSearchAllList.add(mSearchAllTitle1);

            for (HomeOffer item : mOffers) {

                SearchAll mSearchAllDis = new SearchAll();

                mSearchAllDis.mDispensaryPojo = null;
                mSearchAllDis.mHomeOffer = item;
                mSearchAllDis.mDispensaryMenu = null;
                mSearchAllDis.mNearMe = null;
                mSearchAllDis.type = 2;
                mSearchAllList.add(mSearchAllDis);
            }


        }

        if (mDispensaryMenu.size() > 0) {

            NearMe mNearMe2 = new NearMe();
            mNearMe2.setTitle("MENU ITEM");
            SearchAll mSearchAllTitle2 = new SearchAll();
            mSearchAllTitle2.mDispensaryPojo = null;
            mSearchAllTitle2.mHomeOffer = null;
            mSearchAllTitle2.mDispensaryMenu = null;
            mSearchAllTitle2.mNearMe = mNearMe2;
            mSearchAllTitle2.type = 0;
            mSearchAllList.add(mSearchAllTitle2);

            for (DispensaryMenu item : mDispensaryMenu) {

                SearchAll mSearchAllDis = new SearchAll();

                mSearchAllDis.mDispensaryPojo = null;
                mSearchAllDis.mHomeOffer = null;
                mSearchAllDis.mDispensaryMenu = item;
                mSearchAllDis.mNearMe = null;
                mSearchAllDis.type = 3;
                mSearchAllList.add(mSearchAllDis);
            }

        }

        mAllSearchAdapter = new AllSearchAdapter(mContext, mSearchAllList);
//        rv_all_search_result.swapAdapter(mAllSearchAdapter, true);
        rv_all_search_result.setAdapter(mAllSearchAdapter);

    }

    public void RequestTopSearches() {
        try {
            mContext.showWaitIndicator(true);


            auth = "Bearer " + BaseActivity.fcmIdToken;

            Log.e("auth", auth);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, TOP_SEARCHES_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
//                            Log.e("Response", response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    for (int i = 0; i < response.length(); i++) {

                                        TopSearches mTopSearches = new TopSearches();
                                        JSONObject jresponse = response.getJSONObject(i);
                                        Log.e("RequestTopSearches ", "" + jresponse);

                                        if (i == 0) {
                                            mTopSearches.setSearchId("ALL");
                                            mTopSearches.setHits(jresponse.get("hits").toString());
                                        } else {
                                            mTopSearches.setSearchId(jresponse.get("searchId").toString());
                                            mTopSearches.setHits(jresponse.get("hits").toString());
                                        }

                                        BaseActivity.mTOP_SEARCHES_LIST.add(mTopSearches);
                                    }


                                    SetTopSearchAdapter();


                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
