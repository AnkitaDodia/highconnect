package com.sp.highconnect.fragments;


import android.support.v4.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.adapter.MyDispensaryAdapter;
import com.sp.highconnect.adapter.OfferAdapter;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.AllReawards;
import com.sp.highconnect.model.DispensaryPojo;
import com.sp.highconnect.model.HomeOffer;
import com.sp.highconnect.model.MyDispensary;
import com.sp.highconnect.model.NearMe;
import com.sp.highconnect.model.Offer;
import com.sp.highconnect.model.VenueLocation;
import com.sp.highconnect.restinterface.RestInterface;
import com.sp.highconnect.util.InternetStatus;
import com.sp.highconnect.util.SpacesItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.sp.highconnect.apihelper.Constants.ALL_REWARDS_URL;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class HomeFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    DashboardActivity mContext;
    private static String TAG = "HomeFragment";

    boolean isInternet;

    RecyclerView rv_my_dispensary, rv_offer;

    TextView txt_title_my_dispensaries, txt_title_show_all;

    LinearLayout layout_home_main;
    public ArrayList<DispensaryPojo> mDispensary = new ArrayList<>();
    public ArrayList<DispensaryPojo> mMyDispensary = new ArrayList<>();

    ArrayList<Offer> offerArray = new ArrayList<>();
//    ArrayList<HomeOffer> homeOffers = new ArrayList<>();
    boolean isFirst = true;

    MapView mMapView;
    GoogleMap mGoogleMap;
    private Marker mMarker;
    private LatLng mLocation;

    private View mCustomMarkerView;
    private CircleImageView mMarkerImageView;

//    private DispensaryAdapter mDispensaryAdapter;
    private MyDispensaryAdapter mMyDispensaryAdapter;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mMapView = (MapView) view.findViewById(R.id.mapview);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        mCustomMarkerView = inflater.inflate(R.layout.view_custom_marker, null);
        mMarkerImageView = mCustomMarkerView.findViewById(R.id.profile_image);

        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        inItView(view);
        setListener();

        if (BaseActivity.mDISPENSARY_LIST.size() == 0) {

            if (isInternet) {
                sendDispensaryRequest();
                RequestAllRewards();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        } else {

            BaseActivity.mMAPS_MARKER_LIST.clear();
            BaseActivity.mALL_HOME_OFFERS_LIST.clear();
            offerArray.clear();
            mDispensary = new ArrayList<>(BaseActivity.mDISPENSARY_LIST);

//            setListData(mDispensary);

            for (int i = 0; i < BaseActivity.mDISPENSARY_LIST.size(); i++) {
                offerArray = BaseActivity.mDISPENSARY_LIST.get(i).getOffers();
//                BaseActivity.mALL_OFFERS_LIST.add(offerArray);

                VenueLocation latLong = BaseActivity.mDISPENSARY_LIST.get(i).getLocation();
                BaseActivity.mMAPS_MARKER_LIST.add(latLong);
//                            Log.e("BaseActivity.mapMarkers", "" + BaseActivity.mapMarkers.size());


                if (offerArray.size() > 0) {
                    for (int j = 0; j < offerArray.size(); j++) {
//                                    Log.e("OFFER_NAME", "" + offerArray.get(j).getTitle());

                        HomeOffer data = new HomeOffer();
                        data.setDispensaryImage(BaseActivity.mDISPENSARY_LIST.get(i).getSmlogo());
                        data.setDispensaryName(BaseActivity.mDISPENSARY_LIST.get(i).getName());
                        data.setLocation(BaseActivity.mDISPENSARY_LIST.get(i).getLocation());

                        data.setOffer(offerArray.get(j));

                        BaseActivity.mALL_HOME_OFFERS_LIST.add(data);
                    }
                }
            }

            setOfferList(BaseActivity.mALL_HOME_OFFERS_LIST);
            SetMyDispensary();
//            GetDistance();
        }

    }

    private void inItView(View view) {

        txt_title_my_dispensaries = view.findViewById(R.id.txt_title_my_dispensaries);
        txt_title_show_all = view.findViewById(R.id.txt_title_show_all);

        txt_title_my_dispensaries.setText(BaseActivity.title_my_dispensaries);


        rv_my_dispensary = view.findViewById(R.id.rv_my_dispensary);
        rv_my_dispensary.setHasFixedSize(true);
        rv_my_dispensary.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

//        rv_my_dispensary.setNestedScrollingEnabled(false);

        rv_offer = view.findViewById(R.id.rv_offer);
        rv_offer.setNestedScrollingEnabled(false);
        rv_offer.addItemDecoration(new SpacesItemDecoration(8));



        layout_home_main = view.findViewById(R.id.layout_home_main);
        mContext.overrideFonts(layout_home_main);
    }

    private void setListener() {
        txt_title_show_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.isFromMap = false;

                mContext.replaceFragment(new ViewAllDispensaryFragment());
            }
        });

        rv_offer.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_offer, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                for (int i = 0; i < BaseActivity.mDISPENSARY_LIST.size(); i++) {
                    if (BaseActivity.mDISPENSARY_LIST.get(i).getLocationId().equalsIgnoreCase(BaseActivity.mALL_HOME_OFFERS_LIST.get(position).getOffer().getDispensaryId())) {
                        BaseActivity.dispensaryName = BaseActivity.mDISPENSARY_LIST.get(i).getName();
                        BaseActivity.dispensaryImage = BaseActivity.mDISPENSARY_LIST.get(i).getSmlogo();
                        BaseActivity.mOFFERS_LIST = BaseActivity.mDISPENSARY_LIST.get(i).getOffers();
                        BaseActivity.mDispensaryData = BaseActivity.mDISPENSARY_LIST.get(i);
                        Log.e("OFFER_SIZE", "" + BaseActivity.mOFFERS_LIST.size());
                        break;
                    }
                }

                BaseActivity.mOffersFromWhere = 0;
                mContext.replaceFragment(new OfferPagerFragment());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

   /* private void changeFragment() {
        OfferPagerFragment detailsFragment = new OfferPagerFragment();
        android.support.v4.app.FragmentManager fragmentManager = mContext.getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, detailsFragment);
        fragmentTransaction.commit();
    }*/

    private void sendDispensaryRequest() {
        try {
            String lat = String.valueOf(BaseActivity.currnet_latitude);
            String longi = String.valueOf(BaseActivity.currnet_latitude);
            String maxDistance = "1000000";
            String auth = "Bearer " + BaseActivity.fcmIdToken;

            Log.e("auth", "" + auth);

            Log.e("DISPENSARY","DISPENSARY : lat  : "+lat+" longi : "+longi);

            mContext.showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.LOCATION_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendDispensaryRequest(lat, longi, maxDistance, auth, new Callback<ArrayList<DispensaryPojo>>() {

                @Override
                public void success(ArrayList<DispensaryPojo> dispensaryPojos, Response response) {
                    if (response.getStatus() == 200) {
                        Log.e("DISPENSARY", "" + new Gson().toJson(dispensaryPojos.get(0)));
                        mContext.showWaitIndicator(false);
                        BaseActivity.mDISPENSARY_LIST = dispensaryPojos;
                        mDispensary = new ArrayList<>(BaseActivity.mDISPENSARY_LIST);

                        setListData(mDispensary);

                        for (int i = 0; i < dispensaryPojos.size(); i++) {

                            offerArray = dispensaryPojos.get(i).getOffers();
//                            BaseActivity.mALL_OFFERS_LIST.add(offerArray);

                            VenueLocation latLong = dispensaryPojos.get(i).getLocation();
                            BaseActivity.mMAPS_MARKER_LIST.add(latLong);
//                            Log.e("BaseActivity.mapMarkers", "" + BaseActivity.mapMarkers.size());


                            if (offerArray.size() > 0) {
                                for (int j = 0; j < offerArray.size(); j++) {
//                                    Log.e("OFFER_NAME", "" + offerArray.get(j).getTitle());

                                    HomeOffer data = new HomeOffer();
                                    data.setDispensaryImage(dispensaryPojos.get(i).getSmlogo());
                                    data.setDispensaryName(dispensaryPojos.get(i).getName());
                                    data.setLocation(dispensaryPojos.get(i).getLocation());

                                    data.setOffer(offerArray.get(j));

                                    BaseActivity.mALL_HOME_OFFERS_LIST.add(data);
                                }
                            }
                        }


                        setOfferList(BaseActivity.mALL_HOME_OFFERS_LIST);
                        SetMyDispensary();

                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    mContext.showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SetMyDispensary() {

        BaseActivity.MyDispensary.clear();
        mMyDispensary.clear();
        BaseActivity.MyDispensary = mContext.loadSharedPreferencesLogList(mContext);

//        Log.e("MyDispensary","SetMyDispensary : "+BaseActivity.MyDispensary.size());

        Log.e("MyDispensary","before mDispensary : "+mDispensary.size());
        Log.e("MyDispensary","before dispensary : "+BaseActivity.mDISPENSARY_LIST.size());

//        if(BaseActivity.MyDispensary.size() > 0){

            rv_my_dispensary.setVisibility(View.VISIBLE);

            for(int i = 0; i < BaseActivity.MyDispensary.size() ; i++){

                Log.e("MyDispensary","SetMyDispensary : "+BaseActivity.MyDispensary.get(i));

                for (Iterator<DispensaryPojo> iterator = mDispensary.iterator(); iterator.hasNext(); ) {
                    DispensaryPojo obj = iterator.next();
                    if (obj.getId().equals(BaseActivity.MyDispensary.get(i))) {
                        // Remove the current element from the iterator and the list.
                        iterator.remove();
                        mMyDispensary.add(obj);

                    }
                }

            }

//        }
//        else {
//
//            rv_my_dispensary.setVisibility(View.GONE);
//
//        }

        Log.e("MyDispensary","after mDispensary : "+mDispensary.size());
        Log.e("MyDispensary","after dispensary : "+BaseActivity.mDISPENSARY_LIST.size());

        Log.e("MyDispensary","mMyDispensary : "+mMyDispensary.size());

//        mMyDispensaryAdapter = new MyDispensaryAdapter(mContext, mMyDispensary);
//        rv_my_dispensary.setAdapter(mMyDispensaryAdapter);
        MergerData(mMyDispensary, mDispensary);
//        mDispensaryAdapter.notifyDataSetChanged();

    }

    public void MergerData(ArrayList<DispensaryPojo> mMyDispensary, ArrayList<DispensaryPojo> mDispensary){

        ArrayList<MyDispensary> mMergeDataList = new ArrayList<>();

        if(mMyDispensary.size()!= 0 ){

            NearMe mNearMe = new NearMe();
            mNearMe.setTitle(getResources().getString(R.string.my_dispensary_section));

            for(DispensaryPojo item : mMyDispensary){

                MyDispensary mMergeData2 = new MyDispensary();

                mMergeData2.mDispensaryPojo = item;
                mMergeData2.mNearMe = null;
                mMergeData2.type = true;
                mMergeDataList.add(mMergeData2);
            }

            MyDispensary mMergeData = new MyDispensary();

            mMergeData.mDispensaryPojo = null;
            mMergeData.mNearMe = mNearMe;
            mMergeData.type = false;

            mMergeDataList.add(mMergeData);

            BaseActivity.title_my_dispensaries = "MY DISPENSARIES";
            txt_title_my_dispensaries.setText(BaseActivity.title_my_dispensaries);
        }else {

            BaseActivity.title_my_dispensaries = "DISPENSARIES";
            txt_title_my_dispensaries.setText(BaseActivity.title_my_dispensaries);
        }

        for(DispensaryPojo item : mDispensary){

            MyDispensary mMergeData2 = new MyDispensary();

            mMergeData2.mDispensaryPojo = item;
            mMergeData2.mNearMe = null;
            mMergeData2.type = true;
            mMergeDataList.add(mMergeData2);
        }


        mMyDispensaryAdapter = new MyDispensaryAdapter(mContext, mMergeDataList);
        rv_my_dispensary.setAdapter(mMyDispensaryAdapter);
    }


    private void setListData(ArrayList<DispensaryPojo> dispensaryPojos) {

//        mDispensaryAdapter = new DispensaryAdapter(mContext, dispensaryPojos);
//        rv_dispensary.setAdapter(mDispensaryAdapter);

    }

    private void setOfferList(ArrayList<HomeOffer> mOfferList) {
        OfferAdapter mAdapter = new OfferAdapter(mContext, mOfferList);

        rv_offer.setLayoutManager(new LinearLayoutManager(mContext));
        rv_offer.getLayoutManager().setMeasurementCacheEnabled(false);
        rv_offer.setAdapter(mAdapter);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {


        mGoogleMap = googleMap;
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

        mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);
        mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);

        mGoogleMap.setOnMarkerClickListener(this);

        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng arg0)
            {
//                android.util.Log.i("onMapClick", "Horray!");
//                Toast.makeText(mContext, "onMapClick", Toast.LENGTH_LONG).show();

//                for (int i = 0; i < BaseActivity.mDISPENSARY_LIST.size(); i++) {
//
//                    if (BaseActivity.mDISPENSARY_LIST.get(i).getName().equalsIgnoreCase(BaseActivity.mDISPENSARY_LIST.get(0).getName())) {
//
//                        BaseActivity.mMarkerClickPosition = 0;
//                    }
//                }

                BaseActivity.mMarkerClickPosition = 0;
                BaseActivity.isFromMap = true;
                BaseActivity.mMarkerClickLatlong = mLocation;
                mContext.replaceFragment(new ViewAllDispensaryFragment());

            }
        });

        ploatMarkers();

    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

//        Toast.makeText(mContext, "" + marker.getTitle(), Toast.LENGTH_SHORT).show();
        for (int i = 0; i < BaseActivity.mDISPENSARY_LIST.size(); i++) {

            if (BaseActivity.mDISPENSARY_LIST.get(i).getName().equalsIgnoreCase(marker.getTitle())) {

                BaseActivity.mMarkerClickPosition = i;
            }
        }

        BaseActivity.isFromMap = true;
        BaseActivity.mMarkerClickLatlong = marker.getPosition();
        mContext.replaceFragment(new ViewAllDispensaryFragment());


        return true;
    }

    private void ploatMarkers() {

        // For dropping a marker at a point on the Map
        /*LatLng sydney = new LatLng(Double.parseDouble("-34"), Double.parseDouble("151"));
        mGoogleMap.addMarker(new MarkerOptions().position(sydney).title("Sydney").snippet("Sydney Is Awesome Place"));
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble("-33.5"), Double.parseDouble("152"))).title("Sydney").snippet("New Sydney LatLong"));
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble("-33"), Double.parseDouble("151.50"))).title("Sydney").snippet("New Sydney LatLong"));

        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(4).build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/

        if (BaseActivity.mMAPS_MARKER_LIST.size() > 0) {

            mGoogleMap.clear();

            for (int i = 0; i < BaseActivity.mMAPS_MARKER_LIST.size(); i++) //BaseActivity.mapMarkers.size()
            {
                double lat = Double.parseDouble(BaseActivity.mMAPS_MARKER_LIST.get(i).getCoordinates().get(1).toString());
                double lon = Double.parseDouble(BaseActivity.mMAPS_MARKER_LIST.get(i).getCoordinates().get(0).toString());
                Log.e("LOCATION", "for :  " + i + "   Lat: " + lat + " Lon: " + lon);

//            mLocation = new LatLng(lat, lon);
//            mGoogleMap.addMarker(new MarkerOptions().position(mLocation).title("Sydney "+i).snippet("Sydney LatLong "+i));

//                DispensaryPojo data = mList.get(position);

                createMarker(lat, lon, BaseActivity.mDISPENSARY_LIST.get(i).getName(), "", BaseActivity.mDISPENSARY_LIST.get(i).getSmlogo());

            }
        } else {

            Log.e("ploatMarkers", "ploatMarkers : response not get yet");

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    ploatMarkers();

                }
            }, 5000);
        }


    }

    protected void createMarker(double latitude, double longitude, String title, String snippet, String mUrlImg) {


       /* mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
//                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet));
//                .icon(BitmapDescriptorFactory.fromResource(iconResID)));*/

        mMarker = mGoogleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .title(title)
//                .snippet("New Sydney LatLong")


        );

        loadMarkerIcon(mMarker, mUrlImg);

        if (isFirst) {

            mLocation = new LatLng(latitude, longitude);

            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(mLocation).zoom(4).build();
            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            isFirst = !isFirst;
        }


    }

    private void loadMarkerIcon(final Marker marker, String mUrlImg) {
//         = "Url_imagePath;
        Glide.with(this).load(mUrlImg)
                .asBitmap().fitCenter().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {

                if (bitmap != null) {
                    //  Bitmap circularBitmap = getRoundedCornerBitmap(bitmap, 150);
//                    Bitmap mBitmap = getCircularBitmap(bitmap);
//                    mBitmap = addBorderToCircularBitmap(mBitmap, 2, Color.WHITE,squareBitmapWidth);

                    BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(mCustomMarkerView, bitmap));
                    marker.setIcon(icon);
                }

            }
        });

    }

    private Bitmap getMarkerBitmapFromView(View view, Bitmap bitmap) {

        mMarkerImageView.setImageBitmap(bitmap);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = view.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        view.draw(canvas);

        return returnedBitmap;
    }

    private void RequestAllRewards(){
        try {

//            mContext.showWaitIndicator(true);

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

            Log.e("auth", auth);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, ALL_REWARDS_URL, null,
                    new com.android.volley.Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray  response) {
//                            Log.e("Response", response.toString());
//                            mContext.showWaitIndicator(false);

                            Log.e("RequestAllRewards ", "" + response.toString());

                            if (response != null) {

                                try {

                                    for(int i=0; i < response.length() ; i++){

                                        AllReawards mAllReawards = new AllReawards();
                                        JSONObject jresponse = response.getJSONObject(i);
                                        Log.e("RequestAllRewards ", "" + jresponse);


                                        mAllReawards.setRewardId(jresponse.get("rewardId").toString());
                                        mAllReawards.setCreationDate(jresponse.get("creationDate").toString());
                                        mAllReawards.setDescription(jresponse.get("description").toString());
                                        mAllReawards.setImageURL(jresponse.get("imageURL").toString());
                                        mAllReawards.setRedeemCode(jresponse.get("redeemCode").toString());
                                        mAllReawards.setRedeemLimit(jresponse.get("redeemLimit").toString());
                                        mAllReawards.setStarsRequired(jresponse.get("starsRequired").toString());
                                        mAllReawards.setVenue(jresponse.get("venue").toString());
                                        mAllReawards.setStatus(jresponse.get("status").toString());
                                        mAllReawards.setFinePrint(jresponse.get("finePrint").toString());

                                        BaseActivity.mALL_REWARDS_LIST.add(mAllReawards);
                                    }

//                                    mTopSearchesAdapter = new TopSearchesAdapter(mContext, BaseActivity.mTOP_SEARCHES_LIST);
//                                    rv_top_search.setAdapter(mTopSearchesAdapter);


                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
//                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }



    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}