package com.sp.highconnect.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.adapter.DispensaryRewardsAdapter;
import com.sp.highconnect.adapter.OfferAdapter;
import com.sp.highconnect.apihelper.Constants;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.AllReawards;
import com.sp.highconnect.model.DispensaryPojo;
import com.sp.highconnect.model.HomeOffer;
import com.sp.highconnect.model.Offer;
import com.sp.highconnect.model.User;
import com.sp.highconnect.model.Waitlist;
import com.sp.highconnect.util.SettingsPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by My 7 on 15-Aug-18.
 */

public class DispensaryDetailFragment extends Fragment implements OnMapReadyCallback {
    DashboardActivity mContext;

    DispensaryPojo data;

    LinearLayout layout_dispensary_detail_main, layout_check_in, layout_menu, layout_my_dispensary;

    ImageView image_dispensary_detail;

    TextView text_detail_dispensary_name, text_detail_distance, text_dispensary_des, text_detail_call, text_adderess, text_website;
    TextView text_monday, text_tuesday, text_wednesday, text_thursday, text_friday, text_saturday, text_sunday,
            txt_title_dispancary_rewards, txt_title_dispancary_offers, text_menu_title, txt_my_dispensary, txt_dispensary_available_star, txt_rewards_discription, txt_check_in;

    Button btn_join_rewards;
    private boolean isJoinedRewards = false;

    protected static final int REQUEST_CALL_PHONE_ACCESS_PERMISSION = 101;

    RecyclerView rv_all_dispensary_offer, rv_dispensary_rewards;

    String dis;

    double latitude, longitude;
    MapView mMapView;
    GoogleMap mGoogleMap;
    private Marker mMarker;

    LinearLayout ll_detail_call, ll_details_address, ll_detail_website;

    private View mCustomMarkerView;
    private CircleImageView mMarkerImageView;


    private boolean isWaitlistAvailable = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_dispensary, container, false);


        mMapView = (MapView) view.findViewById(R.id.map_details_dispensary);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        mCustomMarkerView = inflater.inflate(R.layout.view_custom_marker, null);
        mMarkerImageView = mCustomMarkerView.findViewById(R.id.profile_image);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        data = BaseActivity.mDispensaryData;

        inItView(view);

    }

    private void inItView(View view) {
        layout_dispensary_detail_main = view.findViewById(R.id.layout_dispensary_detail_main);
        mContext.overrideFonts(layout_dispensary_detail_main);

        rv_all_dispensary_offer = view.findViewById(R.id.rv_all_dispensary_offer);
        rv_dispensary_rewards = view.findViewById(R.id.rv_dispensary_rewards);
        rv_dispensary_rewards.setHasFixedSize(true);


        rv_dispensary_rewards.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_dispensary_rewards, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                if(isJoinedRewards){

                    mContext.replaceFragment(new RewardsPagerFragment());

                }else {

                    Toast.makeText(mContext, "Please Join Rewards First", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        image_dispensary_detail = view.findViewById(R.id.image_dispensary_detail);

        text_detail_dispensary_name = view.findViewById(R.id.text_detail_dispensary_name);
        text_detail_distance = view.findViewById(R.id.text_detail_distance);
        text_dispensary_des = view.findViewById(R.id.text_dispensary_des);
        text_detail_call = view.findViewById(R.id.text_detail_call);
        text_adderess = view.findViewById(R.id.text_adderess);
        text_website = view.findViewById(R.id.text_website);

        txt_title_dispancary_offers = view.findViewById(R.id.txt_title_dispancary_offers);
        txt_title_dispancary_rewards = view.findViewById(R.id.txt_title_dispancary_rewards);

        txt_check_in = view.findViewById(R.id.txt_check_in);
        text_menu_title = view.findViewById(R.id.text_menu_title);
        txt_my_dispensary = view.findViewById(R.id.txt_my_dispensary);
        txt_rewards_discription = view.findViewById(R.id.txt_rewards_discription);
        txt_dispensary_available_star = view.findViewById(R.id.txt_dispensary_available_star);

        text_monday = view.findViewById(R.id.text_monday);
        text_tuesday = view.findViewById(R.id.text_tuesday);
        text_wednesday = view.findViewById(R.id.text_wednesday);
        text_thursday = view.findViewById(R.id.text_thursday);
        text_friday = view.findViewById(R.id.text_friday);
        text_saturday = view.findViewById(R.id.text_saturday);
        text_sunday = view.findViewById(R.id.text_sunday);

        layout_check_in = view.findViewById(R.id.layout_check_in);
        layout_menu = view.findViewById(R.id.layout_menu);
        layout_my_dispensary = view.findViewById(R.id.layout_my_dispensary);

        ll_detail_call = view.findViewById(R.id.ll_detail_call);
        ll_details_address = view.findViewById(R.id.ll_details_address);
        ll_detail_website = view.findViewById(R.id.ll_detail_website);

        btn_join_rewards = view.findViewById(R.id.btn_join_rewards);

        setData();
        setListener();
    }

    private void setData() {
        latitude = data.getLocation().getCoordinates().get(1);
        longitude = data.getLocation().getCoordinates().get(0);

        Log.e("DispensaryDetail", "Name : " + data.getName() + "  latitude : " + latitude + "  longitude : " + longitude);

        String dis = mContext.CalculationByDistance(BaseActivity.currnet_latitude, BaseActivity.currnet_longitude, latitude, longitude);


        Glide.with(mContext).load(data.getSmlogo()).into(image_dispensary_detail);

        text_detail_dispensary_name.setText(data.getName());
        text_detail_distance.setText(String.valueOf(dis));
        text_dispensary_des.setText(data.getDescription());
        text_detail_call.setText(data.getPhoneNumber());
        text_adderess.setText(data.getAddress());
        text_website.setText(data.getWebsite());

        if (data.getDispensaryMenus().size() == 0) {
            text_menu_title.setText("Menu: 0");
            text_menu_title.setTextColor(Color.parseColor("#bebebe"));
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                layout_menu.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.btn_gray_border));
            } else {
                layout_menu.setBackground(ContextCompat.getDrawable(mContext, R.drawable.btn_gray_border));
            }

            layout_menu.setClickable(false);

        } else {
            text_menu_title.setText("Menu: " + data.getDispensaryMenus().size());
        }

        setHours();

        if (data.getWaitlistEnabled()) {

            txt_check_in.setText("WaitList");
            layout_check_in.setBackground(ContextCompat.getDrawable(mContext, R.drawable.btn_green_border));

        } else {

            txt_check_in.setText("WaitList");
            layout_check_in.setBackground(ContextCompat.getDrawable(mContext, R.drawable.btn_gray_border));
        }


        if (data.getOffers().size() > 0) {
            setOfferData();
        }

        if (BaseActivity.MyDispensary.contains(data.getId())) {

            final int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                layout_my_dispensary.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.btn_green_border));
            } else {
                layout_my_dispensary.setBackground(ContextCompat.getDrawable(mContext, R.drawable.btn_green_border));
            }

            txt_my_dispensary.setTextColor(getResources().getColor(R.color.colorAccent));


        } else {

            final int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                layout_my_dispensary.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.btn_gray_border));
            } else {
                layout_my_dispensary.setBackground(ContextCompat.getDrawable(mContext, R.drawable.btn_gray_border));
            }

            txt_my_dispensary.setTextColor(Color.parseColor("#bebebe"));
        }

        txt_title_dispancary_rewards.setText(data.getName().toUpperCase() + " REWARDS");
        txt_title_dispancary_offers.setText(data.getName().toUpperCase() + " OFFERS");
        txt_rewards_discription.setText("The Rewards Program at " + data.getName() + " is offered to all of our customers. We Love rewards, everyone loves rewards, use our rewards !");

        if (BaseActivity.mALL_REWARDS_LIST.size() > 0) {

            BaseActivity.mDispensary_Rewards_list.clear();

            for (AllReawards mAllReawardsItem : BaseActivity.mALL_REWARDS_LIST) {

//                Log.e("REWARDS","venue : "+mAllReawardsItem.getVenue());
//                Log.e("REWARDS","Dispensary Venue : "+data.getLocationId());


                if (mAllReawardsItem.getVenue().equals(data.getLocationId())) {

                    BaseActivity.mDispensary_Rewards_list.add(mAllReawardsItem);

                    Log.e("REWARDS", "venue rewards : " + mAllReawardsItem.getDescription());
                    Log.e("REWARDS", "Dispensary Venue : " + mAllReawardsItem.getVenue());
                    Log.e("REWARDS", "Dispensary Venue : " + mAllReawardsItem.getStatus());
                }

            }

            setRewardsData();
            requestRewardCurrentStars();
        }



    }

    private void setOfferData() {

        final ArrayList<HomeOffer> mHomeOffers = new ArrayList<>();

        ArrayList<Offer> offers = data.getOffers();


        Log.e("offers", "offers size: " + offers.size());
        for (int i = 0; i < offers.size(); i++) {
            HomeOffer homeOffer = new HomeOffer();

            homeOffer.setLocation(data.getLocation());
            homeOffer.setDispensaryName(data.getName());
            homeOffer.setDispensaryImage(data.getSmlogo());

            homeOffer.setOffer(offers.get(i));

            mHomeOffers.add(homeOffer);

        }


        OfferAdapter mAdapter = new OfferAdapter(mContext, mHomeOffers);

        rv_all_dispensary_offer.setLayoutManager(new LinearLayoutManager(mContext));
        rv_all_dispensary_offer.getLayoutManager().setMeasurementCacheEnabled(false);
        rv_all_dispensary_offer.setAdapter(mAdapter);

        rv_all_dispensary_offer.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_all_dispensary_offer, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {


//                    Log.e("mMergedSearchAllList", "mMergedSearchAllList : " + mSearchAllList.get(position).mHomeOffer.getOffer().getTitle());
//                Toast.makeText(mContext, "You clicked offers", Toast.LENGTH_SHORT).show();

                for (int i = 0; i < BaseActivity.mDISPENSARY_LIST.size(); i++) {
                    if (BaseActivity.mDISPENSARY_LIST.get(i).getLocationId().equalsIgnoreCase(mHomeOffers.get(position).getOffer().getDispensaryId())) {
                        BaseActivity.dispensaryName = BaseActivity.mDISPENSARY_LIST.get(i).getName();
                        BaseActivity.dispensaryImage = BaseActivity.mDISPENSARY_LIST.get(i).getSmlogo();
                        BaseActivity.mOFFERS_LIST = BaseActivity.mDISPENSARY_LIST.get(i).getOffers();
                        BaseActivity.mDispensaryData = BaseActivity.mDISPENSARY_LIST.get(i);
                        Log.e("OFFER_SIZE", "" + BaseActivity.mOFFERS_LIST.size());
                        break;
                    }
                }

                BaseActivity.mOffersFromWhere = 2;
                mContext.replaceFragment(new OfferPagerFragment());
//                    changeFragment();


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    private void setRewardsData() {

        DispensaryRewardsAdapter mDispensaryRewardsAdapter = new DispensaryRewardsAdapter(mContext, BaseActivity.mDispensary_Rewards_list);

        rv_dispensary_rewards.setLayoutManager(new LinearLayoutManager(mContext));
        rv_dispensary_rewards.getLayoutManager().setMeasurementCacheEnabled(false);
        rv_dispensary_rewards.setAdapter(mDispensaryRewardsAdapter);
    }


    private void setHours() {
        String mon = data.getHours().getMonday().getOpen() + " - " + data.getHours().getMonday().getClose();
        String tue = data.getHours().getTuesday().getOpen() + " - " + data.getHours().getTuesday().getClose();
        String wed = data.getHours().getWednesday().getOpen() + " - " + data.getHours().getWednesday().getClose();
        String thu = data.getHours().getThursday().getOpen() + " - " + data.getHours().getThursday().getClose();
        String fri = data.getHours().getFriday().getOpen() + " - " + data.getHours().getFriday().getClose();
        String sat = data.getHours().getSaturday().getOpen() + " - " + data.getHours().getSaturday().getClose();
        String sun = data.getHours().getSunday().getOpen() + " - " + data.getHours().getSunday().getClose();

        text_monday.setText("Mon: " + mon);
        text_tuesday.setText("Tue: " + tue);
        text_wednesday.setText("Wed: " + wed);
        text_thursday.setText("Thu: " + thu);
        text_friday.setText("Fri: " + fri);
        text_saturday.setText("Sat: " + sat);
        text_sunday.setText("Sun: " + sun);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);

        if (dayOfTheWeek.toLowerCase().equalsIgnoreCase("monday")) {
            text_monday.setTextColor(Color.parseColor("#0d9648"));
        }

        if (dayOfTheWeek.toLowerCase().equalsIgnoreCase("tuesday")) {
            text_tuesday.setTextColor(Color.parseColor("#0d9648"));
        }

        if (dayOfTheWeek.toLowerCase().equalsIgnoreCase("wednesday")) {
            text_wednesday.setTextColor(Color.parseColor("#0d9648"));
        }

        if (dayOfTheWeek.toLowerCase().equalsIgnoreCase("thursday")) {
            text_thursday.setTextColor(Color.parseColor("#0d9648"));
        }

        if (dayOfTheWeek.toLowerCase().equalsIgnoreCase("friday")) {
            text_friday.setTextColor(Color.parseColor("#0d9648"));
        }

        if (dayOfTheWeek.toLowerCase().equalsIgnoreCase("saturday")) {
            text_saturday.setTextColor(Color.parseColor("#0d9648"));
        }

        if (dayOfTheWeek.toLowerCase().equalsIgnoreCase("sunday")) {
            text_sunday.setTextColor(Color.parseColor("#0d9648"));
        }
    }

    private void setListener() {
        ll_detail_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                        && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    AskForPermission();
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:60321748000"));
                    startActivity(callIntent);
                }
            }
        });

        ll_details_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Log.e("text_adderess click", "  lat  :  " + BaseActivity.currnet_latitude + "  long  : " + BaseActivity.currnet_longitude);
//                Toast.makeText(mContext, "text_adderess click  "+"  lat  :  "+BaseActivity.currnet_latitude+"  long  : "+BaseActivity.currnet_longitude, Toast.LENGTH_SHORT).show();

                if (BaseActivity.currnet_latitude != 0 && BaseActivity.currnet_longitude != 0) {

                    String url = "http://maps.google.com/maps?saddr=" + BaseActivity.currnet_latitude + "," + BaseActivity.currnet_longitude + "&daddr=" + latitude + "," + longitude;
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse(url));
                    startActivity(intent);
                }

            }
        });

        ll_detail_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse(data.getWebsite()));
                startActivity(browserIntent);
            }
        });

        layout_check_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mContext.getLogin() == 0){

                    Toast.makeText(mContext, "Please signup to continue.", Toast.LENGTH_SHORT).show();

                }else {

                    if (isWaitlistAvailable) {

                        ShowCheckinDialog();

                    } else {

                        requestGetWaitlist();
                    }
                }




//                if (mContext.getLogin() == 1) {
//                    ShowCheckinDialog();
//                } else {
//                    mContext.replaceFragment(new SignupFragment());
//                }
            }
        });

        layout_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.dispensaryName = data.getName();
                BaseActivity.dispensaryImage = data.getSmlogo();
                BaseActivity.distance = dis;
                BaseActivity.mMENU_LIST.clear();
                BaseActivity.mMENU_LIST = data.getDispensaryMenus();
                mContext.replaceFragment(new MenuFragment());
            }
        });

        layout_my_dispensary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if(mContext.getLogin() == 1)
//                {

                Log.e("MyDispensary", "id : " + data.getId());

//                    ArrayList<String> MyDispensary = new ArrayList<>();
//                    MyDispensary = mContext.loadSharedPreferencesLogList(mContext);

                //If it's already in Local ArryList then remove it
                if (BaseActivity.MyDispensary.contains(data.getId())) {

                    final int sdk = android.os.Build.VERSION.SDK_INT;
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        layout_my_dispensary.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.btn_gray_border));
                    } else {
                        layout_my_dispensary.setBackground(ContextCompat.getDrawable(mContext, R.drawable.btn_gray_border));
                    }

                    txt_my_dispensary.setTextColor(Color.parseColor("#bebebe"));

                    for (Iterator<String> iterator = BaseActivity.MyDispensary.iterator(); iterator.hasNext(); ) {
                        if (iterator.next().equals(data.getId())) {
                            // Remove the current element from the iterator and the list.
                            iterator.remove();
                        }
                    }

                } else {

                    final int sdk = android.os.Build.VERSION.SDK_INT;
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        layout_my_dispensary.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.btn_green_border));
                    } else {
                        layout_my_dispensary.setBackground(ContextCompat.getDrawable(mContext, R.drawable.btn_green_border));
                    }

                    txt_my_dispensary.setTextColor(getResources().getColor(R.color.colorAccent));
                    BaseActivity.MyDispensary.add(data.getId());

                }


                mContext.saveSharedPreferencesLogList(mContext, BaseActivity.MyDispensary);

                Log.e("MyDispensary", "MyDispensary click : " + BaseActivity.MyDispensary.size());


//                }
//                else {
//                    mContext.replaceFragment(new SignupFragment());
//                }

            }
        });

        btn_join_rewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ShowJoinRewardsDialog();
            }
        });
    }

    private void requestGetWaitlist() {

        try {
            mContext.showWaitIndicator(true);


            final String auth = "Bearer " + BaseActivity.fcmIdToken;

            Log.e("auth", auth);

            String GET_WAITLIST_URL = "https://demo.highconnect.co/api/v1/r/Waitlists?filter[where][location]=" + BaseActivity.mDispensaryData.getLocationId();

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, GET_WAITLIST_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("GET_WAITLIST", "Response " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    for (int i = 0; i < response.length(); i++) {

                                        Waitlist mWaitlist = new Waitlist();
                                        JSONObject jresponse = response.getJSONObject(i);
                                        Log.e("GET_WAITLIST", "Response" + jresponse);

                                        mWaitlist.setWaitlistId(jresponse.get("waitlistId").toString());
                                        mWaitlist.setLocation(jresponse.get("location").toString());
                                        mWaitlist.setTime(jresponse.get("time").toString());

                                        BaseActivity.mWAITLIST_LIST.add(mWaitlist);

                                        BaseActivity.LatestWaitList = jresponse.get("waitlistId").toString();
                                    }

                                    if (BaseActivity.LatestWaitList == null) {
                                        isWaitlistAvailable = false;
                                        txt_check_in.setText("WaitList");
                                        Toast.makeText(mContext, "No wait-list available for this Dispensary", Toast.LENGTH_SHORT).show();
                                    } else {

                                        requestGetCurrentCustomerWaitlist();
                                    }
//                                    isWaitlistAvailable = true;
//                                    txt_check_in.setText("Check-In");


                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void requestGetCurrentCustomerWaitlist() {

        try {
            mContext.showWaitIndicator(true);


            final String auth = "Bearer " + BaseActivity.fcmIdToken;

            Log.e("auth", auth);
            String GET_CURRENT_CUSTOMER_WAITLIST_URL = "https://demo.highconnect.co/api/v1/r/Customers?filter[where][waitlist]=" + BaseActivity.LatestWaitList + "&filter[where][status]=CheckedIn";

            Log.e("CURRENT_CUS_WAITLIST", "URL : " + GET_CURRENT_CUSTOMER_WAITLIST_URL);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, GET_CURRENT_CUSTOMER_WAITLIST_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("CURRENT_CUS_WAITLIST", "RESPONSE : " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    isWaitlistAvailable = true;
                                    txt_check_in.setText("Check-In :" + response.length());


                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void requestCheckInCustomer() {

        try {
            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            Calendar cal = Calendar.getInstance();
            String time = String.valueOf(cal.getTimeInMillis());
//            data.getName()
            JSONObject params = new JSONObject();
            params.put("customerId", data.getName() + "-" + mUser.getCustomerId() + "-" + String.valueOf(time));
            params.put("waitlist", BaseActivity.LatestWaitList);
            params.put("location", data.getName());
            params.put("nickname", mUser.getCustomerId());
            params.put("checkInDate", time);
            params.put("isAccount", true);
            params.put("items", "one gram");
            params.put("birthday", mUser.getBirthDate());
            params.put("firstName", mUser.getFirstName());
            params.put("lastName", mUser.getLastName());
            params.put("mobileNumber", mUser.getPhoneNumber());
            params.put("sex", mUser.getSex());
            params.put("age", mUser.getAge());
            params.put("notes", "Hello");

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

            Log.e("auth", auth);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Constants.WAITLIST_CHECKIN_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("CheckInCust Response", response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    requestJoinedWaitlist();
//                                    mContext.replaceFragment(new CustomerQueueFragment());

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void requestJoinedWaitlist() {

        try {
            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            Calendar cal = Calendar.getInstance();
            String time = String.valueOf(cal.getTimeInMillis());

            JSONObject params = new JSONObject();
            params.put("waitlistId", data.getLocationId());//Use venueId here  insted of waitlistid
            params.put("user", mUser.getCustomerId());
            params.put("time", time);
            params.put("lat", BaseActivity.currnet_latitude);
            params.put("long", BaseActivity.currnet_longitude);

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

//            Log.e("auth", auth);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Constants.JOINED_WAITLIST_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("JOINED_WAITLIST", "RESPONSE" + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    mContext.replaceFragment(new CustomerQueueFragment());

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void requestRegisterVenueCustomer() {

        try {
            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            Calendar cal = Calendar.getInstance();
            String time = String.valueOf(cal.getTimeInMillis());
//            data.getName()
            JSONObject params = new JSONObject();
            params.put("venuecustomerId", data.getLocationId() + "-" + mUser.getCustomerId());
            params.put("venue", data.getLocationId());
            params.put("customer", mUser.getCustomerId());
            params.put("email", mUser.getEmail());
            params.put("displayName", mUser.getDisplayName());
            params.put("firstName", mUser.getFirstName());
            params.put("time", time);
            params.put("age", mUser.getAge());
            params.put("birthday", mUser.getBirthDate());
            params.put("lastName", mUser.getLastName());
            params.put("optInOffers", mUser.getisReceive());
            params.put("acceptedTOS", true);
            params.put("phoneNumber", mUser.getPhoneNumber());
            params.put("sex", mUser.getSex());

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

//            String REGISTER_VENUE_CUSTOMER_URL = "http://18.237.254.224/api/v1/VenueCustomer/RegisterVenueCustomer";
            Log.e("RegisterVenueCustomer", "JSONObject : " + params.toString());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Constants.REGISTER_VENUE_CUSTOMER_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("RegisterVenueCustomer", "response " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {
                                    requestRewardSignUp();
                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void requestRewardSignUp() {

        try {
            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            Calendar cal = Calendar.getInstance();
            String time = String.valueOf(cal.getTimeInMillis());
//            data.getName()
            JSONObject params = new JSONObject();
            params.put("signupId", data.getLocationId());
            params.put("user", mUser.getCustomerId());
            params.put("time", time);
            params.put("lat", BaseActivity.currnet_latitude);
            params.put("long", BaseActivity.currnet_longitude);

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

//            String REGISTER_VENUE_CUSTOMER_URL = "http://18.237.254.224/api/v1/VenueCustomer/RegisterVenueCustomer";
            Log.e("RewardSignUp", "JSONObject : " + params.toString());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Constants.REWARD_SIGNUP_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("RewardSignUp", "response " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    requestRewardCurrentStars();

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void requestRewardCurrentStars() {

        try {
            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            final String auth = "Bearer " + BaseActivity.fcmIdToken;


            String REWARDS_CURRENT_STAR_URL = "http://18.237.254.224/api/v1/r/VenueCustomers?filter[where][customer]=" + mUser.getCustomerId() + "&filter[limit]=1000";
//            Log.e("RewardSignUp", "JSONObject : "+params.toString());

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, REWARDS_CURRENT_STAR_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("REWARDS_CURRENT_STAR", "response " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null && response.length() > 0) {

                                try {


                                    for (int i = 0; i < response.length(); i++) {

                                        JSONObject jresponse = response.getJSONObject(i);
                                        Log.e("REWARDS_CURRENT_STAR", "availableStars  " + jresponse.get("availableStars").toString());

                                        txt_dispensary_available_star.setText(jresponse.get("availableStars").toString());
                                        BaseActivity.mAvailableStars = Integer.parseInt(jresponse.get("availableStars").toString());
                                    }

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }

                                btn_join_rewards.setVisibility(View.GONE);
                                isJoinedRewards = true;

                            }else {

                                isJoinedRewards = false;
                                btn_join_rewards.setVisibility(View.VISIBLE);

//                                requestRegisterVenueCustomer();

                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void AskForPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            mContext.requestPermission(Manifest.permission.CALL_PHONE,
                    getString(R.string.permission_call_phone_rationale),
                    REQUEST_CALL_PHONE_ACCESS_PERMISSION);
        } else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CALL_PHONE_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        mGoogleMap = googleMap;
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

        mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);
        mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);

//        ploatMarkers();


        LatLng sydney = new LatLng(latitude, longitude);
        mMarker = mGoogleMap.addMarker(new MarkerOptions()
                .position(sydney));
//                .title("Sydney")
//                .snippet("Sydney Is Awesome Place"));

        loadMarkerIcon(mMarker, data.getSmlogo());

        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(16).build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng arg0)
            {

                BaseActivity.mMarkerClickPosition = 0;
                BaseActivity.isFromMap = true;
                BaseActivity.mMarkerClickLatlong = new LatLng(latitude, longitude);
                mContext.replaceFragment(new ViewAllDispensaryFragment());

            }
        });

    }


    private void loadMarkerIcon(final Marker marker, String mUrlImg) {
//         = "Url_imagePath;
        Glide.with(this).load(mUrlImg)
                .asBitmap().fitCenter().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {

                if (bitmap != null) {
                    //  Bitmap circularBitmap = getRoundedCornerBitmap(bitmap, 150);
//                    Bitmap mBitmap = getCircularBitmap(bitmap);
//                    mBitmap = addBorderToCircularBitmap(mBitmap, 2, Color.WHITE,squareBitmapWidth);

                    BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(mCustomMarkerView, bitmap));
                    marker.setIcon(icon);
                }

            }
        });

    }

    private Bitmap getMarkerBitmapFromView(View view, Bitmap bitmap) {

        mMarkerImageView.setImageBitmap(bitmap);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = view.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        view.draw(canvas);

        return returnedBitmap;
    }

    private void ShowCheckinDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setTitle("Check-In");
        builder1.setMessage("Check-In to queue for next available budtender. You will be the next customer served.");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Check-In",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                        mContext.replaceFragment(new CustomerQueueFragment());

                        if (mContext.getLogin() == 1) {

                            requestCheckInCustomer();
//                            mContext.replaceFragment(new CustomerQueueFragment());

                        } else {

                            mContext.replaceFragment(new MyAccountFragment());
                        }

                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void ShowJoinRewardsDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setTitle("Join Rewards");
        builder1.setMessage("Join the rewards program at " + data.getName());
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Join",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                        mContext.replaceFragment(new CustomerQueueFragment());
                        requestRegisterVenueCustomer();


                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


}
