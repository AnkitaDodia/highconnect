package com.sp.highconnect.fragments;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.adapter.DispensaryMenuAdapter;
import com.sp.highconnect.adapter.DispensaryMenuTitleAdapter;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.DispensaryMenu;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.widget.LinearLayout.VERTICAL;

/**
 * Created by My 7 on 22-Aug-18.
 */

public class MenuFragment extends Fragment {
    DashboardActivity mContext;
    private static String TAG = "MenuFragment";

    LinearLayout layout_menu_main;

    RecyclerView rv_menu_title, rv_menu_list;

    CircleImageView img_dispensary_logo_menu;

    TextView txt_title_dispensary_menu, txt_distance_menu;

    ArrayList<DispensaryMenu> mDispensaryMenuListAll = new ArrayList<>();
    ArrayList<DispensaryMenu> mDispensaryMenuListFilter;
    ArrayList<String> mCategotyTitles = new ArrayList<>();
    DispensaryMenuTitleAdapter mMenuTitleAdapter;

    private boolean isAllMenu = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        mDispensaryMenuListAll = BaseActivity.mMENU_LIST;
//        mDispensaryMenuListAll = new ArrayList<>();
//        mDispensaryMenuListAll = mList;

        initView(view);
    }

    private void initView(View view) {
        layout_menu_main = view.findViewById(R.id.layout_menu_main);
        mContext.overrideFonts(layout_menu_main);

        rv_menu_title = view.findViewById(R.id.rv_menu_title);
        rv_menu_title.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        rv_menu_list = view.findViewById(R.id.rv_menu_list);
        rv_menu_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        img_dispensary_logo_menu = view.findViewById(R.id.img_dispensary_logo_menu);

        txt_title_dispensary_menu = view.findViewById(R.id.txt_title_dispensary_menu);
        txt_distance_menu = view.findViewById(R.id.txt_distance_menu);

        setData();
        setListener();
    }

    private void setData() {
        txt_title_dispensary_menu.setText(BaseActivity.dispensaryName);
        txt_distance_menu.setText(BaseActivity.distance);

        Glide.with(mContext).load(BaseActivity.dispensaryImage).into(img_dispensary_logo_menu);

        mCategotyTitles.add("All");
        for (int i = 0; i < BaseActivity.mMENU_LIST.size(); i++) {
            if (!mCategotyTitles.contains(BaseActivity.mMENU_LIST.get(i).getCategory())) {
                mCategotyTitles.add(BaseActivity.mMENU_LIST.get(i).getCategory());
            }
//            for(int j = 0; j < temp.size(); j++)
//            {
//                if(!temp.get(j).contains(BaseActivity.mMenuList.get(i).getCategory()))
//                {
//                    temp.add(BaseActivity.mMenuList.get(i).getCategory());
//                }
//            }
        }

        setTitleData();
        setMenuData();
    }

    private void setTitleData() {
        mMenuTitleAdapter = new DispensaryMenuTitleAdapter(mContext, mCategotyTitles);
        rv_menu_title.setAdapter(mMenuTitleAdapter);
    }

    private void setMenuData() {

        Log.e(TAG, "isAllMenu  : " + isAllMenu);

        if (isAllMenu) {

            DispensaryMenuAdapter menuAdapter = new DispensaryMenuAdapter(mContext, mDispensaryMenuListAll);
            DividerItemDecoration itemDecor = new DividerItemDecoration(mContext, VERTICAL);
            rv_menu_list.addItemDecoration(itemDecor);
            rv_menu_list.setAdapter(menuAdapter);
            menuAdapter.notifyDataSetChanged();

        }else{

            DispensaryMenuAdapter menuAdapter = new DispensaryMenuAdapter(mContext, mDispensaryMenuListFilter);
            DividerItemDecoration itemDecor = new DividerItemDecoration(mContext, VERTICAL);
            rv_menu_list.addItemDecoration(itemDecor);
            rv_menu_list.setAdapter(menuAdapter);
            menuAdapter.notifyDataSetChanged();
        }


    }

    private void setListener() {
        rv_menu_title.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_menu_title, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                String title = mCategotyTitles.get(position);

                mMenuTitleAdapter.dataChange(title);

                Log.e(TAG, "menu list title " + mCategotyTitles.get(position));

                mDispensaryMenuListFilter = new ArrayList<>();
//                mDispensaryMenuListAll = new ArrayList<>();

                for (int i = 0; i < mDispensaryMenuListAll.size(); i++) {
                    if (mDispensaryMenuListAll.get(i).getCategory().equalsIgnoreCase(title)) {
                        Log.e(TAG, "menu list if " + mDispensaryMenuListAll.get(i).getCategory());

                        isAllMenu = false;

                        DispensaryMenu data = mDispensaryMenuListAll.get(i);
                        mDispensaryMenuListFilter.add(data);
//                        break;
                    } else if(title.equalsIgnoreCase("All")){
                        Log.e(TAG, "menu list else " + mDispensaryMenuListAll.get(i).getCategory());

                        isAllMenu = true;

//                        mDispensaryMenuListAll = mDispensaryMenuListAll;

                    }
                }

                setMenuData();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
}
