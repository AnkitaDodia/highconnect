package com.sp.highconnect.fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.adapter.CurrentCustomerWaitlistAdapter;
import com.sp.highconnect.adapter.OfferAdapter;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.HomeOffer;
import com.sp.highconnect.model.Offer;
import com.sp.highconnect.model.User;
import com.sp.highconnect.model.WaitlistCustomers;
import com.sp.highconnect.util.SettingsPreferences;
import com.sp.highconnect.utility.DividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static android.widget.LinearLayout.VERTICAL;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class CustomerQueueFragment extends Fragment {

    DashboardActivity mContext;
    private CurrentCustomerWaitlistAdapter mCurrentCustomerWaitlistAdapter;

    private RecyclerView rv_customer_waitlist, rv_dispensary_offer;
    private int mNoOfCutomer = 0;
    private long mStartdateSum, mFinishdateSum;
    Button btn_cancle_waitlist;
    private long AvgWaitTime;

    ImageView image_dispensary_logo;
    EditText edt_waitlist_notes;

    Handler mHandlerCustomerStarted;
    Handler mHandlerCustomerStatus;
    private final int FIVE_SECONDS = 5000;

    private int mCustomerPosition;
    private String CutomerNotes = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_customer_queue, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        rv_customer_waitlist = view.findViewById(R.id.rv_customer_waitlist);
        rv_dispensary_offer = view.findViewById(R.id.rv_dispensary_offer);

        btn_cancle_waitlist = view.findViewById(R.id.btn_cancle_waitlist);
        image_dispensary_logo = view.findViewById(R.id.image_dispensary_logo);

        edt_waitlist_notes = view.findViewById(R.id.edt_waitlist_notes);

        Glide.with(mContext).load(BaseActivity.mDispensaryData.getLogo()).into(image_dispensary_logo);

        DividerItemDecoration itemDecor = new DividerItemDecoration(mContext, VERTICAL);
        rv_customer_waitlist.addItemDecoration(itemDecor);
        rv_customer_waitlist.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        mCurrentCustomerWaitlistAdapter = new CurrentCustomerWaitlistAdapter(mContext, BaseActivity.mWAITLIST_CUSTOMER_LIST);

        rv_dispensary_offer.setNestedScrollingEnabled(false);
        rv_dispensary_offer.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        requestGetAllCurrentCustomerWaitlist();

        btn_cancle_waitlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                requestCancleCustomerWaitlist();
            }
        });

        if (BaseActivity.mDispensaryData.getOffers().size() > 0) {
            setOfferData();
        }

        mContext.ShowBadgeLayout(true);

        mHandlerCustomerStatus = new Handler();
        mHandlerCustomerStarted = new Handler();


        edt_waitlist_notes.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    Log.e("Enter pressed","Enter pressed");
                    Toast.makeText(mContext, "Enter pressed", Toast.LENGTH_SHORT).show();

                    CutomerNotes = edt_waitlist_notes.getText().toString();

                    requestUpdateCustomerInfo();
                }
                return false;
            }
        });

    }

    private void requestUpdateCustomerInfo() {

//        http://demo.highconnect.co/api/v1/Customer/UpdateCustomerInfo

        try {
            mContext.showWaitIndicator(true);


            Calendar cal = Calendar.getInstance();
            String time = String.valueOf(cal.getTimeInMillis());

            mCustomerPosition--;


            JSONObject params = new JSONObject();
            params.put("customerId", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getCustomerId());
            params.put("birthday", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getBirthday());
            params.put("firstName", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getFirstName());
            params.put("isAccount", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getIsAccount());
            params.put("lastName", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getLastName());
            params.put("mobileNumber", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getMobileNumber());
            params.put("nickname", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getNickname());
            params.put("notes", ""+CutomerNotes);
            params.put("sex", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getSex());
            params.put("time", time);

            Log.e("customerrewardId", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getCustomerId());
            Log.e("birthday", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getBirthday());
            Log.e("firstName", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getFirstName());
            Log.e("isAccount", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getIsAccount());
            Log.e("lastName", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getLastName());
            Log.e("mobileNumber", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getMobileNumber());
            Log.e("nickname", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getNickname());
            Log.e("notes", ""+CutomerNotes);
            Log.e("sex", BaseActivity.mWAITLIST_CUSTOMER_LIST.get(mCustomerPosition).getSex());
            Log.e("time", time);



//            {
//                "birthday": "Aug 18, 1995",
//                    "customerId": "Green Time Dispensary - Aaron8 - 1537923883689",
//                    "firstName": "Aaron",
//                    "isAccount": "true",
//                    "lastName": "Johnson",
//                    "mobileNumber": "7789984340",
//                    "nickname": "Aaron8",
//                    "notes": “I am writing notes“,
//                "sex": "M",
//                    "time": "1537923883689"
//            }


            final String auth = "Bearer " + BaseActivity.fcmIdToken;

//            Log.e("auth", auth);

            String UPDATE_INFO_URL = "http://demo.highconnect.co/api/v1/Customer/UpdateCustomerInfo";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, UPDATE_INFO_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("UPDATE_INFO", "RESPONSE" + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {


                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            mContext.showWaitIndicator(false);
            e.printStackTrace();
        }

    }

    private void setOfferData() {

        ArrayList<HomeOffer> mHomeOffers = new ArrayList<>();

        ArrayList<Offer> offers = BaseActivity.mDispensaryData.getOffers();


        Log.e("offers", "offers size: " + offers.size());
        for (int i = 0; i < offers.size(); i++) {
            HomeOffer homeOffer = new HomeOffer();

            homeOffer.setLocation(BaseActivity.mDispensaryData.getLocation());
            homeOffer.setDispensaryName(BaseActivity.mDispensaryData.getName());
            homeOffer.setDispensaryImage(BaseActivity.mDispensaryData.getSmlogo());

            homeOffer.setOffer(offers.get(i));

            mHomeOffers.add(homeOffer);

        }


        OfferAdapter mAdapter = new OfferAdapter(mContext, mHomeOffers);

        rv_dispensary_offer.setLayoutManager(new LinearLayoutManager(mContext));
        rv_dispensary_offer.getLayoutManager().setMeasurementCacheEnabled(false);
        rv_dispensary_offer.setAdapter(mAdapter);


    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    /**
     * To Get Al Customer at Dispensary to calculate the avrage waittime
     **/
    private void requestGetAllCurrentCustomerWaitlist() {

        try {
            mContext.showWaitIndicator(true);

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

//            Log.e("auth", auth);
            String GET_ALL_CUSTOMER_WAITLIST_URL = "https://demo.highconnect.co/api/v1/r/Customers?filter[where][location]=" + BaseActivity.mDispensaryData.getLocationId() + "&filter[where][status]=ServiceFinished&filter[limit]=1000";


            Log.e("ALLCUSTOMER", "URL : " + GET_ALL_CUSTOMER_WAITLIST_URL);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, GET_ALL_CUSTOMER_WAITLIST_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("ALLCUSTOMER", "RESPONSE : " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    mNoOfCutomer = response.length();
                                    mStartdateSum = 0;
                                    mFinishdateSum = 0;
                                    Log.e("ALLCUSTOMER", "Number of All customer" + mNoOfCutomer);

                                    for (int i = 0; i < response.length(); i++) {

                                        JSONObject jresponse = response.getJSONObject(i);

                                        long startdate = jresponse.getLong("startDate");
                                        long finishdate = jresponse.getLong("finishDate");

                                        if (finishdate > 0 && startdate > 0) {
//                                        Log.e("ALLCUSTOMER","budtender : "+jresponse.getString("budtender"));
//                                        Log.e("ALLCUSTOMER", "postion : "+i+"  startDate in milisecond : " + startdate+"  finishDate in milisecond : " + finishdate);
                                            mFinishdateSum = mFinishdateSum + finishdate;
                                            mStartdateSum = mStartdateSum + startdate;
                                        }


                                    }
                                    CalculateWaitTime(mFinishdateSum, mStartdateSum);
                                    requestGetCurrentCustomerWaitlist();

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * To Get Customer Queue
     **/
    private void requestGetCurrentCustomerWaitlist() {

        try {
            mContext.showWaitIndicator(true);


            final String auth = "Bearer " + BaseActivity.fcmIdToken;

//            Log.e("auth", auth);
            String GET_CURRENT_CUSTOMER_WAITLIST_URL = "https://demo.highconnect.co/api/v1/r/Customers?filter[where][waitlist]=" + BaseActivity.LatestWaitList + "&filter[where][status]=CheckedIn";


            Log.e("CUSTOMER_WAITLIST", "URL : " + GET_CURRENT_CUSTOMER_WAITLIST_URL);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, GET_CURRENT_CUSTOMER_WAITLIST_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("CUSTOMER_WAITLIST", "RESPONSE" + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    BaseActivity.mWAITLIST_CUSTOMER_LIST.clear();

                                    for (int i = 0; i < response.length(); i++) {

                                        WaitlistCustomers mWaitlistCustomers = new WaitlistCustomers();
                                        JSONObject jresponse = response.getJSONObject(i);
//                                        Log.e("CUSTOMER_WAITLIST", "Each item" + jresponse);

                                        Log.e("CUSTOMER_WAITLIST","customerId : "+jresponse.get("customerId").toString());
                                        Log.e("CUSTOMER_WAITLIST","notes : "+jresponse.get("notes").toString());

                                        mWaitlistCustomers.setCustomerId(jresponse.get("customerId").toString());
                                        mWaitlistCustomers.setCheckInDate(jresponse.getInt("checkInDate"));
                                        mWaitlistCustomers.setStatus(jresponse.get("status").toString());
                                        mWaitlistCustomers.setBirthday(jresponse.get("birthday").toString());
                                        mWaitlistCustomers.setFinishDate(jresponse.getInt("finishDate"));
                                        mWaitlistCustomers.setFirstName(jresponse.get("firstName").toString());
                                        mWaitlistCustomers.setIsAccount(jresponse.get("isAccount").toString());
                                        mWaitlistCustomers.setLastName(jresponse.get("lastName").toString());
                                        mWaitlistCustomers.setLocation(jresponse.get("location").toString());
                                        mWaitlistCustomers.setMobileNumber(jresponse.get("mobileNumber").toString());
                                        mWaitlistCustomers.setNickname(jresponse.get("nickname").toString());
                                        mWaitlistCustomers.setNotes(jresponse.get("notes").toString());
                                        mWaitlistCustomers.setPrice(jresponse.get("price").toString());
                                        mWaitlistCustomers.setProducts(jresponse.get("products").toString());
                                        mWaitlistCustomers.setServiceNotes(jresponse.get("serviceNotes").toString());
                                        mWaitlistCustomers.setSex(jresponse.get("sex").toString());
                                        mWaitlistCustomers.setSkipped(jresponse.get("skipped").toString());
                                        mWaitlistCustomers.setStartDate(jresponse.getInt("startDate"));
                                        mWaitlistCustomers.setWaitlist(jresponse.get("waitlist").toString());
                                        mWaitlistCustomers.setAge(jresponse.get("age").toString());
                                        mWaitlistCustomers.setBudtender(jresponse.get("budtender").toString());
                                        mWaitlistCustomers.setAvgWaitlistTime(AvgWaitTime);
                                        mWaitlistCustomers.setItemPosition(i);
//                                        CalculateWaitTime(jresponse.getInt("finishDate"), jresponse.getInt("startDate"));



                                        BaseActivity.mWAITLIST_CUSTOMER_LIST.add(mWaitlistCustomers);
                                    }



                                    mCurrentCustomerWaitlistAdapter.notifyDataSetChanged();
                                    rv_customer_waitlist.setAdapter(mCurrentCustomerWaitlistAdapter);

                                    scheduleCustomerStartedWaitlist();
                                    findPositionofUser();


                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void findPositionofUser() {

        User mUser = SettingsPreferences.getConsumer(mContext);

        for (int i = 0; i < BaseActivity.mWAITLIST_CUSTOMER_LIST.size(); i ++) {


            if(BaseActivity.mWAITLIST_CUSTOMER_LIST.get(i).getFirstName().equalsIgnoreCase(mUser.getFirstName())){

                mCustomerPosition = i + 1;

                Log.e("CUSTOMER_WAITLIST","Position of user "+i);

                edt_waitlist_notes.setText(""+BaseActivity.mWAITLIST_CUSTOMER_LIST.get(i).getNotes());
                mContext.SetQueueNumber(""+mCustomerPosition);
                break;
            }
        }

    }


    public void scheduleCustomerStartedWaitlist() {


        mHandlerCustomerStarted.postDelayed(new Runnable() {
            public void run() {
                requestGetCustomerStartedWaitlist(); // this method will contain your almost-finished HTTP calls
                mHandlerCustomerStarted.postDelayed(this, FIVE_SECONDS);
            }
        }, FIVE_SECONDS);

    }


    /**
     * To GET Customer Service is Started
     **/
    private void requestGetCustomerStartedWaitlist() {

        try {
            mContext.showWaitIndicator(true);
            final User mUser = SettingsPreferences.getConsumer(mContext);
            final String auth = "Bearer " + BaseActivity.fcmIdToken;


            String GET_ALL_CUSTOMER_WAITLIST_URL = "http://demo.highconnect.co/api/v1/r/Customers?filter[where][waitlist]=" + BaseActivity.LatestWaitList + "&filter[where][status][inq][0]=ServiceStarted&filter[where][status][inq][1]=CheckedIn";

//            Log.e("CUSTOMER_STARTED", "URL : " + GET_ALL_CUSTOMER_WAITLIST_URL);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, GET_ALL_CUSTOMER_WAITLIST_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("CUSTOMER_STARTED", "RESPONSE : " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {



//                                    Log.e("CUSTOMER_STARTED", "Customer ID : " + mUser.getCustomerId());

                                    for (int i = 0; i < response.length(); i++) {

                                        JSONObject jresponse = response.getJSONObject(i);

                                        String status = jresponse.getString("status"); // Skipped  ServiceFinished
                                        String customerId = jresponse.getString("customerId");

//                                        Log.e("CUSTOMER_STARTED","position: "+i+" User Service has been started/ : "+customerId);
//
                                        if (customerId.equalsIgnoreCase(mUser.getCustomerId())) {

                                            Log.e("CUSTOMER_STARTED", "inside If User Service has been started for : " + customerId);

//                                            if(status.equalsIgnoreCase("ServiceStarted")){

//                                                Log.e("CUSTOMER_STARTED","ServiceStarted");



                                            mHandlerCustomerStarted.removeCallbacksAndMessages(null);

                                            Toast.makeText(mContext,"User Service has been Started",Toast.LENGTH_SHORT).show();

                                            scheduleCustomerStatusWaitlist();


//                                            }else {
//
//                                                requestGetCustomerStartedWaitlist();
//                                            }
                                        }


                                    }

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void scheduleCustomerStatusWaitlist() {


        mHandlerCustomerStatus.postDelayed(new Runnable() {
            public void run() {

                requestGetCustomerStatusWaitlist(); // this method will contain your almost-finished HTTP calls
                mHandlerCustomerStatus.postDelayed(this, FIVE_SECONDS);
            }
        }, FIVE_SECONDS);



    }


    /**
     * To GET Customer Service is Finished or Skipped
     **/
    private void requestGetCustomerStatusWaitlist() {

        try {
            mContext.showWaitIndicator(true);
            final User mUser = SettingsPreferences.getConsumer(mContext);
            final String auth = "Bearer " + BaseActivity.fcmIdToken;

//            Log.e("auth", auth);
            String GET_ALL_CUSTOMER_WAITLIST_URL = "https://demo.highconnect.co/api/v1/r/Customers?filter[where][waitlist]=" + BaseActivity.LatestWaitList + "&filter[where][status][inq][0]=ServiceFinished&filter[where][status][inq][1]=Skipped&filter[limit]=1000";

//            Log.e("CUSTOMER_STATUS", "URL : " + GET_ALL_CUSTOMER_WAITLIST_URL);

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, GET_ALL_CUSTOMER_WAITLIST_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("CUSTOMER_STATUS", "RESPONSE : " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {


//                                    Log.e("CUSTOMER_STATUS", "Customer ID : " + mUser.getCustomerId());

                                    for (int i = 0; i < response.length(); i++) {

                                        JSONObject jresponse = response.getJSONObject(i);

                                        String status = jresponse.getString("status"); // Skipped  ServiceFinished
                                        String customerId = jresponse.getString("customerId");

//                                        Log.e("CUSTOMER_STATUS", "position: " + i + " User has been served : " + customerId);

                                        if (customerId.equalsIgnoreCase(mUser.getCustomerId())) {

                                            Log.e("CUSTOMER_STATUS", "User has been served : " + customerId);

                                            if (status.equalsIgnoreCase("ServiceFinished")) {

                                                Log.e("CUSTOMER_STATUS", "ServiceFinished");
                                                mHandlerCustomerStatus.removeCallbacksAndMessages(null);

                                                Toast.makeText(mContext,"User Service has been Finished",Toast.LENGTH_SHORT).show();

                                            } else if (status.equalsIgnoreCase("Skipped")) {

                                                mHandlerCustomerStatus.removeCallbacksAndMessages(null);
                                                Log.e("CUSTOMER_STATUS", "Skipped");
                                                Toast.makeText(mContext,"User Service has been Skipped",Toast.LENGTH_SHORT).show();

                                            }
                                        }
//                                        else {
//
//
//                                            requestGetCustomerStatusWaitlist();
//                                        }


                                    }

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * To Cancle Customer Service from queue
     **/
    private void requestCancleCustomerWaitlist() {

        try {
            mContext.showWaitIndicator(true);

            User mUser = SettingsPreferences.getConsumer(mContext);

            final String auth = "Bearer " + BaseActivity.fcmIdToken;

            Calendar cal = Calendar.getInstance();
            String time = String.valueOf(cal.getTimeInMillis());

            JSONObject params = new JSONObject();
            params.put("customerId", mUser.getCustomerId());
            params.put("time", time);

//            Log.e("auth", auth);
            String GET_ALL_CUSTOMER_WAITLIST_URL = "https://demo.highconnect.co/api/v1/Customer/CancelCustomer";


            Log.e("CANCLECUSTOMER", "URL : " + GET_ALL_CUSTOMER_WAITLIST_URL);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, GET_ALL_CUSTOMER_WAITLIST_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("CANCLECUSTOMER", "RESPONSE : " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void CalculateWaitTime(long finishDate, long startDate) {

        try {
            //time for Each
            long eachtime = (finishDate - startDate) / mNoOfCutomer;

//            Log.e("ALLCUSTOMER", "finishDate in milisecond : " + finishDate);
//            Log.e("ALLCUSTOMER", "startDate in milisecond : " + startDate+"  finishDate in milisecond : " + finishDate);

//            AvgWaitTime = (int)TimeUnit.MILLISECONDS.toMinutes(eachtime);

            AvgWaitTime = eachtime;

            Log.e("ALLCUSTOMER", "wait time for Each in milisecond : " + eachtime);
            Log.e("ALLCUSTOMER", "wait time for Each in Ago : " + TimeUnit.MILLISECONDS.toMinutes(eachtime));

        } catch (Exception e) {


        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mContext.ShowBadgeLayout(false);
        mContext.SetQueueNumber("0");
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandlerCustomerStarted.removeCallbacksAndMessages(null);
        mHandlerCustomerStatus.removeCallbacksAndMessages(null);
//        removeMessages(0);

        mContext.ShowBadgeLayout(false);
        mContext.SetQueueNumber("0");
    }
}
