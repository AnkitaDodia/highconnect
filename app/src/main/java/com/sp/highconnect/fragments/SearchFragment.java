package com.sp.highconnect.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.adapter.SearchOfferPagerAdapter;
import com.sp.highconnect.adapter.TopSearchItemAdapter;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.DispensaryMenu;
import com.sp.highconnect.model.DispensaryPojo;
import com.sp.highconnect.model.TopSearchItems;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class SearchFragment extends Fragment {

    DashboardActivity mContext;
    private EditText edt_search;
    private LinearLayout ll_slider_dots;
    private ViewPager vp_offers;

    private SearchOfferPagerAdapter mSearchOfferPagerAdapter;

    private int dotscount;
    private ImageView[] dots;

    boolean isBreak = true, isfirst = true;

    NestedScrollView nsv_search;

    public static ArrayList<TopSearchItems> mTopSearchItemsList = new ArrayList<>();
    RecyclerView rv_top_search_items;

    private TopSearchItemAdapter mTopSearchItemAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        initViews(view);

    }

    private void initViews(View view) {

        edt_search = view.findViewById(R.id.edt_search);
        edt_search.setFocusable(false);

        ll_slider_dots = view.findViewById(R.id.ll_slider_dots);
        vp_offers = view.findViewById(R.id.vp_offers);

        rv_top_search_items = view.findViewById(R.id.rv_top_search_items);
        rv_top_search_items.setHasFixedSize(true);
        rv_top_search_items.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        nsv_search = view.findViewById(R.id.nsv_search);


        edt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mContext.replaceFragment(new SearchResultFragment());
            }
        });

        mSearchOfferPagerAdapter = new SearchOfferPagerAdapter(mContext, BaseActivity.mALL_HOME_OFFERS_LIST);
        vp_offers.setAdapter(mSearchOfferPagerAdapter);

        dotscount = mSearchOfferPagerAdapter.getCount();
        dots = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(mContext);
            dots[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.non_active_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            ll_slider_dots.addView(dots[i], params);

        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.active_dot));

        vp_offers.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.non_active_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        mTopSearchItemAdapter = new TopSearchItemAdapter(mContext, mTopSearchItemsList);
//        rv_top_search_items.setAdapter(mTopSearchItemAdapter);

        rv_top_search_items.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_top_search_items, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {


                BaseActivity.mIsFromTopPicks = true;

                BaseActivity.mToppickSearch = mTopSearchItemsList.get(position).getItemId();
                mContext.replaceFragment(new SearchResultFragment());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        requestGetTopSearch();

    }



    private void requestGetTopSearch() {

        try {
            mContext.showWaitIndicator(true);


            final String auth = "Bearer " + BaseActivity.fcmIdToken;

//            Log.e("auth", auth);

            String GET_WAITLIST_URL = "http://34.221.94.79/api/v1/r/Items?filter[order]=hits%20DESC&filter[limit]=30";

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, GET_WAITLIST_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("GET_TOP_SEARCH", "Response " + response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {
                                    mTopSearchItemsList.clear();
                                    for (int i = 0; i < response.length(); i++) {

                                        isBreak = true;
                                        JSONObject jresponse = response.getJSONObject(i);


                                        Log.e("GetDispensaryInfo", "itemname : " + jresponse.getString("itemId"));
                                        GetDispensaryInfo(i, jresponse.getString("itemId"), jresponse.getInt("hits"));

                                    }

                                    mTopSearchItemAdapter = new TopSearchItemAdapter(mContext, mTopSearchItemsList);
                                    rv_top_search_items.setAdapter(mTopSearchItemAdapter);

                                    mTopSearchItemAdapter.notifyDataSetChanged();
                                    nsv_search.scrollTo(0, 0);

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void GetDispensaryInfo(int position, String mMenuItemName, int mHits) {

        TopSearchItems mTopSearchItems = new TopSearchItems();


        Log.e("GetDispensaryInfo", "All Start by postion :" + position);

        outerloop:
            for (DispensaryPojo mDispensaryItem : BaseActivity.mDISPENSARY_LIST) {

//            Log.e("GetDispensaryInfo", "looking into mDispensaryItem:" +mDispensaryItem.getName());


                for (DispensaryMenu mMenuItem : mDispensaryItem.getDispensaryMenus()) {

                    if (mMenuItem.getName().toLowerCase().equalsIgnoreCase(mMenuItemName)) {

                        Log.e("GetDispensaryInfo", "position  :" + position);
                        Log.e("GetDispensaryInfo", "MenuName : " + mMenuItem.getName());
                        Log.e("GetDispensaryInfo", "mDispensaryItem : " + mDispensaryItem.getName());

                        if (isfirst) {

                            mTopSearchItems.setPosition(0);
                            mTopSearchItems.setItemId(mMenuItemName);
                            mTopSearchItems.setHits(mHits);
                            mTopSearchItems.setLatitude(mDispensaryItem.getLocation().getCoordinates().get(1));
                            mTopSearchItems.setLongitude(mDispensaryItem.getLocation().getCoordinates().get(0));
                            mTopSearchItems.setImageurl(mMenuItem.getImageUrl());

                            isfirst = false;

                        } else {

                            mTopSearchItems.setPosition(1);
                            mTopSearchItems.setItemId(mMenuItemName);
                            mTopSearchItems.setHits(mHits);
                            mTopSearchItems.setLatitude(mDispensaryItem.getLocation().getCoordinates().get(1));
                            mTopSearchItems.setLongitude(mDispensaryItem.getLocation().getCoordinates().get(0));
                            mTopSearchItems.setImageurl(mMenuItem.getImageUrl());
                        }


                        mTopSearchItemsList.add(mTopSearchItems);
                        break outerloop;

                    }
                }


        }

        Log.e("GetDispensaryInfo", "All Done by position : " + position);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        super.onPrepareOptionsMenu(menu);
    }
}
