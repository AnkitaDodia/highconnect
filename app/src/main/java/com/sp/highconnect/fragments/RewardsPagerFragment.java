package com.sp.highconnect.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.adapter.RewardsPagerAdapter;
import com.sp.highconnect.common.BaseActivity;

/**
 * Created by My 7 on 16-Aug-18.
 */

public class RewardsPagerFragment extends Fragment
{
    DashboardActivity mContext;

    LinearLayout layout_rewards_pager_main,ll_rewards_left,ll_rewards_right,layoutDots_play;


    ViewPager viewPager_rewards_detail;

    ImageView image_rewards_left,image_rewards_right;


    RewardsPagerAdapter adapter;

    TextView[] dots;
    private int no_of_rewards;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.detail_rewards_slider, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        no_of_rewards = BaseActivity.mDispensary_Rewards_list.size();

        initView(view);
    }

    private void initView(View view)
    {
        layout_rewards_pager_main = view.findViewById(R.id.layout_rewards_pager_main);
        mContext.overrideFonts(layout_rewards_pager_main);

        ll_rewards_left = view.findViewById(R.id.ll_rewards_left);
        ll_rewards_right = view.findViewById(R.id.ll_rewards_right);
        layoutDots_play = view.findViewById(R.id.layoutDots_play);

        image_rewards_left = view.findViewById(R.id.image_rewards_left);
        image_rewards_right = view.findViewById(R.id.image_rewards_right);

        image_rewards_left.setColorFilter(mContext.getResources().getColor(R.color.theme_color_light_brown));
        image_rewards_right.setColorFilter(mContext.getResources().getColor(R.color.theme_color_light_brown));

//        ll_dispensary_details = view.findViewById(R.id.ll_dispensary_details);

        viewPager_rewards_detail = view.findViewById(R.id.viewPager_rewards_detail);
        viewPager_rewards_detail.addOnPageChangeListener(viewPagerPageChangeListener);

        addBottomDots(0);
        setData();
        setListener();
    }

    private void setData()
    {
        adapter = new RewardsPagerAdapter(getChildFragmentManager(),mContext,no_of_rewards);
        viewPager_rewards_detail.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    private void setListener()
    {
        ll_rewards_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItemAtPosition(-1);
                if (current >= 0) {
                    viewPager_rewards_detail.setCurrentItem(current);
                    changeview(viewPager_rewards_detail.getCurrentItem());
                }
            }
        });

        ll_rewards_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItemAtPosition(+1);
                if (current <= no_of_rewards -1) {
                    viewPager_rewards_detail.setCurrentItem(current);
                    changeview(viewPager_rewards_detail.getCurrentItem());
                }
            }
        });

//        ll_dispensary_details.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                BaseActivity.mDispensaryData = mList.get(position);
//                mContext.replaceFragment(new DispensaryDetailFragment());
//
//            }
//        });
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            changeview(viewPager_rewards_detail.getCurrentItem());
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addBottomDots(int currentPage) {

        dots = new TextView[no_of_rewards];

        layoutDots_play.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(mContext);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.dot_light));
            layoutDots_play.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(getResources().getColor(R.color.dot_dark));

    }

    public int getItemAtPosition(int i) {
        return viewPager_rewards_detail.getCurrentItem() + i;
    }

    public void changeview(int pager_no) {

        if (pager_no == 0) {

            image_rewards_left.setVisibility(View.GONE);
            image_rewards_right.setVisibility(View.VISIBLE);

        } else if (pager_no == no_of_rewards - 1) {

            image_rewards_left.setVisibility(View.VISIBLE);
            image_rewards_right.setVisibility(View.GONE);

        } else {

            image_rewards_left.setVisibility(View.VISIBLE);
            image_rewards_right.setVisibility(View.VISIBLE);

        }
    }
}
