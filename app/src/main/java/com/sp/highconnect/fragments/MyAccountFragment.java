package com.sp.highconnect.fragments;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.llollox.androidtoggleswitch.widgets.ToggleSwitch;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.apihelper.Constants;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.User;
import com.sp.highconnect.util.InternetStatus;
import com.sp.highconnect.util.SettingsPreferences;
import com.sp.highconnect.utility.CameraUtils;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.sp.highconnect.utility.CameraUtils.MEDIA_TYPE_IMAGE;


/**
 * Created by My 7 on 08-Mar-18.
 */

public class MyAccountFragment extends Fragment {
    DashboardActivity mContext;

    boolean isInternet;

    ToggleSwitch mSwitchGender;

    EditText edt_firstname, edt_lastname, edt_email, edt_username, edt_phonenumber, edt_birthday;

    Switch switch_receive_update, switch_terms_privacy;

    ImageView imageView_user;

    LinearLayout layout_account_main, layout_continue;

    Calendar mCalender;

    int mGenderPosition = 0;
    String firstName, lastName, email, userName, phone, birthDate, time, age, gender;
    boolean isReceive_Update, isTerms_Privacy;

    String auth;

    public static final int PICK_IMAGE = 1;
    public static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    public static String imageStoragePath;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_account, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (DashboardActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        mCalender = Calendar.getInstance();

        initView(view);
        setListeners();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    private void initView(View view) {
        layout_account_main = view.findViewById(R.id.layout_account_main);
        mContext.overrideFonts(layout_account_main);

        mSwitchGender = view.findViewById(R.id.multiple_switches);
        mSwitchGender.setCheckedPosition(mGenderPosition);


        edt_firstname = view.findViewById(R.id.edt_firstname);
        edt_lastname = view.findViewById(R.id.edt_lastname);
        edt_email = view.findViewById(R.id.edt_email);
        edt_username = view.findViewById(R.id.edt_username);
        edt_phonenumber = view.findViewById(R.id.edt_phonenumber);
        edt_birthday = view.findViewById(R.id.edt_birthday);

        switch_receive_update = view.findViewById(R.id.switch_receive_update);
        switch_terms_privacy = view.findViewById(R.id.switch_terms_privacy);

        imageView_user = view.findViewById(R.id.imageView_user);

        layout_continue = view.findViewById(R.id.layout_continue);


        if (mContext.getLogin() == 1) {

            setLocalData();

        } else {

//            edt_firstname.setText("HCUser006");
//            edt_lastname.setText("HCUser006");
//            edt_email.setText("HCUser006@yahoo.co.in");
//            edt_username.setText("HCUser006");
//            edt_phonenumber.setText("1234567890");
//            edt_birthday.setText("Aug 4,1992");
//            switch_terms_privacy.setChecked(true);

        }


    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            mCalender.set(Calendar.YEAR, year);
            mCalender.set(Calendar.MONTH, monthOfYear);
            mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            age = getAge(year, monthOfYear, dayOfMonth);
            Log.e("Calculate Age", "Get Age : " + getAge(year, monthOfYear, dayOfMonth));
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "MMM dd,yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

        edt_birthday.setText(sdf.format(mCalender.getTime()));
    }

    private void setListeners() {
        mSwitchGender.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int pos) {
                // Your code ...
                mGenderPosition = pos;

//                0 for male  / 1 for female
                if (mGenderPosition == 1) {
                    gender = "F";
                } else {
                    gender = "M";
                }

//                Toast.makeText(mContext, "Clicked on : "+mGenderPosition, Toast.LENGTH_SHORT).show();
            }
        });

        edt_birthday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(mContext, date, mCalender
                        .get(Calendar.YEAR), mCalender.get(Calendar.MONTH),
                        mCalender.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        layout_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_firstname.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please enter First Name..!", Toast.LENGTH_LONG).show();
                } else if (edt_lastname.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please enter Last Name..!", Toast.LENGTH_LONG).show();
                } else if (edt_email.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please enter Email Address..!", Toast.LENGTH_LONG).show();
                } else if (edt_username.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please enter Display Name..!", Toast.LENGTH_LONG).show();
                } else if (edt_phonenumber.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please enter Phone Number", Toast.LENGTH_LONG).show();
                } else if (edt_birthday.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please enter Birth Date..!", Toast.LENGTH_LONG).show();
                } else if (!switch_terms_privacy.isChecked()) {
                    Toast.makeText(mContext, "Please Confirm terms and condition..!", Toast.LENGTH_LONG).show();
                } else {
                    firstName = edt_firstname.getText().toString();
                    lastName = edt_lastname.getText().toString();
                    email = edt_email.getText().toString();
                    userName = edt_username.getText().toString();
                    phone = edt_phonenumber.getText().toString();
                    birthDate = edt_birthday.getText().toString();
                    isReceive_Update = switch_receive_update.isChecked();
                    isTerms_Privacy = switch_terms_privacy.isChecked();
                    Calendar cal = Calendar.getInstance();
                    time = String.valueOf(cal.getTimeInMillis());

                    if (mGenderPosition == 1) {
                        gender = "F";
                    } else {
                        gender = "M";
                    }

                    if (isInternet) {

                        sendSignUpCustomerRequest();

                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        if (mContext.getImagePath() == null) {

            imageView_user.setImageBitmap(null);

        } else {

            imageView_user.setImageBitmap(decodeFile(new File(mContext.getImagePath())));
        }

        imageView_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CreateSelectDialog();


            }
        });
    }

    private void setLocalData() {
        User data = SettingsPreferences.getConsumer(mContext);

        if (data != null) {
            edt_firstname.setText(data.getFirstName());
            edt_email.setText(data.getEmail());
            edt_username.setText(data.getDisplayName());
            edt_lastname.setText(data.getLastName());
            edt_phonenumber.setText(data.getPhoneNumber());
            edt_birthday.setText(data.getBirthDate());

            if (data.getSex().equalsIgnoreCase("f")) {
                mGenderPosition = 1;
                mSwitchGender.setCheckedPosition(mGenderPosition);
            } else {
                mGenderPosition = 0;
                mSwitchGender.setCheckedPosition(mGenderPosition);
            }

            switch_receive_update.setChecked(data.getisReceive());
            switch_terms_privacy.setChecked(data.getisTerms());

            Log.e("switch_terms_privacy", "switch_terms_privacy : " + data.getisTerms());
            Log.e("switch_receive_update", "switch_receive_update : " + data.getisReceive());
        }

    }


    public void sendSignUpCustomerRequest() {
        try {
            mContext.showWaitIndicator(true);

            JSONObject params = new JSONObject();
            params.put("customerId", userName);
            params.put("email", email);
            params.put("displayName", userName);
            params.put("firstName", firstName);
            params.put("time", time);
            params.put("age", age);
            params.put("birthDate", birthDate);
            params.put("lastName", lastName);
            params.put("optInOffers", "true");
            params.put("phoneNumber", phone);
            params.put("sex", gender);

            auth = "Bearer " + BaseActivity.fcmIdToken;

            Log.e("auth", auth);
            Log.e("params", params.toString());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Constants.CUSTOMER_BASE_URL, params,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("SignUpCustomer Response", response.toString());
                            mContext.showWaitIndicator(false);

                            if (response != null) {

                                try {

                                    User mUserItem = new User();

                                    mUserItem.setCustomerId(response.getString("customerId"));
                                    mUserItem.setDisplayName(response.getString("displayName"));
                                    mUserItem.setEmail(response.getString("email"));
                                    mUserItem.setFirstName(response.getString("firstName"));
                                    mUserItem.setTime(response.getString("time"));
                                    mUserItem.setAge(response.getString("age"));
                                    mUserItem.setBirthDate(response.getString("birthDate"));
                                    mUserItem.setLastName(response.getString("lastName"));
                                    mUserItem.setPhoneNumber(response.getString("phoneNumber"));
                                    mUserItem.setSex(response.getString("sex"));
                                    mUserItem.setReceive(response.getBoolean("optInOffers"));
                                    mUserItem.setTerms(true);

                                    SettingsPreferences.storeConsumer(mContext, mUserItem);

                                    mContext.setLogin(1);

                                    Toast.makeText(mContext, "User Register Successfully", Toast.LENGTH_SHORT).show();

                                    mContext.replaceFragment(new HomeFragment());

                                } catch (Exception e) {
                                    Log.e("EXCEPTION", "" + e.getMessage());
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR_FROM_VOLLEY", error.toString());
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("Authorization", auth);
                    map.put("content-type", "application/json");
                    return map;
                }
            };
            Volley.newRequestQueue(mContext).add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    private void CreateSelectDialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(mContext);
        View promptsView = li.inflate(R.layout.dialog_choose_picture, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        // set dialog message
        alertDialogBuilder.setCancelable(false);


        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        LinearLayout dialog_main = promptsView.findViewById(R.id.dialog_main);
        mContext.overrideFonts(dialog_main);
        final LinearLayout ll_dialog_camera = (LinearLayout) promptsView.findViewById(R.id.ll_dialog_camera);
        final LinearLayout ll_dialog_gallery = (LinearLayout) promptsView.findViewById(R.id.ll_dialog_gallery);
        final LinearLayout ll_dialog_remove = (LinearLayout) promptsView.findViewById(R.id.ll_dialog_remove);
        final LinearLayout ll_dialog_cancel = (LinearLayout) promptsView.findViewById(R.id.ll_dialog_cancel);

        if (mContext.getImagePath() == null) {

            ll_dialog_remove.setVisibility(View.GONE);

        } else {

            ll_dialog_remove.setVisibility(View.VISIBLE);

//            imageView_user.setImageBitmap(decodeFile(new File(mContext.getImagePath())));
        }


        ll_dialog_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
//                Toast.makeText(mContext, "Clicked Camera", Toast.LENGTH_SHORT).show();

                if (CameraUtils.checkPermissions(mContext)) {
//                    layout_include.setVisibility(View.GONE);
                    captureImage();
                } else {
                    requestCameraPermission(MEDIA_TYPE_IMAGE);
                }
            }
        });

        ll_dialog_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                        && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    AskForPermissionForGallery();
                } else {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, PICK_IMAGE);
                }

//                Toast.makeText(mContext, "Clicked Gallery", Toast.LENGTH_SHORT).show();


            }
        });

        ll_dialog_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                mContext.removeImagePath();
                imageView_user.setImageBitmap(null);
            }
        });

        ll_dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    private void AskForPermissionForGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            mContext.requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int requestcode, Intent data) {
//        super.onActivityResult(requestCode, requestcode, data);

        if (requestCode == PICK_IMAGE) {
            if (requestcode == RESULT_OK) {
                Uri selectedImageUri = data.getData();


//                File fImage = new File(selectedImageUri.getPath());

//                fImage.getAbsolutePath();

                imageStoragePath = selectedImageUri.getPath();

                if (imageStoragePath != null) {
                    mContext.saveImagePath(imageStoragePath);
                    imageView_user.setImageBitmap(decodeFile(new File(imageStoragePath)));
                }

//                imageView_user.setImageBitmap(decodeFile(new File(selectedImageUri.getPath())));
                Log.e("Gallery_Result", "PICK_IMAGE  " + selectedImageUri);
                try {

                } catch (Exception e) {
                    Toast.makeText(mContext, "Internal Error", Toast.LENGTH_LONG).show();
                    Log.e(e.getClass().getName(), e.getMessage(), e);
                }
            }
        } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (requestcode == RESULT_OK) {
                // Refreshing the gallery
                try {
                    CameraUtils.refreshGallery(mContext.getApplicationContext(), imageStoragePath);
                    Log.e("Camera_Result", "CAMERA_CAPTURE  " + imageStoragePath);

                    if (imageStoragePath != null) {
                        mContext.saveImagePath(imageStoragePath);
                        imageView_user.setImageBitmap(decodeFile(new File(imageStoragePath)));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestcode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(mContext.getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(mContext.getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission(final int type) {
        Dexter.withActivity(mContext)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(mContext);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(mContext, file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    // Decodes image and scales it to reduce memory consumption
    public Bitmap decodeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE = 80;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            Log.e("scale", "scale : " + scale);
            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

}
