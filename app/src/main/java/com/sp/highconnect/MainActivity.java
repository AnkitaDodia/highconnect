package com.sp.highconnect;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;

public class MainActivity extends AppCompatActivity {


    private String TAG = "MainActivity";
    Context mContext;
    private FirebaseAuth mAuth;

//    highconnectdev at gmail.com
//    d=}NB{8%S/cNG+(e

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        mAuth = FirebaseAuth.getInstance();

        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInAnonymously:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Log.w(TAG, "signInAnonymously:success "+user);
                            Toast.makeText(mContext, "Authentication is Successful.",
                                    Toast.LENGTH_SHORT).show();
                            GetToken();
                        } else {
                            Log.w(TAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(mContext, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        updateUI(currentUser);
    }

    private void GetToken(){

        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
        mUser.getIdToken(true)
                .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if (task.isSuccessful()) {
                            String idToken = task.getResult().getToken();
                            // Send token to your backend via HTTPS
                            Log.e("idToken","idToken : "+idToken);
                            // ...
                        } else {
                            // Handle error -> task.getException();
                        }
                    }
                });
    }

//    eyJhbGciOiJSUzI1NiIsImtpZCI6ImZmNTRmZjM0MTFiZmMwMDJiYTBjZDAwNzA2YmEzYmM4NTBiZWIwMmIifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vaGlnaGNvbm5lY3QtZGVtby0zOTMwOSIsInByb3ZpZGVyX2lkIjoiYW5vbnltb3VzIiwiYXVkIjoiaGlnaGNvbm5lY3QtZGVtby0zOTMwOSIsImF1dGhfdGltZSI6MTUzMzcwMTU1MywidXNlcl9pZCI6Ijc1VW5vZ1loUnBVOFhxY2lyNzNLc25QWG9BbTEiLCJzdWIiOiI3NVVub2dZaFJwVThYcWNpcjczS3NuUFhvQW0xIiwiaWF0IjoxNTMzNzAxNjExLCJleHAiOjE1MzM3MDUyMTEsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnt9LCJzaWduX2luX3Byb3ZpZGVyIjoiYW5vbnltb3VzIn19.cTvUS_BmliJT2MqjM-dCZdB_aJWneK7ZxHCIuAUX0_Rd2mOY14KiKpeQqV6b8RROE8N-d4_0F_UfPARwdLTwmO8rigMNkusS6FeTVJ1u1eNm68oNC66eOtZKKPVpHhfZ79X2sQekDPrmBNDSex5i_SQVe4dabscTkHo60xqMt1DA2QffygIMdMTBQeMsSKmug22xkn3Wo_lMCFd_axLRqpTJzpFSqQ2Un_p_WIbr2-yNXOCW0PZzCA4QNU5jTXHFVCBAToMDjKHbZFrx1aIRfymEYZsbUV9t1pBR0sUn7dEWKRPkscP2qn2Tv2j1sG7qjr6BPwValDsNvt7V5bJvag

}
