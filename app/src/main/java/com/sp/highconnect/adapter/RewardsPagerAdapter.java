package com.sp.highconnect.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.fragments.RewardsDetailFragment;

/**
 * Created by My 7 on 16-Aug-18.
 */

public class RewardsPagerAdapter extends FragmentPagerAdapter
{
    private DashboardActivity mContext;

    private int no_of_rewards;

    public RewardsPagerAdapter(FragmentManager childFragmentManager, DashboardActivity mContext, int no_of_rewards) {
        super(childFragmentManager);
        this.mContext = mContext;
        this.no_of_rewards = no_of_rewards;
        Log.e("no_of_rewards",""+no_of_rewards);
    }

    @Override
    public Fragment getItem(int position) {
        return(RewardsDetailFragment.newInstance(mContext,position));
    }

    @Override
    public int getCount() {
        return no_of_rewards;
    }
}