package com.sp.highconnect.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.TopSearchItems;
import com.sp.highconnect.model.TopSearches;
import com.sp.highconnect.ui.SquareRelativeLayout;

import java.util.ArrayList;

/**
 * Created by My 7 on 22-Aug-18.
 */

public class TopSearchItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<TopSearchItems> mList = new ArrayList<>();

    DashboardActivity mContext;

    public TopSearchItemAdapter(DashboardActivity mContext, ArrayList<TopSearchItems> list)
    {
        this.mContext = mContext;
        mList = list;
    }

    @Override
    public TopSearchesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_top_search_item_list, null);

//        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(260, ViewGroup.LayoutParams.MATCH_PARENT);
//        itemView.setLayoutParams(lp);
//        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(300, 300);
//        itemView.setLayoutParams(lp);

        return new TopSearchesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TopSearchItems mTopSearchItems = mList.get(position);
//        MyViewHolder mMyViewHolder = new MyViewHolder();
        TopSearchesViewHolder mMyViewHolder = (TopSearchesViewHolder) holder;

        if (mTopSearchItems.getPosition() == 0){
            mMyViewHolder.txt_top_search_item_title.setText("BEST SELLER");
            mMyViewHolder.txt_top_search_item_title.setVisibility(View.VISIBLE);
        }else {
            mMyViewHolder.txt_top_search_item_title.setText("");
            mMyViewHolder.txt_top_search_item_title.setVisibility(View.GONE);
        }

        String imagePath = "https://tripntvcontent.s3.us-west-2.amazonaws.com/"+mTopSearchItems.getImageurl();
        Glide.with(mContext).load(imagePath).into(mMyViewHolder.image_top_search_item);

        String dis = mContext.CalculationByDistance(BaseActivity.currnet_latitude,BaseActivity.currnet_longitude,mTopSearchItems.getLatitude(),mTopSearchItems.getLongitude());

        mMyViewHolder.txt_top_search_item_name.setText(mTopSearchItems.getItemId());
        mMyViewHolder.txt_top_search_item_dis.setText(dis);
        mMyViewHolder.txt_top_search_item_hits.setText(":  "+mTopSearchItems.getHits());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class TopSearchesViewHolder extends RecyclerView.ViewHolder {

        SquareRelativeLayout ll_top_search_item_main ;

        TextView txt_top_search_item_title, txt_top_search_item_name, txt_top_search_item_dis, txt_top_search_item_hits;
        ImageView image_top_search_item;

        public TopSearchesViewHolder(View itemView) {
            super(itemView);

            ll_top_search_item_main = itemView.findViewById(R.id.ll_top_search_item_main);
            mContext.overrideFonts(ll_top_search_item_main);

            image_top_search_item = itemView.findViewById(R.id.image_top_search_item);

            txt_top_search_item_title = itemView.findViewById(R.id.txt_top_search_item_title);
            txt_top_search_item_name = itemView.findViewById(R.id.txt_top_search_item_name);
            txt_top_search_item_dis = itemView.findViewById(R.id.txt_top_search_item_dis);
            txt_top_search_item_hits = itemView.findViewById(R.id.txt_top_search_item_hits);
        }
    }
}
