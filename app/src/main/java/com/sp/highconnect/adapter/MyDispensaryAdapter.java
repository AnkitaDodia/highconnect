package com.sp.highconnect.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.fragments.DispensaryDetailFragment;
import com.sp.highconnect.model.MyDispensary;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class MyDispensaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<MyDispensary> mList = new ArrayList<>();

    DashboardActivity mContext;

    public MyDispensaryAdapter(DashboardActivity mContext, ArrayList<MyDispensary> dispensaryPojos)
    {
        this.mContext = mContext;
        mList = dispensaryPojos;
    }

   @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType==0) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dispensary_list, null, false);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            itemView.setLayoutParams(lp);
            return new MyItemViewHolder(itemView);
        } else {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dispensary_section, null, false);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            itemView.setLayoutParams(lp);

            return new SectionViewHolder(itemView);
        }

    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        MyDispensary data = mList.get(position);

        if(data.type) {


            MyItemViewHolder mHolder = (MyItemViewHolder) holder;
//            h.textView.setText(item.getRow());

            ArrayList<Double> coordinates = data.mDispensaryPojo.getLocation().getCoordinates();
            Double latitude = coordinates.get(0);
            Double longitude = coordinates.get(1);

            String dis = mContext.CalculationByDistance(BaseActivity.currnet_latitude,BaseActivity.currnet_longitude,latitude,longitude);
            mHolder.row_dispensary_dis.setText(dis);

            LinearLayout.LayoutParams params = new
                    LinearLayout.LayoutParams(300,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            mHolder.row_dispensary_main.setLayoutParams(params);

            mHolder.row_dispensary_name.setText(data.mDispensaryPojo.getName());

            Glide.with(mContext)
                    .load(data.mDispensaryPojo.getSmlogo())
                    .into(mHolder.row_dispensary_image);

            mHolder.row_dispensary_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseActivity.mDispensaryData = mList.get(position).mDispensaryPojo;
                    mContext.replaceFragment(new DispensaryDetailFragment());

//                Toast.makeText(mContext, "Click From MyDispensary Adapter", Toast.LENGTH_SHORT).show();
                }
            });

        } else {


            SectionViewHolder h = (SectionViewHolder) holder;
//            h.row_dispensary_section


            h.textView.setText(data.mNearMe.getTitle());


        }


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyItemViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout row_dispensary_main;

        ImageView row_dispensary_image;

        TextView row_dispensary_name,row_dispensary_dis;

        public MyItemViewHolder(View itemView)
        {
            super(itemView);

            row_dispensary_main = itemView.findViewById(R.id.row_dispensary_main);

            row_dispensary_image = itemView.findViewById(R.id.row_dispensary_image);

            row_dispensary_name = itemView.findViewById(R.id.row_dispensary_name);
            row_dispensary_dis = itemView.findViewById(R.id.row_dispensary_dis);

            mContext.overrideFonts(row_dispensary_main);
        }
    }

   /* public class RowViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public RowViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(android.R.id.text1);
        }
    }*/

    public class SectionViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
//        private LinearLayout row_dispensary_section;
        public SectionViewHolder(View itemView) {
            super(itemView);
//            row_dispensary_section = (LinearLayout) itemView.findViewById(R.id.row_dispensary_section);
            textView = (TextView) itemView.findViewById(R.id.txt_dispensary_section);
        }
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        MyDispensary item = mList.get(position);
        if(item.type) {
            return 0;
        } else {
            return 1;
        }
    }
}
