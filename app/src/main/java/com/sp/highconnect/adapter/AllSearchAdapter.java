package com.sp.highconnect.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.Cost;
import com.sp.highconnect.model.DispensaryMenu;
import com.sp.highconnect.model.DispensaryPojo;
import com.sp.highconnect.model.HomeOffer;
import com.sp.highconnect.model.NearMe;
import com.sp.highconnect.model.SearchAll;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class AllSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<SearchAll> mList = new ArrayList<>();

    DashboardActivity mContext;

    public AllSearchAdapter(DashboardActivity mContext, ArrayList<SearchAll> dispensaryPojos)
    {
        this.mContext = mContext;
        mList = dispensaryPojos;
    }

   @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == 0) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search_section, null, false);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            itemView.setLayoutParams(lp);
            return new SearchTitleViewHolder(itemView);

        } else if(viewType == 1){

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_all_dispensary_list, null, false);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            itemView.setLayoutParams(lp);
            return new DispensaryViewHolder(itemView);
        }else if(viewType == 2){

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offer_list, null, false);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            itemView.setLayoutParams(lp);
            return new OfferViewHolder(itemView);
        }else {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_menu_item, null,false);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            itemView.setLayoutParams(lp);
            return new MenuViewHolder(itemView);
        }


    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mViewHolder, final int position) {
        SearchAll data = mList.get(position);

        if(data.type == 0) {

            SearchTitleViewHolder mSearchTitleViewHolder = (SearchTitleViewHolder) mViewHolder;
            NearMe mNearMe = mList.get(position).mNearMe;
            mSearchTitleViewHolder.textView.setText(mNearMe.getTitle());

        } else if(data.type == 1){

            DispensaryViewHolder mDispensaryViewHolder = (DispensaryViewHolder) mViewHolder;

            DispensaryPojo mDispensaryPojo = mList.get(position).mDispensaryPojo;

            Double latitude = mDispensaryPojo.getLocation().getCoordinates().get(0);
            Double longitude = mDispensaryPojo.getLocation().getCoordinates().get(1);

            String dis = mContext.CalculationByDistance(BaseActivity.currnet_latitude,BaseActivity.currnet_longitude,latitude,longitude);
            mDispensaryViewHolder.all_dispensary_dis.setText(dis);

            mDispensaryViewHolder.all_dispensary_name.setText(mDispensaryPojo.getName());
            mDispensaryViewHolder.all_dispensary_distance.setText(mDispensaryPojo.getDescription());

            Glide.with(mContext)
                    .load(mDispensaryPojo.getSmlogo())
                    .into(mDispensaryViewHolder.all_dispensary_image);

            mDispensaryViewHolder.all_dispensary_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    Log.e("getLocation","getLocation : "+mList.get(position).getLocation().getCoordinates().toString());
//                    BaseActivity.mDispensaryData = mList.get(position);
//                    mContext.replaceFragment(new DispensaryDetailFragment());
                }
            });

        }else if(data.type == 2){

            HomeOffer mHomeOffer = mList.get(position).mHomeOffer;
            final OfferViewHolder mOfferViewHolder = (OfferViewHolder) mViewHolder;

//        Log.e("IMAGE_PATH",""+data.getOffer().getImageUrl());
            Log.e("offers","adapter offers size: "+mList.size());

            try {
                String path = "https://tripntvcontent.s3.us-west-2.amazonaws.com/"+mHomeOffer.getOffer().getImageUrl();
                Glide.with(mContext).load(path).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mOfferViewHolder.progress_offer_list.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mOfferViewHolder.progress_offer_list.setVisibility(View.GONE);
                        return false;
                    }
                }).into(mOfferViewHolder.img_offer_list);

                Glide.with(mContext).load(mHomeOffer.getDispensaryImage()).into(mOfferViewHolder.img_dispensary_item);
            }catch (Exception e){
                e.printStackTrace();
            }

            mOfferViewHolder.text_dispensary_name_list.setText(mHomeOffer.getDispensaryName());
            mOfferViewHolder.text_offer_tital_list.setText(mHomeOffer.getOffer().getTitle());

            Double latitude = mHomeOffer.getLocation().getCoordinates().get(0);
            Double longitude = mHomeOffer.getLocation().getCoordinates().get(1);

            String dis = mContext.CalculationByDistance(BaseActivity.currnet_latitude,BaseActivity.currnet_longitude,latitude,longitude);
            mOfferViewHolder.text_distance_list.setText(dis);

        }else{

            final MenuViewHolder holder = (MenuViewHolder) mViewHolder;

            DispensaryMenu mDispensaryMenu = mList.get(position).mDispensaryMenu;
            ArrayList<Cost> cost  = mDispensaryMenu.getCost();


            switch (mDispensaryMenu.getCategory())
            {
                case "Indica":
                    holder.menu_category.setText("Indica");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_indicae_bg));
                    break;
                case "Sativa":
                    holder.menu_category.setText("Sativa");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_sativa_bg));
                    break;
                case "Accessories":
                    holder.menu_category.setText("Access.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_access_bg));
                    break;
                case "Concentrates":
                    holder.menu_category.setText("Concen.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_concen_bg));
                    break;
                case "Candy":
                    holder.menu_category.setText("Candy");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_candy_bg));
                    break;
                case "Hybrid":
                    holder.menu_category.setText("Hybrid");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_hybrid_bg));
                    break;
                case "Snacks":
                    holder.menu_category.setText("Snacks");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_snacks_bg));
                    break;
                case "BakedGoods":
                    holder.menu_category.setText("BakedG.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_baked_goods_bg));
                    break;
                case "BathBodyBeauty":
                    holder.menu_category.setText("BathBo.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_bath_body_bg));
                    break;
                case "Bongs":
                    holder.menu_category.setText("Bongs");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_bongs_bg));
                    break;
                case "Bowls":
                    holder.menu_category.setText("Bowls");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_bowls_bg));
                    break;
                case "Bubblers":
                    holder.menu_category.setText("Bubble.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_bubblers_bg));
                    break;
                case "Budder":
                    holder.menu_category.setText("Budder");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_budder_bg));
                    break;
                case "CBDEdibles":
                    holder.menu_category.setText("CBDEdi.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_cdb_edibles_bg));
                    break;
                case "CBDOil":
                    holder.menu_category.setText("CBDOil");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_cdb_oil_bg));
                    break;
                case "Capsules":
                    holder.menu_category.setText("Capsul.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_capsules_bg));
                    break;
                case "Chocolates":
                    holder.menu_category.setText("Chocol.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_chocolates_bg));
                    break;
                case "Cooking":
                    holder.menu_category.setText("Cookin.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_cooking_bg));
                    break;
                case "Crumble":
                    holder.menu_category.setText("Crumble");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_crumble_bg));
                    break;
                case "Drinks":
                    holder.menu_category.setText("Drinks");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_drinks_bg));
                    break;
                case "Frozen":
                    holder.menu_category.setText("Frozen");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_frozen_bg));
                    break;
                case "Grinders":
                    holder.menu_category.setText("Grinde.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_grinders_bg));
                    break;
                case "Hash":
                    holder.menu_category.setText("Hash");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_hash_bg));
                    break;
                case "Kief":
                    holder.menu_category.setText("Kief");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_kief_bg));
                    break;
                case "Lighters":
                    holder.menu_category.setText("Lighte.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_lighters_bg));
                    break;
                case "LiveResin":
                    holder.menu_category.setText("LiveRe.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_live_resin_bg));
                    break;
                case "Nails":
                    holder.menu_category.setText("Nails");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_nails_bg));
                    break;
                case "Oils":
                    holder.menu_category.setText("Oils");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_oils_bg));
                    break;
                case "Papers":
                    holder.menu_category.setText("Papers");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_papers_bg));
                    break;
                case "Pipes":
                    holder.menu_category.setText("Pipes");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_pipes_bg));
                    break;
                case "PreRolls":
                    holder.menu_category.setText("PreRolls");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_pre_rolls_bg));
                    break;
                case "Rigs":
                    holder.menu_category.setText("Rigs");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_rigs_bg));
                    break;
                case "RollingMachines":
                    holder.menu_category.setText("Rollin.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_rolling_machines_bg));
                    break;
                case "Rosin":
                    holder.menu_category.setText("Rosin");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_rosin_bg));
                    break;
                case "Shake":
                    holder.menu_category.setText("Shake");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_shake_bg));
                    break;
                case "Shatter":
                    holder.menu_category.setText("Shatte.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_shatter_bg));
                    break;
                case "SnacksMunchies":
                    holder.menu_category.setText("Snacks.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_snacks_munchies_bg));
                    break;
                case "Trays":
                    holder.menu_category.setText("Trays");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_trays_bg));
                    break;
                case "Tinctures":
                    holder.menu_category.setText("Tinctu.");
                    holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_tinctures_bg));
                    break;
            }

            holder.menu_name.setText(mDispensaryMenu.getName());
            holder.menu_dis.setText(mDispensaryMenu.getDescription());

            String imagePath = "https://tripntvcontent.s3.us-west-2.amazonaws.com/"+mDispensaryMenu.getImageUrl();
            Glide.with(mContext).load(imagePath).into(holder.menu_image);

            MenuPriceAdapter adapter = new MenuPriceAdapter(mContext,cost);
            holder.rv_menu_price_list.setAdapter(adapter);

            holder.menu_dis.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.menu_dis.getLineCount() == 1)
                    {
                        holder.menu_dis.setEllipsize(null);
                        holder.menu_dis.setMaxLines(5);
                    }
                    else
                    {
                        holder.menu_dis.setEllipsize(TextUtils.TruncateAt.END);
                        holder.menu_dis.setMaxLines(1);
                    }
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class DispensaryViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout all_dispensary_main;

        ImageView all_dispensary_image;

        TextView all_dispensary_name,all_dispensary_dis,all_dispensary_distance;

        public DispensaryViewHolder(View itemView)
        {
            super(itemView);

            all_dispensary_main = itemView.findViewById(R.id.all_dispensary_main);

            all_dispensary_image = itemView.findViewById(R.id.all_dispensary_image);

            all_dispensary_name = itemView.findViewById(R.id.all_dispensary_name);
            all_dispensary_dis = itemView.findViewById(R.id.all_dispensary_dis);
            all_dispensary_distance = itemView.findViewById(R.id.all_dispensary_distance);

            mContext.overrideFonts(all_dispensary_main);
        }
    }


    public class OfferViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout offer_list_main;

        ImageView img_offer_list;

        ProgressBar progress_offer_list;

        CircleImageView img_dispensary_item;

        TextView text_dispensary_name_list,text_offer_tital_list,text_distance_list;

        public OfferViewHolder(View itemView)
        {
            super(itemView);

            offer_list_main = itemView.findViewById(R.id.offer_list_main);

            img_offer_list = itemView.findViewById(R.id.img_offer_list);

            progress_offer_list = itemView.findViewById(R.id.progress_offer_list);

            img_dispensary_item = itemView.findViewById(R.id.img_dispensary_item);

            text_dispensary_name_list = itemView.findViewById(R.id.text_dispensary_name_list);
            text_offer_tital_list = itemView.findViewById(R.id.text_offer_tital_list);
            text_distance_list = itemView.findViewById(R.id.text_distance_list);

            mContext.overrideFonts(offer_list_main);
        }
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout layout_menu_item_main,layout_menu_category;

        ImageView menu_image;

        TextView menu_category,menu_name;

        TextView menu_dis;

        RecyclerView rv_menu_price_list;

        public MenuViewHolder(View itemView)
        {
            super(itemView);

            layout_menu_item_main = itemView.findViewById(R.id.layout_menu_item_main);
            mContext.overrideFonts(layout_menu_item_main);

            layout_menu_category = itemView.findViewById(R.id.layout_menu_category);

            menu_image = itemView.findViewById(R.id.menu_image);

            menu_category = itemView.findViewById(R.id.menu_category);
            menu_name = itemView.findViewById(R.id.menu_name);
            menu_dis = itemView.findViewById(R.id.menu_dis);

            rv_menu_price_list = itemView.findViewById(R.id.rv_menu_price_list);
            rv_menu_price_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        }
    }

    public class SearchTitleViewHolder extends RecyclerView.ViewHolder
    {

        private TextView textView;

        public SearchTitleViewHolder(View itemView) {

            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.txt_search_section);
        }
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        SearchAll item = mList.get(position);
        if(item.type == 0) {
            return 0;
        } else if(item.type == 1) {
            return 1;
        }else if(item.type == 2) {
            return 2;
        }else {
            return 3;
        }

    }
}
