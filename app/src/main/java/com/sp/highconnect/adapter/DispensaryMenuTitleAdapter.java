package com.sp.highconnect.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;

import java.util.ArrayList;

/**
 * Created by My 7 on 22-Aug-18.
 */

public class DispensaryMenuTitleAdapter extends RecyclerView.Adapter<DispensaryMenuTitleAdapter.MyViewHolder>
{
    private static String TAG = "MenuTitleAdapter" ;
    private static String mSelectedTitle = "all" ;
    ArrayList<String> mList ;

    DashboardActivity mContext;

    public DispensaryMenuTitleAdapter(DashboardActivity mContext, ArrayList<String> list)
    {
        this.mContext = mContext;
        mList = list;
    }

    @Override
    public DispensaryMenuTitleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_menu_title_item, null);

        return new DispensaryMenuTitleAdapter.MyViewHolder(itemView);
    }

    public void dataChange(String title)
    {

        mSelectedTitle = title;
        Log.e("title","in DataChange  : "+title);

        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(DispensaryMenuTitleAdapter.MyViewHolder holder, int position) {
        String data = mList.get(position);
        String drawablename = null;
        holder.text_menu_title.setText(data);

        Log.e(TAG,""+data);

        try{


        holder.text_menu_title.setTextColor(ContextCompat.getColor(mContext, getColorResourceId(data.toLowerCase())));
        holder.layout_menu_item.setBackground(ContextCompat.getDrawable(mContext, getDrawableResourceId("menu_title_item_white_bg")));

        if(data == mSelectedTitle){

            drawablename = "menu_title_item_"+data.toLowerCase()+"_bg";

            Log.e("drawablename",""+drawablename);

            holder.text_menu_title.setTextColor(ContextCompat.getColor(mContext, getColorResourceId("white")));
            holder.layout_menu_item.setBackground(ContextCompat.getDrawable(mContext, getDrawableResourceId(drawablename)));

        }

        }catch (Exception e){

        }


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout layout_menu_title,layout_menu_item;

        TextView text_menu_title;

        public MyViewHolder(View itemView) {
            super(itemView);

            layout_menu_title = itemView.findViewById(R.id.layout_menu_title);
            mContext.overrideFonts(layout_menu_title);

            layout_menu_item = itemView.findViewById(R.id.layout_menu_item);

            text_menu_title = itemView.findViewById(R.id.text_item__menu_title);
        }
    }

    public int getDrawableResourceId(String name) {

        int resourceId = mContext.getResources().getIdentifier(name, "drawable", mContext.getPackageName());
        return resourceId;
    }

    public int getColorResourceId(String name) {

        int resourceId = mContext.getResources().getIdentifier(name, "color", mContext.getPackageName());
        return resourceId;
    }
}
