package com.sp.highconnect.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.model.AllReawards;
import com.sp.highconnect.model.Cost;

import java.util.ArrayList;

/**
 * Created by My 7 on 22-Aug-18.
 */

public class DispensaryRewardsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<AllReawards> mList = new ArrayList<>();

    DashboardActivity mContext;

    public DispensaryRewardsAdapter(DashboardActivity mContext, ArrayList<AllReawards> list)
    {
        this.mContext = mContext;
        mList = list;
    }

    @Override
    public DispensaryRewardsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_dispensary_rewards, null, false);

        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        itemView.setLayoutParams(lp);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AllReawards data = mList.get(position);
//        MyViewHolder mMyViewHolder = new MyViewHolder();
        MyViewHolder mMyViewHolder= (MyViewHolder) holder;
        mMyViewHolder.txt_dis_rewards_title.setText(data.getDescription());
        mMyViewHolder.txt_dis_rewards_required.setText("Cost : "+data.getStarsRequired());

        Glide.with(mContext)
                .load(data.getImageURL())
                .into(mMyViewHolder.img_dis_rewards);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ll_dis_rewards_main;
        TextView txt_dis_rewards_title, txt_dis_rewards_required;
        ImageView img_dis_rewards;

        public MyViewHolder(View itemView) {
            super(itemView);

            ll_dis_rewards_main = itemView.findViewById(R.id.ll_dis_rewards_main);
            mContext.overrideFonts(ll_dis_rewards_main);

            img_dis_rewards = itemView.findViewById(R.id.img_dis_rewards);
            txt_dis_rewards_title = itemView.findViewById(R.id.txt_dis_rewards_title);
            txt_dis_rewards_required = itemView.findViewById(R.id.txt_dis_rewards_required);
        }
    }
}
