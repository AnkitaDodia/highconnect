package com.sp.highconnect.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.model.Cost;
import com.sp.highconnect.model.DispensaryMenu;

import java.util.ArrayList;

/**
 * Created by My 7 on 22-Aug-18.
 */

public class DispensaryMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private ArrayList<DispensaryMenu> mList = new ArrayList<>();
    private ArrayList<Cost> cost = new ArrayList<>();

    private DashboardActivity mContext;

    public DispensaryMenuAdapter(DashboardActivity mContext, ArrayList<DispensaryMenu> list)
    {
        this.mContext = mContext;
        mList = list;
    }

    @Override
    public DispensaryMenuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_menu_item, null, false);

        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        itemView.setLayoutParams(lp);

        return new DispensaryMenuAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mholder, int position) {

        DispensaryMenu data = mList.get(position);
        cost = data.getCost();
        final MyViewHolder holder= (MyViewHolder) mholder;

        switch (data.getCategory())
        {
           case "Indica":
                holder.menu_category.setText("Indica");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_indicae_bg));
                break;
            case "Sativa":
                holder.menu_category.setText("Sativa");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_sativa_bg));
                break;
            case "Accessories":
                holder.menu_category.setText("Access.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_access_bg));
                break;
            case "Concentrates":
                holder.menu_category.setText("Concen.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_concen_bg));
                break;
            case "Candy":
                holder.menu_category.setText("Candy");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_candy_bg));
                break;
            case "Hybrid":
                holder.menu_category.setText("Hybrid");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_hybrid_bg));
                break;
            case "Snacks":
                holder.menu_category.setText("Snacks");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_snacks_bg));
                break;
            case "BakedGoods":
                holder.menu_category.setText("BakedG.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_baked_goods_bg));
                break;
            case "BathBodyBeauty":
                holder.menu_category.setText("BathBo.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_bath_body_bg));
                break;
            case "Bongs":
                holder.menu_category.setText("Bongs");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_bongs_bg));
                break;
            case "Bowls":
                holder.menu_category.setText("Bowls");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_bowls_bg));
                break;
            case "Bubblers":
                holder.menu_category.setText("Bubble.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_bubblers_bg));
                break;
            case "Budder":
                holder.menu_category.setText("Budder");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_budder_bg));
                break;
            case "CBDEdibles":
                holder.menu_category.setText("CBDEdi.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_cdb_edibles_bg));
                break;
            case "CBDOil":
                holder.menu_category.setText("CBDOil");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_cdb_oil_bg));
                break;
            case "Capsules":
                holder.menu_category.setText("Capsul.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_capsules_bg));
                break;
            case "Chocolates":
                holder.menu_category.setText("Chocol.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_chocolates_bg));
                break;
            case "Cooking":
                holder.menu_category.setText("Cookin.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_cooking_bg));
                break;
            case "Crumble":
                holder.menu_category.setText("Crumble");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_crumble_bg));
                break;
            case "Drinks":
                holder.menu_category.setText("Drinks");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_drinks_bg));
                break;
            case "Frozen":
                holder.menu_category.setText("Frozen");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_frozen_bg));
                break;
            case "Grinders":
                holder.menu_category.setText("Grinde.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_grinders_bg));
                break;
            case "Hash":
                holder.menu_category.setText("Hash");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_hash_bg));
                break;
            case "Kief":
                holder.menu_category.setText("Kief");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_kief_bg));
                break;
            case "Lighters":
                holder.menu_category.setText("Lighte.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_lighters_bg));
                break;
            case "LiveResin":
                holder.menu_category.setText("LiveRe.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_live_resin_bg));
                break;
            case "Nails":
                holder.menu_category.setText("Nails");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_nails_bg));
                break;
            case "Oils":
                holder.menu_category.setText("Oils");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_oils_bg));
                break;
            case "Papers":
                holder.menu_category.setText("Papers");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_papers_bg));
                break;
            case "Pipes":
                holder.menu_category.setText("Pipes");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_pipes_bg));
                break;
            case "PreRolls":
                holder.menu_category.setText("PreRolls");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_pre_rolls_bg));
                break;
            case "Rigs":
                holder.menu_category.setText("Rigs");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_rigs_bg));
                break;
            case "RollingMachines":
                holder.menu_category.setText("Rollin.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_rolling_machines_bg));
                break;
            case "Rosin":
                holder.menu_category.setText("Rosin");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_rosin_bg));
                break;
            case "Shake":
                holder.menu_category.setText("Shake");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_shake_bg));
                break;
            case "Shatter":
                holder.menu_category.setText("Shatte.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_shatter_bg));
                break;
            case "SnacksMunchies":
                holder.menu_category.setText("Snacks.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_snacks_munchies_bg));
                break;
            case "Trays":
                holder.menu_category.setText("Trays");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_trays_bg));
                break;
            case "Tinctures":
                holder.menu_category.setText("Tinctu.");
                holder.layout_menu_category.setBackground(ContextCompat.getDrawable(mContext, R.drawable.menu_item_tinctures_bg));
                break;
        }

        holder.menu_name.setText(data.getName());
        holder.menu_dis.setText(data.getDescription());

        String imagePath = "https://tripntvcontent.s3.us-west-2.amazonaws.com/"+data.getImageUrl();
        Glide.with(mContext).load(imagePath).into(holder.menu_image);

        MenuPriceAdapter adapter = new MenuPriceAdapter(mContext,cost);
        holder.rv_menu_price_list.setAdapter(adapter);

//        holder.menu_dis.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(holder.menu_dis.getLineCount() == 1)
//                {
//                    holder.menu_dis.setEllipsize(null);
//                    holder.menu_dis.setMaxLines(5);
//                }
//                else
//                {
//                    holder.menu_dis.setEllipsize(TextUtils.TruncateAt.END);
//                    holder.menu_dis.setMaxLines(1);
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout layout_menu_item_main,layout_menu_category;

        ImageView menu_image;

        TextView menu_category,menu_name;

        TextView menu_dis;

        RecyclerView rv_menu_price_list;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            layout_menu_item_main = itemView.findViewById(R.id.layout_menu_item_main);
            mContext.overrideFonts(layout_menu_item_main);

            layout_menu_category = itemView.findViewById(R.id.layout_menu_category);

            menu_image = itemView.findViewById(R.id.menu_image);

            menu_category = itemView.findViewById(R.id.menu_category);
            menu_name = itemView.findViewById(R.id.menu_name);
            menu_dis = itemView.findViewById(R.id.menu_dis);

            rv_menu_price_list = itemView.findViewById(R.id.rv_menu_price_list);
            rv_menu_price_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        }
    }
}
