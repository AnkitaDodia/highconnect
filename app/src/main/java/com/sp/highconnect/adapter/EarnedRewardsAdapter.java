package com.sp.highconnect.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.fragments.MyRewardsFragment;
import com.sp.highconnect.model.HomeOffer;
import com.sp.highconnect.model.MyEarnedRewards;

import java.util.ArrayList;

/**
 * Created by My 7 on 15-Aug-18.
 */

public class EarnedRewardsAdapter extends RecyclerView.Adapter<EarnedRewardsAdapter.MyViewHolder> {
    DashboardActivity mContext;
    MyRewardsFragment mMyRewardsFragment;

    ArrayList<MyEarnedRewards> mEarnedRewardsList = new ArrayList<>();

    public EarnedRewardsAdapter(DashboardActivity mContext, MyRewardsFragment myRewardsFragment, ArrayList<MyEarnedRewards> EarnedRewardsList) {
        this.mContext = mContext;
        mEarnedRewardsList = EarnedRewardsList;
        mMyRewardsFragment = myRewardsFragment;
    }

    @NonNull
    @Override
    public EarnedRewardsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_earned_rewards_list, null);

        return new EarnedRewardsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final EarnedRewardsAdapter.MyViewHolder holder, int position) {
        final MyEarnedRewards data = mEarnedRewardsList.get(position);

        try {
            Glide.with(mContext)
                    .load(data.getDispensaryLogo())
                    .into(holder.img_dispensary_item);

            Glide.with(mContext)
                    .load(data.getRewardLogo())
                    .into(holder.img_earned_reward);

            holder.text_dispensary_name_list.setText(data.getDispensaryName());
            holder.text_rewards_tital_list.setText(data.getRewardDescription());
            holder.txt_rewards_required_star.setText(data.getRewardsStar());

            holder.ll_row_cancel_rewards.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    BaseActivity.RewardId = data.getRewardId();
                    BaseActivity.RewardLocationId = data.getDispensaryId();

//                    mMyRewardsFragment.requestCancelRewards();
                    Log.e("EARNED_REWARDS_Id","RewardId : "+data.getRewardId());

                    mMyRewardsFragment.ShowCancelRewardsDialog(data.getRewardDescription(), data.getDispensaryName(), data.getRewardsStar());

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return mEarnedRewardsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_earned_reward, img_dispensary_item;
        TextView text_dispensary_name_list, text_rewards_tital_list, txt_rewards_required_star;
        LinearLayout ll_row_cancel_rewards;

        public MyViewHolder(View view) {
            super(view);

            ll_row_cancel_rewards = view.findViewById(R.id.ll_row_cancel_rewards);

            img_earned_reward = view.findViewById(R.id.img_earned_reward);
            img_dispensary_item = view.findViewById(R.id.img_dispensary_item);

            text_dispensary_name_list = view.findViewById(R.id.text_dispensary_name_list);
            text_rewards_tital_list = view.findViewById(R.id.text_rewards_tital_list);
            txt_rewards_required_star = view.findViewById(R.id.txt_rewards_required_star);

//            mContext.overrideFonts(offer_list_main);
        }
    }
}
