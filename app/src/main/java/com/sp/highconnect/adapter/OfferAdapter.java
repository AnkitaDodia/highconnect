package com.sp.highconnect.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.HomeOffer;
import com.sp.highconnect.model.Offer;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by My 7 on 15-Aug-18.
 */

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.MyViewHolder>
{
    DashboardActivity mContext;

    ArrayList<HomeOffer> mList = new ArrayList<>();

    public OfferAdapter(DashboardActivity mContext, ArrayList<HomeOffer> mOfferList)
    {
        this.mContext = mContext;
        mList = mOfferList;
    }

    @NonNull
    @Override
    public OfferAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_offer_list, null);

        return new OfferAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final OfferAdapter.MyViewHolder holder, int position) {
        HomeOffer data = mList.get(position);

//        Log.e("IMAGE_PATH",""+data.getOffer().getImageUrl());
        Log.e("offers","adapter offers size: "+mList.size());

        try {
            String path = "https://tripntvcontent.s3.us-west-2.amazonaws.com/"+data.getOffer().getImageUrl();
            Glide.with(mContext).load(path).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    holder.progress_offer_list.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    holder.progress_offer_list.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.img_offer_list);

            Glide.with(mContext).load(data.getDispensaryImage()).into(holder.img_dispensary_item);
        }catch (Exception e){
            e.printStackTrace();
        }

        holder.text_dispensary_name_list.setText(data.getDispensaryName());
        holder.text_offer_tital_list.setText(data.getOffer().getTitle());

        Double latitude = data.getLocation().getCoordinates().get(0);
        Double longitude = data.getLocation().getCoordinates().get(1);

        String dis = mContext.CalculationByDistance(BaseActivity.currnet_latitude,BaseActivity.currnet_longitude,latitude,longitude);
        holder.text_distance_list.setText(dis);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout offer_list_main;

        ImageView img_offer_list;

        ProgressBar progress_offer_list;

        CircleImageView img_dispensary_item;

        TextView text_dispensary_name_list,text_offer_tital_list,text_distance_list;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            offer_list_main = itemView.findViewById(R.id.offer_list_main);

            img_offer_list = itemView.findViewById(R.id.img_offer_list);

            progress_offer_list = itemView.findViewById(R.id.progress_offer_list);

            img_dispensary_item = itemView.findViewById(R.id.img_dispensary_item);

            text_dispensary_name_list = itemView.findViewById(R.id.text_dispensary_name_list);
            text_offer_tital_list = itemView.findViewById(R.id.text_offer_tital_list);
            text_distance_list = itemView.findViewById(R.id.text_distance_list);

            mContext.overrideFonts(offer_list_main);
        }
    }
}
