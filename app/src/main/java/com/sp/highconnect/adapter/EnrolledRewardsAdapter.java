package com.sp.highconnect.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.model.EnrolledRewardsHistory;
import com.sp.highconnect.model.MyRewardsHistory;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableListPosition;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.ArrayList;

public class EnrolledRewardsAdapter extends ExpandableRecyclerViewAdapter<DispensaryViewHolder, HistoryViewHolder> {

//    private Activity activity;
    DashboardActivity activity;
    EnrolledRewardsAdapter mEnrolledRewardsAdapter;
//    ArrayList<EnrolledRewardsHistory> mEnrolledRewardsHistoryList = new ArrayList<>();

    public EnrolledRewardsAdapter(DashboardActivity activity, ArrayList<EnrolledRewardsHistory> mEnrolledRewardsHistoryList) {
        super(mEnrolledRewardsHistoryList);
        this.activity = activity;
        mEnrolledRewardsAdapter = this;
    }


    @Override
    public DispensaryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_enrolled_rewards, parent, false);

        return new DispensaryViewHolder(view);
    }

    @Override
    public HistoryViewHolder onCreateChildViewHolder(ViewGroup parent, final int viewType) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_rewards_history, parent, false);

        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(HistoryViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final MyRewardsHistory myRewardsHistory = ((EnrolledRewardsHistory) group).getItems().get(childIndex);
        holder.onBind(myRewardsHistory, group);
    }

    @Override
    public void onBindGroupViewHolder(final DispensaryViewHolder holder, final int flatPosition, ExpandableGroup group) {
//        EnrolledRewardsHistory mEnrolledRewardsHistory
//        mEnrolledRewardsHistoryList.get(flatPosition);
//        holder.onBind(mEnrolledRewardsHistoryList.get(flatPosition), group);

        holder.setGroupName(activity, group);

        holder.txt_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mEnrolledRewardsAdapter.isGroupExpanded(flatPosition)){
                    holder.txt_history.setText("History +");
                }else {
                    holder.txt_history.setText("History -");
                }

                mEnrolledRewardsAdapter.toggleGroup(flatPosition);


            }
        });
    }


}

class DispensaryViewHolder extends GroupViewHolder {

    private ImageView img_row_dispensary;
    public TextView txt_dispensary_name, txt_dispensary_star, txt_history;

    public DispensaryViewHolder(View itemView) {
        super(itemView);

        img_row_dispensary = (ImageView) itemView.findViewById(R.id.img_row_dispensary);
        txt_dispensary_name = (TextView) itemView.findViewById(R.id.txt_dispensary_name);
        txt_dispensary_star = (TextView) itemView.findViewById(R.id.txt_dispensary_star);
        txt_history = (TextView) itemView.findViewById(R.id.txt_history);
    }

    @Override
    public void expand() {
//        txt_history.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        Log.i("Adapter", "expand");
        txt_history.setText("History -");
    }

    @Override
    public void collapse() {
        Log.i("Adapter", "collapse");
//        txt_history.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
        txt_history.setText("History +");
    }

    public void setGroupName(DashboardActivity activity, final ExpandableGroup group) {
        txt_dispensary_name.setText(group.getTitle());
        txt_dispensary_star.setText(((EnrolledRewardsHistory) group).getAvailableStars());
        Glide.with(activity)
                .load(((EnrolledRewardsHistory) group).getLargelogourl())
                .into(img_row_dispensary);



    }




    /*public void onBind(EnrolledRewardsHistory mEnrolledRewardsHistory, ExpandableGroup group) {
        txt_dispensary_name.setText(mEnrolledRewardsHistory.getDispensaryname());

    }*/
}

class HistoryViewHolder extends ChildViewHolder {

    private TextView txt_row_eventtype, txt_row_time, txt_row_reason, txt_row_budtender, txt_row_stars;

    public HistoryViewHolder(View itemView) {
        super(itemView);

        txt_row_eventtype = (TextView) itemView.findViewById(R.id.txt_row_eventtype);
        txt_row_time = (TextView) itemView.findViewById(R.id.txt_row_time);
        txt_row_reason = (TextView) itemView.findViewById(R.id.txt_row_reason);
        txt_row_budtender = (TextView) itemView.findViewById(R.id.txt_row_budtender);
        txt_row_stars = (TextView) itemView.findViewById(R.id.txt_row_stars);

    }

    public void onBind(MyRewardsHistory mMyRewardsHistory, ExpandableGroup group) {
        txt_row_eventtype.setText(mMyRewardsHistory.getEventType());
        txt_row_time.setText(BaseActivity.getDate(Long.parseLong(mMyRewardsHistory.getTime()), "dd/MM/yyyy hh:mm:ss.SSS"));
        txt_row_reason.setText(mMyRewardsHistory.getReason());
        txt_row_budtender.setText(mMyRewardsHistory.getBudtender());
        txt_row_stars.setText(mMyRewardsHistory.getStars());
    }
}
