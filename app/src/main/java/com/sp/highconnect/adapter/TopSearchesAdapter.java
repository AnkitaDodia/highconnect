package com.sp.highconnect.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.common.BaseActivity.ClickListener;
import com.sp.highconnect.model.TopSearches;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by My 7 on 22-Aug-18.
 */

public class TopSearchesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<TopSearches> mList = new ArrayList<>();
    private ClickListener mlistener;

    DashboardActivity mContext;

    public TopSearchesAdapter(DashboardActivity mContext, ArrayList<TopSearches> list, ClickListener listener)
    {
        this.mContext = mContext;
        mList = list;
        mlistener = listener;
    }

    @Override
    public TopSearchesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_top_searches_item, null);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        itemView.setLayoutParams(lp);

        return new TopSearchesViewHolder(itemView, mlistener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        TopSearches data = mList.get(position);
//        MyViewHolder mMyViewHolder = new MyViewHolder();
        TopSearchesViewHolder mMyViewHolder= (TopSearchesViewHolder) holder;

        if (position == 0){
            mMyViewHolder.txt_item_top_searches_title.setTextColor(ContextCompat.getColor(mContext, R.color.concentrates));
        }else {
            mMyViewHolder.txt_item_top_searches_title.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }

//        mMyViewHolder.ll_item_top_searches_cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                mList.get(position).getSearchId();
//                Toast.makeText(mContext, ""+mList.get(position).getSearchId(), Toast.LENGTH_SHORT).show();
//
//            }
//        });


        mMyViewHolder.txt_item_top_searches_title.setText(data.getSearchId());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class TopSearchesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        LinearLayout ll_main_top_searches, ll_item_top_searches_cancel ;
        TextView txt_item_top_searches_title;
        private WeakReference<ClickListener> listenerRef;

        public TopSearchesViewHolder(View itemView, ClickListener listener) {
            super(itemView);

            listenerRef = new WeakReference<>(listener);
            ll_main_top_searches = itemView.findViewById(R.id.ll_main_top_searches);
            ll_item_top_searches_cancel = itemView.findViewById(R.id.ll_item_top_searches_cancel);
            mContext.overrideFonts(ll_main_top_searches);

            txt_item_top_searches_title = itemView.findViewById(R.id.txt_item_top_searches_title);

            itemView.setOnClickListener(this);
            ll_item_top_searches_cancel.setOnClickListener(this);
        }

        // onClick Listener for view
        @Override
        public void onClick(View v) {

            /*if (v.getId() == ll_item_top_searches_cancel.getId()) {
                Toast.makeText(v.getContext(), "ITEM PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(v.getContext(), "ROW PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            }*/

            listenerRef.get().onClick(v,getAdapterPosition());
        }


        //onLongClickListener for view
        @Override
        public boolean onLongClick(View v) {

            listenerRef.get().onLongClick(v, getAdapterPosition());
            return true;
        }
    }
}
