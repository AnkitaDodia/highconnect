package com.sp.highconnect.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.fragments.OfferPagerFragment;
import com.sp.highconnect.model.HomeOffer;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SearchOfferPagerAdapter extends PagerAdapter {

    private DashboardActivity mContext;
    private LayoutInflater layoutInflater;
    public ArrayList<HomeOffer> mAll_Offers_List = new ArrayList<>();

    public SearchOfferPagerAdapter(DashboardActivity context, ArrayList<HomeOffer> mList) {
        this.mContext = context;
        this.mAll_Offers_List = mList;
    }

    @Override
    public int getCount() {
//        mAll_Offers_List.size()
        return mAll_Offers_List.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = layoutInflater.inflate(R.layout.item_search_offer, null);

        HomeOffer data = mAll_Offers_List.get(position);


        LinearLayout offer_list_main = itemView.findViewById(R.id.offer_list_main);

        ImageView img_offer_list = itemView.findViewById(R.id.img_offer_list);

        final ProgressBar progress_offer_list  = itemView.findViewById(R.id.progress_offer_list);

        CircleImageView img_dispensary_item = itemView.findViewById(R.id.img_dispensary_item);

        TextView text_dispensary_name_list = itemView.findViewById(R.id.text_dispensary_name_list);
        TextView text_offer_tital_list = itemView.findViewById(R.id.text_offer_tital_list);
        TextView text_distance_list = itemView.findViewById(R.id.text_distance_list);

        mContext.overrideFonts(offer_list_main);

        String path = "https://tripntvcontent.s3.us-west-2.amazonaws.com/"+data.getOffer().getImageUrl();

        Glide.with(mContext).load(path).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                progress_offer_list.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progress_offer_list.setVisibility(View.GONE);
                return false;
            }
        }).into(img_offer_list);

        Glide.with(mContext).load(data.getDispensaryImage()).into(img_dispensary_item);

        text_dispensary_name_list.setText(data.getDispensaryName());
        text_offer_tital_list.setText(data.getOffer().getTitle());

        Double latitude = data.getLocation().getCoordinates().get(0);
        Double longitude = data.getLocation().getCoordinates().get(1);

        String dis = mContext.CalculationByDistance(BaseActivity.currnet_latitude,BaseActivity.currnet_longitude,latitude,longitude);
        text_distance_list.setText(dis);

        itemView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                //this will log the page number that was click
                Log.i("TAG", "This page was clicked: " + position);

                for (int i = 0; i < BaseActivity.mDISPENSARY_LIST.size(); i++) {
                    if (BaseActivity.mDISPENSARY_LIST.get(i).getLocationId().equalsIgnoreCase(BaseActivity.mALL_HOME_OFFERS_LIST.get(position).getOffer().getDispensaryId())) {
                        BaseActivity.dispensaryName = BaseActivity.mDISPENSARY_LIST.get(i).getName();
                        BaseActivity.dispensaryImage = BaseActivity.mDISPENSARY_LIST.get(i).getSmlogo();
                        BaseActivity.mOFFERS_LIST = BaseActivity.mDISPENSARY_LIST.get(i).getOffers();
                        BaseActivity.mDispensaryData = BaseActivity.mDISPENSARY_LIST.get(i);
                        Log.e("OFFER_SIZE", "" + BaseActivity.mOFFERS_LIST.size());
                        break;
                    }
                }

                BaseActivity.mOffersFromWhere = 3;
                mContext.replaceFragment(new OfferPagerFragment());
            }
        });


        ViewPager vp = (ViewPager) container;
        vp.addView(itemView, 0);
        return itemView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}