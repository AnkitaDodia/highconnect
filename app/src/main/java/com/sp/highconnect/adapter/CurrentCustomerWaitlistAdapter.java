package com.sp.highconnect.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.model.WaitlistCustomers;

import java.util.ArrayList;

public class CurrentCustomerWaitlistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<WaitlistCustomers> mList = new ArrayList<>();

    DashboardActivity mContext;

    public CurrentCustomerWaitlistAdapter(DashboardActivity mContext, ArrayList<WaitlistCustomers> list)
    {
        this.mContext = mContext;
        mList = list;
    }

    @Override
    public CustomerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_customer_waitlist_item, null);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        itemView.setLayoutParams(lp);

        return new CustomerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        WaitlistCustomers data = mList.get(position);
        CustomerViewHolder mMyViewHolder= (CustomerViewHolder) holder;

        int number = position + 1 ;
        mMyViewHolder.txt_item_customer_name.setText(data.getNickname());
        mMyViewHolder.txt_item_customer_number.setText(number+".");
        mMyViewHolder.txt_item_customer_waittime.setText("~"+millisecToTime(number*data.getAvgWaitlistTime()));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class CustomerViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ll_main_customer_waitlist ;

        TextView txt_item_customer_number, txt_item_customer_name, txt_item_customer_waittime;

        public CustomerViewHolder(View itemView) {
            super(itemView);

            ll_main_customer_waitlist = itemView.findViewById(R.id.ll_main_customer_waitlist);
            mContext.overrideFonts(ll_main_customer_waitlist);

            txt_item_customer_number = itemView.findViewById(R.id.txt_item_customer_number);
            txt_item_customer_name = itemView.findViewById(R.id.txt_item_customer_name);
            txt_item_customer_waittime = itemView.findViewById(R.id.txt_item_customer_waittime);
        }
    }

    private String millisecToTime(long millisec) {
        long sec = millisec/1000;
        long second = sec % 60;
        long minute = sec / 60;
        if (minute >= 60) {
            long hour = minute / 60;
            minute %= 60;
//            return hour + ":" + (minute < 10 ? "0" + minute : minute) + ":" + (second < 10 ? "0" + second : second);
            return hour +"h" ;
        }
//        return minute + ":" + (second < 10 ? "0" + second : second);
        return minute + "m";
    }
}
