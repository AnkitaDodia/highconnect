package com.sp.highconnect.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.fragments.DispensaryDetailFragment;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.model.DispensaryPojo;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class AllDispensaryAdapter extends RecyclerView.Adapter<AllDispensaryAdapter.MyViewHolder>
{
    ArrayList<DispensaryPojo> mList = new ArrayList<>();

    DashboardActivity mContext;

    public AllDispensaryAdapter(DashboardActivity mContext, ArrayList<DispensaryPojo> dispensaryPojos)
    {
        this.mContext = mContext;
        mList = dispensaryPojos;
    }

    @Override
    public AllDispensaryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_all_dispensary_list, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AllDispensaryAdapter.MyViewHolder holder, final int position) {
        DispensaryPojo data = mList.get(position);

        Double latitude = data.getLocation().getCoordinates().get(0);
        Double longitude = data.getLocation().getCoordinates().get(1);

//        Double dis =  mContext.distance(BaseActivity.lat,BaseActivity.longi,latitude,longitude);

//        LatLng StartP = new LatLng(BaseActivity.lat,BaseActivity.longi);
//        LatLng EndP = new LatLng(latitude,longitude);
//
//        double dis = mContext.CalculationByDistance(StartP,EndP);

//        holder.all_dispensary_dis.setText(dis+"km");

        String dis = mContext.CalculationByDistance(BaseActivity.currnet_latitude,BaseActivity.currnet_longitude,latitude,longitude);
        holder.all_dispensary_dis.setText(dis);

        holder.all_dispensary_name.setText(data.getName());
        holder.all_dispensary_distance.setText(data.getDescription());

        Glide.with(mContext)
                .load(data.getSmlogo())
                .into(holder.all_dispensary_image);

        holder.all_dispensary_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("getLocation","getLocation : "+mList.get(position).getLocation().getCoordinates().toString());
                BaseActivity.mDispensaryData = mList.get(position);
                mContext.replaceFragment(new DispensaryDetailFragment());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout all_dispensary_main;

        ImageView all_dispensary_image;

        TextView all_dispensary_name,all_dispensary_dis,all_dispensary_distance;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            all_dispensary_main = itemView.findViewById(R.id.all_dispensary_main);

            all_dispensary_image = itemView.findViewById(R.id.all_dispensary_image);

            all_dispensary_name = itemView.findViewById(R.id.all_dispensary_name);
            all_dispensary_dis = itemView.findViewById(R.id.all_dispensary_dis);
            all_dispensary_distance = itemView.findViewById(R.id.all_dispensary_distance);

            mContext.overrideFonts(all_dispensary_main);
        }
    }
}
