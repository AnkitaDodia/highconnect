package com.sp.highconnect.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.model.Cost;

import java.util.ArrayList;

/**
 * Created by My 7 on 22-Aug-18.
 */

public class MenuPriceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<Cost> mList = new ArrayList<>();

    DashboardActivity mContext;

    public MenuPriceAdapter(DashboardActivity mContext, ArrayList<Cost> list)
    {
        this.mContext = mContext;
        mList = list;
    }

    @Override
    public MenuPriceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_menu_item_price, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Cost data = mList.get(position);
//        MyViewHolder mMyViewHolder = new MyViewHolder();
        MyViewHolder mMyViewHolder= (MyViewHolder) holder;
        mMyViewHolder.text_menu_price.setText(data.getPrice());
        mMyViewHolder.text_menu_unit.setText(data.getUnit());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout menu_price_item_main;

        TextView text_menu_price,text_menu_unit;

        public MyViewHolder(View itemView) {
            super(itemView);

            menu_price_item_main = itemView.findViewById(R.id.menu_price_item_main);
            mContext.overrideFonts(menu_price_item_main);

            text_menu_price = itemView.findViewById(R.id.text_menu_price);
            text_menu_unit = itemView.findViewById(R.id.text_menu_unit);
        }
    }
}
