package com.sp.highconnect.adapter;

import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.fragments.DispensaryDetailFragment;
import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.R;
import com.sp.highconnect.model.DispensaryPojo;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class DispensaryAdapter extends RecyclerView.Adapter<DispensaryAdapter.MyViewHolder>
{
    ArrayList<DispensaryPojo> mList = new ArrayList<>();

    DashboardActivity mContext;

    public DispensaryAdapter(DashboardActivity mContext, ArrayList<DispensaryPojo> dispensaryPojos)
    {
        this.mContext = mContext;
        mList = dispensaryPojos;
    }

    @Override
    public DispensaryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_dispensary_list, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DispensaryAdapter.MyViewHolder holder,final int position) {
        DispensaryPojo data = mList.get(position);

        ArrayList<Double> coordinates = data.getLocation().getCoordinates();
        Double latitude = coordinates.get(1);
        Double longitude = coordinates.get(0);

        Log.e("Distance","Name :"+data.getName()+" latFrom : "+BaseActivity.currnet_latitude+"  longFrom : "+BaseActivity.currnet_longitude+"  latTo : "+latitude+"  longTo : "+longitude);
        String dis = mContext.CalculationByDistance(BaseActivity.currnet_latitude,BaseActivity.currnet_longitude,latitude,longitude);

        holder.row_dispensary_dis.setText(dis);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(300,
                ViewGroup.LayoutParams.MATCH_PARENT);
        holder.row_dispensary_main.setLayoutParams(params);

        holder.row_dispensary_name.setText(data.getName());

        Glide.with(mContext)
                .load(data.getSmlogo())
                .into(holder.row_dispensary_image);

        holder.row_dispensary_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.mDispensaryData = mList.get(position);
                mContext.replaceFragment(new DispensaryDetailFragment());

//                Toast.makeText(mContext, "Click From MyDispensary Adapter", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout row_dispensary_main;

        ImageView row_dispensary_image;

        TextView row_dispensary_name,row_dispensary_dis;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            row_dispensary_main = itemView.findViewById(R.id.row_dispensary_main);

            row_dispensary_image = itemView.findViewById(R.id.row_dispensary_image);

            row_dispensary_name = itemView.findViewById(R.id.row_dispensary_name);
            row_dispensary_dis = itemView.findViewById(R.id.row_dispensary_dis);

            mContext.overrideFonts(row_dispensary_main);
        }
    }
}
