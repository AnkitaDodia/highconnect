package com.sp.highconnect.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.sp.highconnect.DashboardActivity;
import com.sp.highconnect.fragments.OfferDetailFragment;

/**
 * Created by My 7 on 16-Aug-18.
 */

public class OfferPagerAdapter extends FragmentPagerAdapter
{
    private DashboardActivity mContext;

    private int no_of_offers;

    public OfferPagerAdapter(FragmentManager childFragmentManager, DashboardActivity mContext, int no_of_offers) {
        super(childFragmentManager);
        this.mContext = mContext;
        this.no_of_offers = no_of_offers;
        Log.e("no_of_offers",""+no_of_offers);
    }

    @Override
    public Fragment getItem(int position) {
        return(OfferDetailFragment.newInstance(mContext,position));
    }

    @Override
    public int getCount() {
        return no_of_offers;
    }
}