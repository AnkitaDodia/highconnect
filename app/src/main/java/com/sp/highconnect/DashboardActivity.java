package com.sp.highconnect;


import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sp.highconnect.common.BaseActivity;
import com.sp.highconnect.fragments.CustomerQueueFragment;
import com.sp.highconnect.fragments.DispensaryDetailFragment;
import com.sp.highconnect.fragments.HomeFragment;
import com.sp.highconnect.fragments.MenuFragment;
import com.sp.highconnect.fragments.MyAccountFragment;
import com.sp.highconnect.fragments.MyRewardsFragment;
import com.sp.highconnect.fragments.OfferPagerFragment;
import com.sp.highconnect.fragments.PrivacyPolicyFragment;
import com.sp.highconnect.fragments.RewardsDetailFragment;
import com.sp.highconnect.fragments.RewardsPagerFragment;
import com.sp.highconnect.fragments.SearchFragment;
import com.sp.highconnect.fragments.SearchResultFragment;
import com.sp.highconnect.fragments.SignupFragment;
import com.sp.highconnect.fragments.TermsofUseFragment;
import com.sp.highconnect.fragments.ViewAllDispensaryFragment;
import com.sp.highconnect.utility.DividerItemDecoration;

import java.util.List;

public class DashboardActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Context mContext;
    DrawerLayout drawer;
    ImageButton menuRight;
    NavigationView navigationView;
    ImageView img__text_logo;

    Fragment currentFragment;
    LinearLayout ll_nav_back;

    RelativeLayout lay_topbar_queue;
    TextView txt_waitlist_queue_no;
    //For exit from application
    boolean doubleBackToExitPressedOnce = false;
    //For exit from application

    public static boolean mMapIsTouched = false, markerClicked = false;
    public static final int PICK_IMAGE = 1;
    public static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        menuRight = (ImageButton) findViewById(R.id.menuRight);

        menuRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
            }
        });


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
        navMenuView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL_LIST));

        img__text_logo = findViewById(R.id.img__text_logo);
        img__text_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(new HomeFragment());
//                addHomeFragment(BaseActivity.HOME_FRAGMENT_TAG);
            }
        });

        lay_topbar_queue = findViewById(R.id.lay_topbar_queue);
        txt_waitlist_queue_no = findViewById(R.id.txt_waitlist_queue_no);

        ll_nav_back = findViewById(R.id.ll_nav_back);
        ll_nav_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    performBackTransaction();

//            if (getFragmentManager().getBackStackEntryCount() == 0) {
//                super.onBackPressed();
//            } else {
//                getFragmentManager().popBackStack();
//            }
                }

            }
        });

        if( savedInstanceState == null )
            replaceFragment(new HomeFragment());
//        addHomeFragment(BaseActivity.HOME_FRAGMENT_TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String text = "";
        if (id == R.id.nav_home) {
            text = "home";

            HomeFragment mHomeFragment = new HomeFragment();
            replaceFragment(mHomeFragment);

//            addHomeFragment(BaseActivity.HOME_FRAGMENT_TAG);

        } else if (id == R.id.nav_search) {
            text = "nav_search";

            SearchFragment mSearchFragment = new SearchFragment();
            replaceFragment(mSearchFragment);

//            addSearchFragment(BaseActivity.SEARCH_FRAGMENT_TAG);

        } else if (id == R.id.nav_my_account) {
            text = "nav_my_account";

            MyAccountFragment mMyAccountFragment = new MyAccountFragment();
            replaceFragment(mMyAccountFragment);

        }else if (id == R.id.nav_my_rewards) {
            text = "nav_my_rewards";

            MyRewardsFragment mMyRewardsFragment = new MyRewardsFragment();
            replaceFragment(mMyRewardsFragment);

        }else if (id == R.id.nav_terms_of_use) {

            text = "nav_terms_of_use";

            TermsofUseFragment mTermsofUseFragment = new TermsofUseFragment();
            replaceFragment(mTermsofUseFragment);

        }else if (id == R.id.nav_privacy_policy) {

            text = "nav_privacy_policy";

            PrivacyPolicyFragment mPrivacyPolicyFragment= new PrivacyPolicyFragment();
            replaceFragment(mPrivacyPolicyFragment);

        }

        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    public void replaceFragment(Fragment targetFragment){

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, targetFragment,targetFragment.getClass().getName());
        transaction.addToBackStack(targetFragment.getClass().getName());
//        transaction.commit();
        transaction.commitAllowingStateLoss();

        currentFragment = targetFragment;
    }

    public void performBackTransaction()
    {
        FragmentManager fm = getSupportFragmentManager();

        try {
            if(fm.getBackStackEntryCount() > 1)
            {
//                fm.popBackStackImmediate();

                String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
                Log.e("MainActivity","Current fragment tag is: "+tag);

                findTheFragmentWhilePoping(tag);
            }
            else
            {
                Log.i("MainActivity","Nothing in back stack call super");
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();

                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(mContext,"Please click BACK again to exit",Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce=false;
                    }
                }, 2000);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void findTheFragmentWhilePoping(String tag)
    {
       FragmentManager fm = getSupportFragmentManager();

        if(tag.equalsIgnoreCase(HomeFragment.class.getName()))
        {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                System.exit(0);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(mContext,"Please click BACK again to exit",Toast.LENGTH_LONG).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);

        }
        else if(tag.equalsIgnoreCase(SearchFragment.class.getName()))
        {
            HomeFragment f = (HomeFragment) fm.findFragmentByTag(HomeFragment.class.getName());
            currentFragment = f;
        }
        else if(tag.equalsIgnoreCase(MyAccountFragment.class.getName()))
        {
            HomeFragment f = (HomeFragment) fm.findFragmentByTag(HomeFragment.class.getName());
            currentFragment = f;
        }
        else if(tag.equalsIgnoreCase(MyRewardsFragment.class.getName()))
        {
            HomeFragment f = (HomeFragment) fm.findFragmentByTag(HomeFragment.class.getName());
            currentFragment = f;
        }
        else if(tag.equalsIgnoreCase(TermsofUseFragment.class.getName()))
        {
            HomeFragment f = (HomeFragment) fm.findFragmentByTag(HomeFragment.class.getName());
            currentFragment = f;
        }
        else if(tag.equalsIgnoreCase(PrivacyPolicyFragment.class.getName()))
        {
            HomeFragment f = (HomeFragment) fm.findFragmentByTag(HomeFragment.class.getName());
            currentFragment = f;
        }

        else if(tag.equalsIgnoreCase(ViewAllDispensaryFragment.class.getName()))
        {
            HomeFragment f = (HomeFragment) fm.findFragmentByTag(HomeFragment.class.getName());
            currentFragment = f;
        }
        else if(tag.equalsIgnoreCase(DispensaryDetailFragment.class.getName()))
        {
            HomeFragment f = (HomeFragment) fm.findFragmentByTag(HomeFragment.class.getName());
            currentFragment = f;
        }
        else if(tag.equalsIgnoreCase(SearchResultFragment.class.getName()))
        {
            SearchFragment f = (SearchFragment) fm.findFragmentByTag(SearchFragment.class.getName());
            currentFragment = f;
        }
        else if(tag.equalsIgnoreCase(MenuFragment.class.getName()))
        {
            DispensaryDetailFragment f = (DispensaryDetailFragment) fm.findFragmentByTag(DispensaryDetailFragment.class.getName());
            currentFragment = f;

        }else if(tag.equalsIgnoreCase(RewardsDetailFragment.class.getName())){

            DispensaryDetailFragment f = (DispensaryDetailFragment) fm.findFragmentByTag(DispensaryDetailFragment.class.getName());
            currentFragment = f;
        }else if(tag.equalsIgnoreCase(RewardsPagerFragment.class.getName())){

            DispensaryDetailFragment f = (DispensaryDetailFragment) fm.findFragmentByTag(DispensaryDetailFragment.class.getName());
            currentFragment = f;
        }else if(tag.equalsIgnoreCase(CustomerQueueFragment.class.getName())){

            DispensaryDetailFragment f = (DispensaryDetailFragment) fm.findFragmentByTag(DispensaryDetailFragment.class.getName());
            currentFragment = f;

        }else if(tag.equalsIgnoreCase(OfferPagerFragment.class.getName())){

            if(mOffersFromWhere == 0){

                HomeFragment f = (HomeFragment) fm.findFragmentByTag(HomeFragment.class.getName());
                currentFragment = f;

            }else if(mOffersFromWhere == 1){

                SearchResultFragment f = (SearchResultFragment) fm.findFragmentByTag(SearchResultFragment.class.getName());
                currentFragment = f;

            }else if(mOffersFromWhere == 2){

                DispensaryDetailFragment f = (DispensaryDetailFragment) fm.findFragmentByTag(DispensaryDetailFragment.class.getName());
                currentFragment = f;

            }else if(mOffersFromWhere == 3){

                SearchFragment f = (SearchFragment) fm.findFragmentByTag(SearchFragment.class.getName());
                currentFragment = f;
            }


        }

        replaceFragment(currentFragment);

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            performBackTransaction();
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, PICK_IMAGE);
                }
                break;
            case REQUEST_LOCATION_PERMISSION:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLastLocation();
                }

                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        Log.e("POSITION",""+BaseActivity.viewpagerPosition);

        List<Fragment> listOfFragments = getSupportFragmentManager().getFragments();

        if(listOfFragments.size() >= 1){
            for (Fragment fragment : listOfFragments) {

                if(fragment instanceof MyAccountFragment){
                    fragment.onActivityResult(requestCode, resultCode, data);
                }

            }
        }
    }

    public void ShowBadgeLayout(boolean isShow){

        if (isShow){
            lay_topbar_queue.setVisibility(View.VISIBLE);
        }else {
            lay_topbar_queue.setVisibility(View.GONE);
        }

    }

    public void SetQueueNumber(String no){

        txt_waitlist_queue_no.setText(""+no);
    }

}
