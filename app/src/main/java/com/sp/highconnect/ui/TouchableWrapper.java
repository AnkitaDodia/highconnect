package com.sp.highconnect.ui;

/**
 * Created by Adite-Ankita on 19-Oct-16.
 */
import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import com.sp.highconnect.DashboardActivity;

public class TouchableWrapper extends FrameLayout {

    public TouchableWrapper(Context context) {
        super(context);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                DashboardActivity.mMapIsTouched = false;
                break;

            case MotionEvent.ACTION_UP:
                DashboardActivity.mMapIsTouched = true;
                break;

            case MotionEvent.ACTION_MOVE:
                DashboardActivity.mMapIsTouched = false;
                DashboardActivity.markerClicked = false;
                break;
        }
        return super.dispatchTouchEvent(event);
    }
}