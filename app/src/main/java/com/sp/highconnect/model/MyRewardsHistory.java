package com.sp.highconnect.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MyRewardsHistory implements Parcelable{

   String eventType;
   String time;
   String reason;
   String budtender;
   String stars;
   String venueid;

   public MyRewardsHistory(Parcel in) {
      eventType = in.readString();
   }

    public MyRewardsHistory() {
    }


   public String getEventType() {
      return eventType;
   }

   public void setEventType(String eventType) {
      this.eventType = eventType;
   }

   public String getTime() {
      return time;
   }

   public void setTime(String time) {
      this.time = time;
   }

   public String getReason() {
      return reason;
   }

   public void setReason(String reason) {
      this.reason = reason;
   }

   public String getBudtender() {
      return budtender;
   }

   public void setBudtender(String budtender) {
      this.budtender = budtender;
   }

   public String getStars() {
      return stars;
   }

   public void setStars(String stars) {
      this.stars = stars;
   }

   public String getVenueid() {
      return venueid;
   }

   public void setVenueid(String venueid) {
      this.venueid = venueid;
   }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(eventType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MyRewardsHistory> CREATOR = new Creator<MyRewardsHistory>() {
        @Override
        public MyRewardsHistory createFromParcel(Parcel in) {
            return new MyRewardsHistory(in);
        }

        @Override
        public MyRewardsHistory[] newArray(int size) {
            return new MyRewardsHistory[size];
        }
    };
}
