package com.sp.highconnect.model;

import android.annotation.SuppressLint;
import android.os.Parcel;


import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ParcelCreator")
public class EnrolledRewardsHistory extends ExpandableGroup<MyRewardsHistory> {

    String mDispensaryname;
    String mAvailableStars;
    String mLargelogourl;

    ArrayList<MyRewardsHistory> RewardsHistoryList = new ArrayList<>();

    public EnrolledRewardsHistory(String dispensaryname, String availableStars, String largelogourl, List<MyRewardsHistory> items) {
        super(dispensaryname, items);
        this.mDispensaryname = dispensaryname;
        this.mAvailableStars = availableStars;
        this.mLargelogourl = largelogourl;
    }


    public String getDispensaryname() {
        return mDispensaryname;
    }

    public void setDispensaryname(String dispensaryname) {
        this.mDispensaryname = dispensaryname;
    }

    public String getAvailableStars() {
        return mAvailableStars;
    }

    public void setAvailableStars(String availableStars) {
        this.mAvailableStars = availableStars;
    }

    public String getLargelogourl() {
        return mLargelogourl;
    }

    public void setLargelogourl(String largelogourl) {
        this.mLargelogourl = largelogourl;
    }

    public ArrayList<MyRewardsHistory> getRewardsHistoryList() {
        return RewardsHistoryList;
    }

    public void setRewardsHistoryList(ArrayList<MyRewardsHistory> rewardsHistoryList) {
        RewardsHistoryList = rewardsHistoryList;
    }
}
