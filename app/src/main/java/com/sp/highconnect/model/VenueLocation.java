package com.sp.highconnect.model;

/**
 * Created by My 7 on 14-Aug-18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class VenueLocation {

    @SerializedName("coordinates")
    @Expose
    private ArrayList<Double> coordinates = null;
    @SerializedName("type")
    @Expose
    private String type;

    public ArrayList<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}