package com.sp.highconnect.model;

public class EarnedRewards {

    String rewardDescription;
    String venueid;
    String rewardid;


    public String getRewardDescription() {
        return rewardDescription;
    }

    public void setRewardDescription(String rewardDescription) {
        this.rewardDescription = rewardDescription;
    }

    public String getVenueid() {
        return venueid;
    }

    public void setVenueid(String venueid) {
        this.venueid = venueid;
    }

    public String getRewardId() {
        return rewardid;
    }

    public void setRewardId(String reward) {
        this.rewardid = reward;
    }

}
