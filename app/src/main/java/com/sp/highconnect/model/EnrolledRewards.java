package com.sp.highconnect.model;

import java.util.ArrayList;

public class EnrolledRewards {

    String availableStars;
    String venueid;

    public String getAvailableStars() {
        return availableStars;
    }

    public void setAvailableStars(String availableStars) {
        this.availableStars = availableStars;
    }

    public String getVenueid() {
        return venueid;
    }

    public void setVenueid(String venueid) {
        this.venueid = venueid;
    }

}
