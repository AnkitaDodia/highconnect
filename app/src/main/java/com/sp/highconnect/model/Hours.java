package com.sp.highconnect.model;

/**
 * Created by My 7 on 14-Aug-18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hours {

    @SerializedName("wednesday")
    @Expose
    private OpenClose wednesday;
    @SerializedName("tuesday")
    @Expose
    private OpenClose tuesday;
    @SerializedName("thursday")
    @Expose
    private OpenClose thursday;
    @SerializedName("sunday")
    @Expose
    private OpenClose sunday;
    @SerializedName("saturday")
    @Expose
    private OpenClose saturday;
    @SerializedName("monday")
    @Expose
    private OpenClose monday;
    @SerializedName("holiday")
    @Expose
    private OpenClose holiday;
    @SerializedName("friday")
    @Expose
    private OpenClose friday;

    public OpenClose getWednesday() {
        return wednesday;
    }

    public void setWednesday(OpenClose wednesday) {
        this.wednesday = wednesday;
    }

    public OpenClose getTuesday() {
        return tuesday;
    }

    public void setTuesday(OpenClose tuesday)
    {
        this.tuesday = tuesday;
    }

    public OpenClose getThursday() {
        return thursday;
    }

    public void setThursday(OpenClose thursday) {
        this.thursday = thursday;
    }

    public OpenClose getSunday() {
        return sunday;
    }

    public void setSunday(OpenClose sunday)
    {
        this.sunday = sunday;
    }

    public OpenClose getSaturday() {
        return saturday;
    }

    public void setSaturday(OpenClose saturday)
    {
        this.saturday = saturday;
    }

    public OpenClose getMonday() {
        return monday;
    }

    public void setMonday(OpenClose monday)
    {
        this.monday = monday;
    }

    public OpenClose getHoliday() {
        return holiday;
    }

    public void setHoliday(OpenClose holiday)
    {
        this.holiday = holiday;
    }

    public OpenClose getFriday() {
        return friday;
    }

    public void setFriday(OpenClose friday)
    {
        this.friday = friday;
    }
}