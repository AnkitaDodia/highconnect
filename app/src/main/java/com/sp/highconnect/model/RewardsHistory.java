package com.sp.highconnect.model;

import java.util.ArrayList;

public class RewardsHistory {

   String eventType;
   String time;
   String reason;
   String budtender;
   String stars;
   String venueid;

   public String getEventType() {
      return eventType;
   }

   public void setEventType(String eventType) {
      this.eventType = eventType;
   }

   public String getTime() {
      return time;
   }

   public void setTime(String time) {
      this.time = time;
   }

   public String getReason() {
      return reason;
   }

   public void setReason(String reason) {
      this.reason = reason;
   }

   public String getBudtender() {
      return budtender;
   }

   public void setBudtender(String budtender) {
      this.budtender = budtender;
   }

   public String getStars() {
      return stars;
   }

   public void setStars(String stars) {
      this.stars = stars;
   }

   public String getVenueid() {
      return venueid;
   }

   public void setVenueid(String venueid) {
      this.venueid = venueid;
   }
}
