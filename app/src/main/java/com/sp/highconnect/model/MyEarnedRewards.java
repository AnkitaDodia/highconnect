package com.sp.highconnect.model;

public class MyEarnedRewards {

    String RewardId;
    String RewardDescription;
    String RewardsStar;
    String RewardLogo;

    String DispensaryId;
    String DispensaryName;
    String DispensaryLogo;

    public String getRewardId() {
        return RewardId;
    }

    public void setRewardId(String rewardId) {
        RewardId = rewardId;
    }


    public String getRewardDescription() {
        return RewardDescription;
    }

    public void setRewardDescription(String rewardDescription) {
        RewardDescription = rewardDescription;
    }

    public String getRewardsStar() {
        return RewardsStar;
    }

    public void setRewardsStar(String rewardsStar) {
        RewardsStar = rewardsStar;
    }

    public String getRewardLogo() {
        return RewardLogo;
    }

    public void setRewardLogo(String rewardLogo) {
        RewardLogo = rewardLogo;
    }


    public String getDispensaryId() {
        return DispensaryId;
    }

    public void setDispensaryId(String dispensaryId) {
        DispensaryId = dispensaryId;
    }

    public String getDispensaryName() {
        return DispensaryName;
    }

    public void setDispensaryName(String dispensaryName) {
        DispensaryName = dispensaryName;
    }

    public String getDispensaryLogo() {
        return DispensaryLogo;
    }

    public void setDispensaryLogo(String dispensaryLogo) {
        DispensaryLogo = dispensaryLogo;
    }


}
