package com.sp.highconnect.model;

/**
 * Created by My 7 on 15-Aug-18.
 */

public class HomeOffer
{
    String dispensaryName;
    String dispensaryImage;
    VenueLocation location;
    Offer offer;

    public String getDispensaryName() {
        return dispensaryName;
    }

    public void setDispensaryName(String dispensaryName) {
        this.dispensaryName = dispensaryName;
    }

    public String getDispensaryImage() {
        return dispensaryImage;
    }

    public void setDispensaryImage(String dispensaryImage) {
        this.dispensaryImage = dispensaryImage;
    }

    public VenueLocation getLocation() {
        return location;
    }

    public void setLocation(VenueLocation location) {
        this.location = location;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }
}
