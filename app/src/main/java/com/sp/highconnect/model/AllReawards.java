package com.sp.highconnect.model;

public class AllReawards {

    String rewardId;
    String creationDate;
    String description;
    String imageURL;
    String redeemCode;
    String redeemLimit;
    String starsRequired;
    String venue;
    String status;
    String finePrint;


    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getRedeemCode() {
        return redeemCode;
    }

    public void setRedeemCode(String redeemCode) {
        this.redeemCode = redeemCode;
    }

    public String getRedeemLimit() {
        return redeemLimit;
    }

    public void setRedeemLimit(String redeemLimit) {
        this.redeemLimit = redeemLimit;
    }

    public String getStarsRequired() {
        return starsRequired;
    }

    public void setStarsRequired(String starsRequired) {
        this.starsRequired = starsRequired;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinePrint() {
        return finePrint;
    }

    public void setFinePrint(String finePrint) {
        this.finePrint = finePrint;
    }

}
