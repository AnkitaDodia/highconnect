package com.sp.highconnect.model;

/**
 * Created by My 7 on 14-Aug-18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DispensaryPojo implements Cloneable
{
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("smlogo")
    @Expose
    private String smlogo;
    @SerializedName("http")
    @Expose
    private String http;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("hours")
    @Expose
    private Hours hours;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("locationId")
    @Expose
    private String locationId;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("currentPlaylist")
    @Expose
    private String currentPlaylist;
    @SerializedName("waitlistEnabled")
    @Expose
    private Boolean waitlistEnabled;
    @SerializedName("preorderEnabled")
    @Expose
    private Boolean preorderEnabled;
    @SerializedName("rewardsEnabled")
    @Expose
    private Boolean rewardsEnabled;
    @SerializedName("menu")
    @Expose
    private ArrayList<DispensaryMenu> dispensaryMenus = null;
    @SerializedName("offers")
    @Expose
    private ArrayList<Offer> offers = null;
    @SerializedName("location")
    @Expose
    private VenueLocation location;

    private String dayName;

    public Object getDayName() {
        return dayName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSmlogo() {
        return smlogo;
    }

    public void setSmlogo(String smlogo) {
        this.smlogo = smlogo;
    }

    public String getHttp() {
        return http;
    }

    public void setHttp(String http) {
        this.http = http;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Hours getHours() {
        return hours;
    }

    public void setHours(Hours hours) {
        this.hours = hours;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCurrentPlaylist() {
        return currentPlaylist;
    }

    public void setCurrentPlaylist(String currentPlaylist) {
        this.currentPlaylist = currentPlaylist;
    }

    public Boolean getWaitlistEnabled() {
        return waitlistEnabled;
    }

    public void setWaitlistEnabled(Boolean waitlistEnabled) {
        this.waitlistEnabled = waitlistEnabled;
    }

    public Boolean getPreorderEnabled() {
        return preorderEnabled;
    }

    public void setPreorderEnabled(Boolean preorderEnabled) {
        this.preorderEnabled = preorderEnabled;
    }

    public Boolean getRewardsEnabled() {
        return rewardsEnabled;
    }

    public void setRewardsEnabled(Boolean rewardsEnabled) {
        this.rewardsEnabled = rewardsEnabled;
    }

    public ArrayList<DispensaryMenu> getDispensaryMenus() {
        return dispensaryMenus;
    }

    public void setDispensaryMenus(ArrayList<DispensaryMenu> dispensaryMenus) {
        this.dispensaryMenus = dispensaryMenus;
    }

    public ArrayList<Offer> getOffers() {
        return offers;
    }

    public void setOffers(ArrayList<Offer> offers) {
        this.offers = offers;
    }

    public VenueLocation getLocation() {
        return location;
    }

    public void setLocation(VenueLocation location) {
        this.location = location;
    }

    public DispensaryPojo clone()throws CloneNotSupportedException{
//        return super.clone();

        return new DispensaryPojo(  );

    }

}
