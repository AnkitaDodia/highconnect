package com.sp.highconnect.common;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sp.highconnect.R;
import com.sp.highconnect.model.AllReawards;
import com.sp.highconnect.model.DispensaryMenu;
import com.sp.highconnect.model.DispensaryPojo;
import com.sp.highconnect.model.HomeOffer;
import com.sp.highconnect.model.Offer;
import com.sp.highconnect.model.TopSearches;
import com.sp.highconnect.model.VenueLocation;
import com.sp.highconnect.model.Waitlist;
import com.sp.highconnect.model.WaitlistCustomers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BaseActivity extends AppCompatActivity {

    public static String mLink_Privacy_policy = "https://en.wikipedia.org/wiki/Privacy_policy";
    public static String mLink_Terms_of_Use = " https://en.wikipedia.org/wiki/Terms_of_service";


    public static final String TAG = "BASE_ACTIVITY";

    public static final String HOME_FRAGMENT_TAG = "HOME_FRAGMENT";
    public static final String SEARCH_FRAGMENT_TAG = "SEARCH_FRAGMENT";
    public static final String DEVICE_LIST_FRAGMENT_TAG = "DEVICE_LIST_FRAGMENT";
    public static final String DEVICE_CONTROL_FRAGMENT_TAG = "DEVICE_CONTROL_FRAGMENT";
    public static final String UPDATE_INFO_FRAGMENT_TAG = "UPDATE_INFO_FRAGMENT_TAG";
    public static final String MAIN_MENU_FRAGMENT_TAG = "MAIN_MENU_FRAGMENT_TAG";
    public static final String REMINDER_FRAGMENT_TAG = "REMINDER_FRAGMENT_TAG";
    public static final String ADD_REMINDER_FRAGMENT_TAG = "ADD_REMINDER_FRAGMENT_TAG";
    public static final String STOPWATCH_FRAGMENT_TAG = "STOPWATCH_FRAGMENT_TAG";


    public static final String SHAREDPREF_LISTSTORE_TAG = "SHAREDPREF_LISTSTORE_TAG";

    public static String fcmIdToken,dispensaryName,dispensaryImage,distance, LatestWaitList;

    ProgressDialog mProgressDialog;

    public static ArrayList<DispensaryPojo> mDISPENSARY_LIST = new ArrayList<>();//dispensary
//    public static ArrayList<ArrayList<Offer>> mALL_OFFERS_LIST = new ArrayList<>();//allOffers
    public static ArrayList<HomeOffer> mALL_HOME_OFFERS_LIST = new ArrayList<>();
    public static ArrayList<Offer> mOFFERS_LIST = new ArrayList<>();//offers
    public static ArrayList<VenueLocation> mMAPS_MARKER_LIST = new ArrayList<>();//mapMarkers
    public static ArrayList<DispensaryMenu> mMENU_LIST = new ArrayList<>();//mMenuList


    public static ArrayList<TopSearches> mTOP_SEARCHES_LIST = new ArrayList<>();
    public static ArrayList<AllReawards> mALL_REWARDS_LIST = new ArrayList<>();
    public static ArrayList<Waitlist> mWAITLIST_LIST = new ArrayList<>();

    public static String title_my_dispensaries = "DISPENSARIES";

    public static ArrayList<AllReawards> mDispensary_Rewards_list = new ArrayList<>();

    public static ArrayList<WaitlistCustomers> mWAITLIST_CUSTOMER_LIST = new ArrayList<>();

    public static ArrayList<String> MyDispensary = new ArrayList<>();

    public static double currnet_latitude , currnet_longitude;//= 21.721074 = 71.3836673

    public static DispensaryPojo mDispensaryData;
    public static int mAvailableStars;

    private Typeface font;

    public static boolean isFromMap = false;
    public static LatLng mMarkerClickLatlong;
    public static int mMarkerClickPosition;

    public static String RewardId;
    public static String RewardLocationId;

    public static int mOffersFromWhere = 0;
    public static boolean mIsFromTopPicks = false;
    public static String mToppickSearch = "";
    private String mCurrentFragmentTag;

    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;
    public static final int REQUEST_LOCATION_PERMISSION = 102;

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final int WEEK_MILLIS = 7 * DAY_MILLIS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }


    public static void GetToken(){

        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
        mUser.getIdToken(true)
                .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if (task.isSuccessful()) {
                            String idToken = task.getResult().getToken();
                            // Send token to your backend via HTTPS
                            fcmIdToken = idToken;
                            Log.e("idToken","idToken : "+idToken);
                            // ...
                        } else {
                            // Handle error -> task.getException();
                        }
                    }
                });
    }

    // Code for get and set login  0 is for no login and 1 is for logged in
    public void setLogin(int i)
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("Login",i);
        spe.apply();
    }

    public int getLogin()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        int i = sp.getInt("Login",0);
        return i;
    }

    // Code for get and set login
    public void saveImagePath(String path)
    {
        SharedPreferences sp = getSharedPreferences("ImagePath",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("path",path);
        spe.apply();
    }

    public String getImagePath()
    {
        SharedPreferences sp = getSharedPreferences("ImagePath",MODE_PRIVATE);
        String path = sp.getString("path",null);
        return path;
    }

    public void removeImagePath()
    {
        SharedPreferences sp = getSharedPreferences("ImagePath",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.remove("path");
        spe.commit();

    }


    // Code for get and set login

    public Typeface getRegularTypeFace()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/NunitoSans-Regular.ttf");
        return font;
    }


    public static void saveSharedPreferencesLogList(Context context, List<String> collageList) {
        SharedPreferences mPrefs = context.getSharedPreferences("MyDispensary", context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(collageList);
        prefsEditor.putString("myJson", json);
        prefsEditor.commit();
    }

    public static ArrayList<String> loadSharedPreferencesLogList(Context context) {
        ArrayList<String> savedCollage = new ArrayList<String>();
        SharedPreferences mPrefs = context.getSharedPreferences("MyDispensary", context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("myJson", "");
        if (json.isEmpty()) {
            savedCollage = new ArrayList<String>();
        } else {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            savedCollage = gson.fromJson(json, type);
        }

        return savedCollage;
    }


    public void overrideFonts(final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(getRegularTypeFace());
            }else if (v instanceof Button) {
                ((Button) v).setTypeface(getRegularTypeFace());
            }else if (v instanceof EditText) {
                ((EditText) v).setTypeface(getRegularTypeFace());
            }
        } catch (Exception e) {
        }
    }

    public void showWaitIndicator(boolean state) {
        showWaitIndicator(state, "");
    }

    public void showWaitIndicator(boolean state, String message) {
        try {
            try {

                if (state) {

                    mProgressDialog = new ProgressDialog(this, R.style.TransparentProgressDialog);
                    mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();
                } else {
                    mProgressDialog.dismiss();
                }

            } catch (Exception e) {


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String  distanceText;

    public String GetDistance(double latA, double lngA, double latB, double lngB){

        // Tag used to cancel the request
        distanceText = "wait";
        String url = getDirectionsUrl(latA, lngA, latB, lngB);

//        String url = "http://maps.googleapis.com/maps/api/directions/json?origin=21.769510,72.118914&destination=21.1702,72.8311&sensor=false&mode=%22DRIVING%22";

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                pDialog.hide();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray routes = jsonObject.getJSONArray("routes");

                    JSONObject routes1 = routes.getJSONObject(0);

                    JSONArray legs = routes1.getJSONArray("legs");

                    JSONObject legs1 = legs.getJSONObject(0);

                    JSONObject distance = legs1.getJSONObject("distance");

                    JSONObject duration = legs1.getJSONObject("duration");

                    distanceText = distance.getString("text");

                    Log.e("distanceText","distanceText : "+distanceText);

//                    durationText = duration.getString("text");

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        });

        Volley.newRequestQueue(this).add(strReq);
// Adding request to request queue
//       AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

        return distanceText;
    }

    private String getDirectionsUrl(double ori_lat, double ori_lng, double dest_lat, double dist_lng) {

        String str_origin = "origin=" + ori_lat + "," + ori_lng;

        String str_dest = "destination=" + dest_lat + "," + dist_lng;

        String sensor = "sensor=false";

        String mode = "mode=driving";

        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        String output = "json";

        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
    }

    public String CalculationByDistance(double latA, double lngA, Double latB, Double lngB)
    {
        String distanceInString = null;

        Location locationA = new Location("point A");

        locationA.setLatitude(latA);
        locationA.setLongitude(lngA);

        Location locationB = new Location("point B");

        locationB.setLatitude(latB);
        locationB.setLongitude(lngB);

        float distance = locationA.distanceTo(locationB);

//        locationA.distanceBetween();
//        Log.e("distance",""+distance);

        if(distance < 1000)
        {
            distanceInString =  Math.round(distance)+" m";
//            distanceInString = distance;
        }
        else if(distance > 100000)
        {
            distance = distance / 1000;

            distanceInString =  Math.round(distance)+" km";
//            distanceInString = distance;
        }

        return distanceInString;
    }


    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);

//        void onPositionClicked(int position);
//        void onLongClicked(int position);
    }

    public void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            showAlertDialog(getString(R.string.permission_title_rationale), rationale,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(BaseActivity.this,
                                    new String[]{permission}, requestCode);
                        }
                    }, getString(R.string.label_ok), null, getString(R.string.label_cancel));
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }

    protected void showAlertDialog(@Nullable String title, @Nullable String message,
                                   @Nullable DialogInterface.OnClickListener onPositiveButtonClickListener,
                                   @NonNull String positiveText,
                                   @Nullable DialogInterface.OnClickListener onNegativeButtonClickListener,
                                   @NonNull String negativeText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeText, onNegativeButtonClickListener);
        AlertDialog mAlertDialog = builder.show();
    }

    public void saveCustomer(String customerId, String displayName, String email, String firstName, String time, String age, String birthDate,
                             String lastName, String phoneNumber, String sex, boolean optInOffers, boolean update)
    {
        SharedPreferences sp = getSharedPreferences("CUSTOMER",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();

        spe.putString("customerId",customerId);
        spe.putString("displayName",displayName);
        spe.putString("email",email);
        spe.putString("firstName",firstName);
        spe.putString("time",time);
        spe.putString("age",age);
        spe.putString("birthDate",birthDate);
        spe.putString("lastName",lastName);
        spe.putBoolean("optInOffers",optInOffers);
        spe.putBoolean("update",update);
        spe.putString("phoneNumber",phoneNumber);
        spe.putString("sex",sex);

        spe.apply();

       /* {
            "customerId": "NewUser11",
                "displayName": "NewUser11",
                "email": "Newuser11@gmail.com",
                "firstName": "NewUser11",
                "time": "1535614984485",
                "age": null,
                "birthDate": null,
                "lastName": null,
                "optInOffers": null,
                "phoneNumber": null,
                "sex": null
        }*/
    }

    public SharedPreferences getCustomer()
    {
        SharedPreferences sp = getSharedPreferences("CUSTOMER", MODE_PRIVATE);
        return sp;
    }


    @Override
    public void onStart() {
        super.onStart();

        if (!checkPermissions()) {
            askPermissionForCurrentLocation();
        } else {
            getLastLocation();
        }
    }

    @SuppressWarnings("MissingPermission")
    public void getLastLocation() {

        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();

                            currnet_latitude = mLastLocation.getLatitude();
                            currnet_longitude = mLastLocation.getLongitude();

                            Log.e("BASE_ACTIVITY","LATITUDE: "+ currnet_latitude);
                            Log.e("BASE_ACTIVITY","LONGITUDE: "+ currnet_longitude);

                        } else {
                            Log.w("BASE_ACTIVITY", "getLastLocation:exception"+task.getException());
                        }
                    }
                });
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void askPermissionForCurrentLocation() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION);

        }
    }

    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

}
