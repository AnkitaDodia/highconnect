package com.sp.highconnect.restinterface;

import com.sp.highconnect.model.DispensaryPojo;
import com.sp.highconnect.model.User;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;
import retrofit.mime.TypedInput;


/**
 * Created by My 7 on 10-Aug-18.
 */
public interface RestInterface
{
    public static String LOCATION_URL = "https://beta.highconnect.co";
    public static String CUSTOMER_BASE_URL = "http://18.237.254.224/api/v1/Customer";




    @GET("/api/locations")
    public void sendDispensaryRequest(@Query("latitude") String latitude, @Query("longitude") String longitude,
                            @Query("maxDistance") String maxDistance, @Header("Authorization") String auth
                            , Callback<ArrayList<DispensaryPojo>> callback);

    @Headers("Content-Type: application/json")
    @POST("/SignUpCustomer")
    public void sendUserRegisterRequest(@Body TypedInput typedInput,
                                        @Header("Authorization") String auth,
                                        Callback<User> callBack);

//    @Headers("Content-Type: application/json")
}
