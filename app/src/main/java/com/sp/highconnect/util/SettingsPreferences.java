package com.sp.highconnect.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.sp.highconnect.model.User;

import java.io.Serializable;

public class SettingsPreferences implements Serializable {

	private static final String PREFS_NAME = "HighConnectDB";

	private static final String DEFAULT_VAL = null;

	private static final String Key_customerId = "customerId";
	private static final String Key_displayName = "displayName";
	private static final String Key_email= "email";
	private static final String Key_firstName = "firstName";
	private static final String Key_time = "time";

	public static void clearDB(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		Editor dbEditor = prefs.edit();
		dbEditor.clear();
		dbEditor.commit();
	}

	public static void storeConsumer(Context context, User user) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor dbEditor = prefs.edit();

//		dbEditor.putString(Key_customerId, user.getCustomerId());
//		dbEditor.putString(Key_displayName, user.getDisplayName());
//		dbEditor.putString(Key_email, user.getEmail());
//		dbEditor.putString(Key_firstName, user.getFirstName());
//		dbEditor.putString(Key_time,user.getTime());

		dbEditor.putString("customerId",user.getCustomerId());
		dbEditor.putString("displayName",user.getDisplayName());
		dbEditor.putString("email",user.getEmail());
		dbEditor.putString("firstName",user.getFirstName());
		dbEditor.putString("time",user.getTime());
		dbEditor.putString("age",user.getAge());
		dbEditor.putString("birthDate",user.getBirthDate());
		dbEditor.putString("lastName",user.getLastName());
		dbEditor.putBoolean("isReceive",user.getisReceive());
		dbEditor.putBoolean("isTerms",user.getisTerms());
		dbEditor.putString("phoneNumber",user.getPhoneNumber());
		dbEditor.putString("sex",user.getSex());

//		dbEditor.apply();

		dbEditor.commit();
	}

	public static User getConsumer(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

		User user = new User();

		user.setCustomerId(prefs.getString("customerId",DEFAULT_VAL));
		user.setDisplayName(prefs.getString("displayName",DEFAULT_VAL));
		user.setEmail(prefs.getString("email",DEFAULT_VAL));
		user.setFirstName(prefs.getString("firstName",DEFAULT_VAL));
		user.setTime(prefs.getString("time",DEFAULT_VAL));
		user.setAge(prefs.getString("age",DEFAULT_VAL));
		user.setBirthDate(prefs.getString("birthDate",DEFAULT_VAL));
		user.setLastName(prefs.getString("lastName",DEFAULT_VAL));
		user.setPhoneNumber(prefs.getString("phoneNumber",DEFAULT_VAL));
		user.setSex(prefs.getString("sex",DEFAULT_VAL));
		user.setReceive(prefs.getBoolean("isReceive",false));
		user.setTerms(prefs.getBoolean("isTerms",false));

		return user;
	}
}
